import { isEmpty, reject, isZero } from '../../../utils';

function maxSubArray(nums: number[]): number {
  const withoutZeros = reject(isZero, nums);
  if (isEmpty(withoutZeros)) {
    return 0;
  }

  let globalMaximum = -Infinity;
  let localMaximum = 0;
  nums.forEach((x) => {
    localMaximum = Math.max(x, localMaximum + x);
    globalMaximum = Math.max(globalMaximum, localMaximum);
  });

  return globalMaximum;
}

export default maxSubArray;
