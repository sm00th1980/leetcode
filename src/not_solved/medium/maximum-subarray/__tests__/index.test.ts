import maxSubArray from '..';

test('example 1', () => {
  const nums = [-2, 1, -3, 4, -1, 2, 1, -5, 4];
  const output = 6;

  expect(maxSubArray(nums)).toBe(output);
});

test('example 2', () => {
  const nums = [-2, 1, -3, 4, -1, 3, -5, 4];
  const output = 6;

  expect(maxSubArray(nums)).toBe(output);
});

test('example 3', () => {
  const nums = [1];
  const output = 1;

  expect(maxSubArray(nums)).toBe(output);
});

test('example 4', () => {
  const nums = [5, 4, -1, 7, 8];
  const output = 23;

  expect(maxSubArray(nums)).toBe(output);
});

test('example 5', () => {
  const nums: number[] = [];
  const output = 0;

  expect(maxSubArray(nums)).toBe(output);
});

test('example 6', () => {
  const nums = [3, 0, 0, 2, 2];
  const output = 7;

  expect(maxSubArray(nums)).toBe(output);
});

test('example 7', () => {
  const nums = [2, -1, 2, 1, 3, -2, 1, 2, 1, -2];
  const output = 9;

  expect(maxSubArray(nums)).toBe(output);
});

test('example 8', () => {
  const nums = [-2, 1];
  const output = 1;

  expect(maxSubArray(nums)).toBe(output);
});

test('example 9', () => {
  const nums = [-2, 1, -3];
  const output = 1;

  expect(maxSubArray(nums)).toBe(output);
});

test('example 10', () => {
  const nums = [-2, 1, 2];
  const output = 3;

  expect(maxSubArray(nums)).toBe(output);
});

test('example 11', () => {
  const nums = [-1, 100, -4, 100, -1];
  const output = 100 - 4 + 100;

  expect(maxSubArray(nums)).toBe(output);
});

test('example 12', () => {
  const nums = [-2, -1];
  const output = -1;

  expect(maxSubArray(nums)).toBe(output);
});

test('example 13', () => {
  const nums = [9, 0, -2, -2, -3, -4, 0, 1, -4, 5, -8, 7, -3, 7, -6, -4, -7, -8];
  const output = 11;

  expect(maxSubArray(nums)).toBe(output);
});

test('example 14', () => {
  const nums = [0];
  const output = 0;

  expect(maxSubArray(nums)).toBe(output);
});

test('example 15', () => {
  const nums = [-1, 0];
  const output = 0;

  expect(maxSubArray(nums)).toBe(output);
});

test('example 16', () => {
  const nums = [1, 2, -1, -2, 2, 1, -2, 1];
  const output = 3;

  expect(maxSubArray(nums)).toBe(output);
});

test('example 17', () => {
  const nums = [
    -57,
    9,
    -72,
    -72,
    -62,
    45,
    -97,
    24,
    -39,
    35,
    -82,
    -4,
    -63,
    1,
    -93,
    42,
    44,
    1,
    -75,
    -25,
    -87,
    -16,
    9,
    -59,
    20,
    5,
    -95,
    -41,
    4,
    -30,
    47,
    46,
    78,
    52,
    74,
    93,
    -3,
    53,
    17,
    34,
    -34,
    34,
    -69,
    -21,
    -87,
    -86,
    -79,
    56,
    -9,
    -55,
    -69,
    3,
    5,
    16,
    21,
    -75,
    -79,
    2,
    -39,
    25,
    72,
    84,
    -52,
    27,
    36,
    98,
    20,
    -90,
    52,
    -85,
    44,
    94,
    25,
    51,
    -27,
    37,
    41,
    -6,
    -30,
    -68,
    15,
    -23,
    11,
    -79,
  ];

  const output = 491;

  expect(maxSubArray(nums)).toBe(output);
});
