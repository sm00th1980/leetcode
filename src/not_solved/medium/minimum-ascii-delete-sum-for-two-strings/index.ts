const getCharCode = (letter: string) => letter.charCodeAt(0);
const calculateCharCodeSum = (letters: string[]): number => {
  return letters.map(getCharCode).reduce((acc: number, code: number) => acc + code, 0);
};
const withoutLetter = (index: number, str: string) => {
  return str
    .split('')
    .filter((_, letterIndex) => {
      return index !== letterIndex;
    })
    .join('');
};

interface DistinctLettersResult {
  distinctLetters: string[];
  result: string[];
}

export const distinctLetters = (str1: string, str2: string): DistinctLettersResult => {
  const distinct = (string1: string, string2: string): DistinctLettersResult => {
    return string1.split('').reduce(
      (acc: DistinctLettersResult, letter: string) => {
        if (string2.split('').includes(letter)) {
          acc.result.push(letter);
        } else {
          acc.distinctLetters.push(letter);
        }

        return acc;
      },
      {
        distinctLetters: [],
        result: [],
      },
    );
  };

  const distinct1 = distinct(str1, str2);
  const distinct2 = distinct(str2, str1);

  return {
    distinctLetters: [...distinct1.distinctLetters, ...distinct2.distinctLetters],
    result: [distinct1.result.join(''), distinct2.result.join('')],
  };
};

export const getCommonSubstrings = (str1: string, str2: string): string[] => {
  const matrix = str1.split('').map((_) => {
    return Array(str2.length).fill(0);
  });

  str1.split('').forEach((letter1, index1) => {
    str2.split('').forEach((letter2, index2) => {
      if (letter1 === letter2) {
        const prevValue = (matrix[index1 - 1] || [])[index2 - 1] || 0;
        matrix[index1][index2] = prevValue + 1;
      }
    });
  });

  interface IMaxIndexes {
    value: number;
    position: number[];
  }

  const maxIndexes: Array<IMaxIndexes> = [
    {
      value: -1,
      position: [],
    },
  ];

  matrix.forEach((row, rowIndex) => {
    row.forEach((value, columnIndex) => {
      if (value === maxIndexes[0].value) {
        maxIndexes.push({
          value,
          position: [rowIndex, columnIndex],
        });
      }

      if (value > maxIndexes[0].value) {
        maxIndexes[0] = {
          value,
          position: [rowIndex, columnIndex],
        };
      }
    });
  });

  maxIndexes.sort((a, b) => {
    if (a.value > b.value) {
      return -1;
    }

    if (a.value < b.value) {
      return 1;
    }

    return 0;
  });

  const maxValue: number = maxIndexes[0].value;
  const maxValues = maxIndexes.filter((element) => element.value >= maxValue);

  return maxValues.map(({ value, position: [x] }) => {
    return Array(value)
      .fill(undefined)
      .map((_, index) => str1[x - index])
      .reverse()
      .join('');
  });
};

export const withoutSubstring = (substr: string, str: string): string => {
  return str.split(substr).join('');
};

export const withoutLetters = (substr: string, str: string): string[] => {
  return substr
    .split('')
    .reduce((acc, letter) => {
      const index = acc.split('').indexOf(letter);
      if (index !== -1) {
        return withoutLetter(index, acc);
      }

      return acc;
    }, str)
    .split('')
    .sort();
};

interface IMinSum {
  distinct: string[];
  result: string;
}

export const minSum = (
  commonSubstr: string,
  str1: string,
  str2: string,
  originalStr1 = '',
  originalStr2 = '',
  sum: (string | undefined)[] = [],
): IMinSum => {
  const {
    distinctLetters: letters,
    result: [s1, s2],
  } = distinctLetters(str1, str2);
  if (s1 === s2) {
    return {
      distinct: [...sum, ...letters] as string[],
      result: s1,
    };
  }

  const sortedStr1 = str1.split('').sort().join('');
  const sortedStr2 = str2.split('').sort().join('');
  if (sortedStr1 === sortedStr2) {
    return {
      distinct: [sortedStr1[0], sortedStr2[0]],
      result: withoutLetter(0, sortedStr1),
    };
  }

  let letter_ = undefined;
  let index_ = -1;
  let commonStr_ = undefined;

  str1.split('').forEach((letter, index) => {
    const newStr1 = withoutLetter(index, str1);
    const commonSubstrs = getCommonSubstrings(newStr1, str2);

    const firstCommonSubstr = commonSubstrs[0];

    if (firstCommonSubstr.length > commonSubstr.length && firstCommonSubstr.includes(commonSubstr)) {
      commonStr_ = commonSubstrs[0];
      letter_ = letter;
      index_ = index;
    }
  });

  if (!commonStr_) {
    return {
      distinct: [...withoutLetters(commonSubstr, originalStr1), ...withoutLetters(commonSubstr, originalStr2)],
      result: commonSubstr,
    };
  }

  return minSum(commonStr_, withoutLetter(index_, str1), str2, str1, str2, [...sum, letter_]);
};

const restMinSum = (s1: string, s2: string) => {
  const commonSubstrings = getCommonSubstrings(s1, s2);

  const direct = commonSubstrings.map((commonSubstring) => {
    return minSum(commonSubstring, s1, s2, s1, s2).distinct;
  });

  const reverse = commonSubstrings.map((commonSubstring) => {
    return minSum(commonSubstring, s2, s1, s2, s1).distinct;
  });

  return [direct, reverse];
};

function minimumDeleteSum(s1: string, s2: string): number {
  const {
    distinctLetters: letters,
    result: [str1, str2],
  } = distinctLetters(s1, s2);
  if (str1 === str2) {
    return calculateCharCodeSum(letters);
  }

  // console.log('distinctLetters', letters);
  // console.log('str1', str1);
  // console.log('str2', str2);
  // console.log('calc', calculateCharCodeSum([...letters, 'b', 'i', 'i']));

  const [directs, reverses] = restMinSum(str1, str2);

  return [...directs.map((direct) => [...letters, ...direct]), ...reverses.map((reverse) => [...letters, ...reverse])]
    .map(calculateCharCodeSum)
    .sort()[0];
}

export default minimumDeleteSum;
