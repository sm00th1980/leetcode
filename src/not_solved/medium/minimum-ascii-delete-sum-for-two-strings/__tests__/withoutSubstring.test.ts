import { withoutSubstring } from '..';

test('example 1', () => {
  const substring = 'le';
  const string = 'delete';

  expect(withoutSubstring(substring, string)).toEqual('dete');
});

test('example 2', () => {
  const substring = 'te';
  const string = 'delete';

  expect(withoutSubstring(substring, string)).toEqual('dele');
});

test('example 3', () => {
  const substring = 'le';
  const string = 'leet';

  expect(withoutSubstring(substring, string)).toEqual('et');
});

test('example 4', () => {
  const substring = 'et';
  const string = 'leet';

  expect(withoutSubstring(substring, string)).toEqual('le');
});
