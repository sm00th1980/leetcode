import { getCommonSubstrings } from '..';

test('example 1', () => {
  const s1 = 'elete';
  const s2 = 'leet';

  expect(getCommonSubstrings(s1, s2)).toEqual(['le', 'et']);
});

test('example 2', () => {
  const s1 = 'delete';
  const s2 = 'leet';

  expect(getCommonSubstrings(s1, s2)).toEqual(['le', 'et']);
});

test('example 3', () => {
  const s1 = 'leet123';
  const s2 = 'leet';

  expect(getCommonSubstrings(s1, s2)).toEqual(['leet']);
});

test('example 4', () => {
  const s1 = '123leet';
  const s2 = 'leet';

  expect(getCommonSubstrings(s1, s2)).toEqual(['leet']);
});

test('example 5', () => {
  const s1 = '123leet123';
  const s2 = 'leet';

  expect(getCommonSubstrings(s1, s2)).toEqual(['leet']);
});
