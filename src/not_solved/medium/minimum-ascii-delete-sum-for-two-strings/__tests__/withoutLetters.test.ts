import { withoutLetters } from '..';

test('example 1', () => {
  const substring = 'lee';
  const string = 'delete';

  expect(withoutLetters(substring, string)).toEqual(['d', 'e', 't']);
});

test('example 2', () => {
  const substring = 'lee';
  const string = 'leet';

  expect(withoutLetters(substring, string)).toEqual(['t']);
});

test('example 3', () => {
  const substring = 'eet';
  const string = 'delete';

  expect(withoutLetters(substring, string)).toEqual(['d', 'e', 'l']);
});

test('example 4', () => {
  const substring = 'eet';
  const string = 'leet';

  expect(withoutLetters(substring, string)).toEqual(['l']);
});
