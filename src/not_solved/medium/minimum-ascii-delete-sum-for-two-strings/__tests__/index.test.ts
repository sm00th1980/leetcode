import minimumDeleteSum from '..';

test('example 1', () => {
  const s1 = 'sea';
  const s2 = 'eat';

  expect(minimumDeleteSum(s1, s2)).toBe(231);
});

test('example 2', () => {
  const s1 = 'delete';
  const s2 = 'leet';

  expect(minimumDeleteSum(s1, s2)).toBe(403);
});

test('example 3', () => {
  const s1 = 'sjfqkfxqoditw';
  const s2 = 'fxymelgo';

  expect(minimumDeleteSum(s1, s2)).toBe(1638);
});

test('example 4', () => {
  const s1 = 'xnbteodleejrzeo';
  const s2 = 'gaouojqkkk';

  expect(minimumDeleteSum(s1, s2)).toBe(2255);
});

xtest('example 5', () => {
  const s1 = 'bpiqrmtp';
  const s2 = 'alfjfobwdtbie';

  expect(minimumDeleteSum(s1, s2)).toBe(1814);
});
