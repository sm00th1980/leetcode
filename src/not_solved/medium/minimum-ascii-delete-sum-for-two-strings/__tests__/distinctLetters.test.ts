import { distinctLetters } from '..';

test('example 1', () => {
  const s1 = 'sea';
  const s2 = 'eat';

  expect(distinctLetters(s1, s2)).toEqual({
    distinctLetters: ['s', 't'],
    result: ['ea', 'ea'],
  });
});

test('example 2', () => {
  const s1 = 'delete';
  const s2 = 'leet';

  expect(distinctLetters(s1, s2)).toEqual({
    distinctLetters: ['d'],
    result: ['elete', 'leet'],
  });
});
