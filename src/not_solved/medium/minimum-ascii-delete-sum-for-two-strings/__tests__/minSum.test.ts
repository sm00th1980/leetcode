import { minSum } from '..';

test('example 1', () => {
  const substr = 'ea';
  const s1 = 'sea';
  const s2 = 'eat';

  expect(minSum(substr, s1, s2)).toEqual({
    distinct: ['s', 't'],
    result: 'ea',
  });
});

test('example 2', () => {
  const substr = 'le';
  const s1 = 'delete';
  const s2 = 'leet';

  expect(minSum(substr, s1, s2)).toEqual({
    distinct: ['d', 'e', 't', 't'],
    result: 'lee',
  });
});

test('example 3', () => {
  const substr = 'et';
  const s1 = 'delete';
  const s2 = 'leet';

  expect(minSum(substr, s1, s2)).toEqual({
    distinct: ['d', 'l', 'e', 'l'].sort(),
    result: 'eet',
  });
});

test('example 4', () => {
  const substr = 'oj';
  const s1 = 'ojo';
  const s2 = 'ooj';

  expect(minSum(substr, s1, s2)).toEqual({
    distinct: ['j', 'j'],
    result: 'oo',
  });
});

xtest('example 5', () => {
  const substr = 'bi';
  const s1 = 'bit';
  const s2 = 'btbi';

  expect(minSum(substr, s1, s2)).toEqual({
    distinct: ['b', 'i', 'i'],
    result: 'bt',
  });
});
