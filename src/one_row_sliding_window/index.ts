import { chunks, takeLast, take } from '../utils';

type Return = {
  beforeIndexes: Array<number>;
  viewportIndexes: Array<number>;
  afterIndexes: Array<number>;
};

type Props = {
  totalCount: number;
  focusedIndex: number;
  viewportCount: number;
  leftCount: number;
  rightCount: number;
};

const oneRowSlidingWindow = ({ totalCount, focusedIndex, viewportCount, leftCount, rightCount }: Props): Return => {
  const numberOfColumns = 1;
  const indexes = Array(totalCount)
    .fill(undefined)
    .map((_, index) => index);

  const pages = chunks(numberOfColumns, indexes);
  const indexOfPage = pages.findIndex((page) => page.includes(focusedIndex));

  const startIndex = indexOfPage * numberOfColumns;
  const endIndex = startIndex + viewportCount;

  const beforeIndexes = indexes.slice(0, startIndex);
  const viewportIndexes = indexes.slice(startIndex, endIndex);
  const afterIndexes = indexes.slice(endIndex);

  return {
    beforeIndexes: takeLast(leftCount, beforeIndexes),
    viewportIndexes,
    afterIndexes: take(rightCount, afterIndexes),
  };
};

export default oneRowSlidingWindow;
