import slidingWindow from '..';

describe('leftCount=1 && rightCount=1', () => {
  const totalCount = 9;
  const viewportCount = 3;
  const leftCount = 1;
  const rightCount = 1;

  test('first page, focusedIndex=0', () => {
    const focusedIndex = 0;

    const output = {
      beforeIndexes: [],
      viewportIndexes: [0, 1, 2],
      afterIndexes: [3],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        viewportCount,
        leftCount,
        rightCount,
      }),
    ).toEqual(output);
  });

  test('second page, focusedIndex=1', () => {
    const focusedIndex = 1;

    const output = {
      beforeIndexes: [0],
      viewportIndexes: [1, 2, 3],
      afterIndexes: [4],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        viewportCount,
        leftCount,
        rightCount,
      }),
    ).toEqual(output);
  });

  test('third page, focusedIndex=2', () => {
    const focusedIndex = 2;

    const output = {
      beforeIndexes: [1],
      viewportIndexes: [2, 3, 4],
      afterIndexes: [5],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        viewportCount,
        leftCount,
        rightCount,
      }),
    ).toEqual(output);
  });

  test('fourth page, focusedIndex=3', () => {
    const focusedIndex = 3;

    const output = {
      beforeIndexes: [2],
      viewportIndexes: [3, 4, 5],
      afterIndexes: [6],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        viewportCount,
        leftCount,
        rightCount,
      }),
    ).toEqual(output);
  });

  test('fifth page, focusedIndex=6', () => {
    const focusedIndex = 6;

    const output = {
      beforeIndexes: [5],
      viewportIndexes: [6, 7, 8],
      afterIndexes: [],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        viewportCount,
        leftCount,
        rightCount,
      }),
    ).toEqual(output);
  });
});

describe('leftCount=Infinity && rightCount=2', () => {
  const totalCount = 9;
  const viewportCount = 3;
  const leftCount = Infinity;
  const rightCount = 2;

  test('first page, focusedIndex=0', () => {
    const focusedIndex = 0;

    const output = {
      beforeIndexes: [],
      viewportIndexes: [0, 1, 2],
      afterIndexes: [3, 4],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        viewportCount,
        leftCount,
        rightCount,
      }),
    ).toEqual(output);
  });

  test('second page, focusedIndex=1', () => {
    const focusedIndex = 1;

    const output = {
      beforeIndexes: [0],
      viewportIndexes: [1, 2, 3],
      afterIndexes: [4, 5],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        viewportCount,
        leftCount,
        rightCount,
      }),
    ).toEqual(output);
  });

  test('third page, focusedIndex=2', () => {
    const focusedIndex = 2;

    const output = {
      beforeIndexes: [0, 1],
      viewportIndexes: [2, 3, 4],
      afterIndexes: [5, 6],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        viewportCount,
        leftCount,
        rightCount,
      }),
    ).toEqual(output);
  });

  test('fourth page, focusedIndex=3', () => {
    const focusedIndex = 3;

    const output = {
      beforeIndexes: [0, 1, 2],
      viewportIndexes: [3, 4, 5],
      afterIndexes: [6, 7],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        viewportCount,
        leftCount,
        rightCount,
      }),
    ).toEqual(output);
  });

  test('fifth page, focusedIndex=6', () => {
    const focusedIndex = 6;

    const output = {
      beforeIndexes: [0, 1, 2, 3, 4, 5],
      viewportIndexes: [6, 7, 8],
      afterIndexes: [],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        viewportCount,
        leftCount,
        rightCount,
      }),
    ).toEqual(output);
  });
});

describe('leftCount=2 && rightCount=Infinity', () => {
  const totalCount = 9;
  const viewportCount = 3;
  const leftCount = 2;
  const rightCount = Infinity;

  test('first page, focusedIndex=0', () => {
    const focusedIndex = 0;

    const output = {
      beforeIndexes: [],
      viewportIndexes: [0, 1, 2],
      afterIndexes: [3, 4, 5, 6, 7, 8],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        viewportCount,
        leftCount,
        rightCount,
      }),
    ).toEqual(output);
  });

  test('second page, focusedIndex=1', () => {
    const focusedIndex = 1;

    const output = {
      beforeIndexes: [0],
      viewportIndexes: [1, 2, 3],
      afterIndexes: [4, 5, 6, 7, 8],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        viewportCount,
        leftCount,
        rightCount,
      }),
    ).toEqual(output);
  });

  test('third page, focusedIndex=2', () => {
    const focusedIndex = 2;

    const output = {
      beforeIndexes: [0, 1],
      viewportIndexes: [2, 3, 4],
      afterIndexes: [5, 6, 7, 8],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        viewportCount,
        leftCount,
        rightCount,
      }),
    ).toEqual(output);
  });

  test('fourth page, focusedIndex=3', () => {
    const focusedIndex = 3;

    const output = {
      beforeIndexes: [1, 2],
      viewportIndexes: [3, 4, 5],
      afterIndexes: [6, 7, 8],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        viewportCount,
        leftCount,
        rightCount,
      }),
    ).toEqual(output);
  });

  test('fifth page, focusedIndex=6', () => {
    const focusedIndex = 6;

    const output = {
      beforeIndexes: [4, 5],
      viewportIndexes: [6, 7, 8],
      afterIndexes: [],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        viewportCount,
        leftCount,
        rightCount,
      }),
    ).toEqual(output);
  });
});
