import generateParenthesis from '..';

test('example 0', () => {
  const n = 0;
  const output: string[] = [];

  expect(generateParenthesis(n)).toEqual(output);
});

test('example 1', () => {
  const n = 1;
  const output = ['()'];

  expect(generateParenthesis(n)).toEqual(output);
});

test('example 2', () => {
  const n = 2;
  const output = ['(())', '()()'];

  expect(generateParenthesis(n)).toEqual(output);
});

test('example 3', () => {
  const n = 3;
  const output = ['((()))', '(()())', '(())()', '()(())', '()()()'].sort();

  expect(generateParenthesis(n)).toEqual(output);
});

test('example 4', () => {
  const n = 4;
  const output = [
    '(((())))',
    '((()()))',
    '((())())',
    '((()))()',
    '(()(()))',
    '(()()())',
    '(()())()',
    '(())(())',
    '(())()()',
    '()((()))',
    '()(()())',
    '()(())()',
    '()()(())',
    '()()()()',
  ].sort();

  expect(generateParenthesis(n)).toEqual(output);
});

test('example 5', () => {
  const n = 5;
  const output = [
    '((((()))))',
    '(((()())))',
    '(((())()))',
    '(((()))())',
    '(((())))()',
    '((()(())))',
    '((()()()))',
    '((()())())',
    '((()()))()',
    '((())(()))',
    '((())()())',
    '((())())()',
    '((()))(())',
    '((()))()()',
    '(()((())))',
    '(()(()()))',
    '(()(())())',
    '(()(()))()',
    '(()()(()))',
    '(()()()())',
    '(()()())()',
    '(()())(())',
    '(()())()()',
    '(())((()))',
    '(())(()())',
    '(())(())()',
    '(())()(())',
    '(())()()()',
    '()(((())))',
    '()((()()))',
    '()((())())',
    '()((()))()',
    '()(()(()))',
    '()(()()())',
    '()(()())()',
    '()(())(())',
    '()(())()()',
    '()()((()))',
    '()()(()())',
    '()()(())()',
    '()()()(())',
    '()()()()()',
  ].sort();

  expect(generateParenthesis(n)).toEqual(output);
});
