import { appendTo } from '..';

test('example 1', () => {
  const input = '';
  const output = ['()'];

  expect(appendTo(input)).toEqual(output);
});

test('example 2', () => {
  const input = '()';
  const output = ['()()', '(())'].sort();

  expect(appendTo(input)).toEqual(output);
});

test('example 3', () => {
  const input = '()()';
  const output = ['()()()', '(())()', '()(())'].sort();

  expect(appendTo(input)).toEqual(output);
});

test('example 4', () => {
  const input = '(())';
  const output = ['()(())', '(())()', '((()))', '(()())'].sort();

  expect(appendTo(input)).toEqual(output);
});
