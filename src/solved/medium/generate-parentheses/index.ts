export const appendTo = (source: string): string[] => {
  const set: Set<string> = Array(source.length + 1)
    .fill(undefined)
    .reduce((acc, _, index) => {
      const start = source.slice(0, index);
      const end = source.slice(index);

      return acc.add(`${start}()${end}`);
    }, new Set());

  return Array.from(set).sort();
};

export const uniqueAndSort = (source: string[]): string[] => {
  return Array.from(new Set(source)).sort();
};

function generateParenthesis(n: number): string[] {
  if (n <= 0) {
    return [];
  }

  if (n === 1) {
    return ['()'];
  }

  if (n === 2) {
    return ['(())', '()()'];
  }

  const initialValue: string[] = [];
  return uniqueAndSort(
    generateParenthesis(n - 1).reduce((acc, source) => {
      return [...acc, ...appendTo(source)];
    }, initialValue),
  );
}

export default generateParenthesis;
