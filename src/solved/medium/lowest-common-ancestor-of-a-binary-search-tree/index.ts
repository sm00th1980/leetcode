import { BinaryTreeNode as TreeNode } from '../../../utils/structures';
import { Nullable } from '../../../utils/types';
import { isNil } from '../../../utils';

type NullableTreeNode = Nullable<TreeNode>;

const isParentOrSelf = (parent: NullableTreeNode, child: NullableTreeNode): boolean => {
  if (isNil(parent)) {
    return false;
  }

  if (parent?.val === child?.val) {
    return true;
  }

  const leftChild = parent?.left as NullableTreeNode;
  const rightChild = parent?.right as NullableTreeNode;

  return isParentOrSelf(leftChild, child) || isParentOrSelf(rightChild, child);
};

const isClosestParent = (parent: NullableTreeNode, child: NullableTreeNode): boolean => {
  if (isNil(parent)) {
    return false;
  }

  if (parent?.val === child?.val) {
    return true;
  }

  if (parent?.left?.val === child?.val) {
    return true;
  }

  if (parent?.right?.val === child?.val) {
    return true;
  }

  return false;
};

const getClosestParent = (root: NullableTreeNode, p: NullableTreeNode): NullableTreeNode => {
  if (isClosestParent(root, p)) {
    return root;
  }

  const leftChild = root?.left as NullableTreeNode;
  if (isParentOrSelf(leftChild, p)) {
    return getClosestParent(leftChild, p);
  }

  const rightChild = root?.right as NullableTreeNode;
  return getClosestParent(rightChild, p);
};

function lowestCommonAncestor(root: TreeNode | null, p: TreeNode | null, q: TreeNode | null): TreeNode | null {
  if (isParentOrSelf(p, q)) {
    return p;
  }

  if (isParentOrSelf(q, p)) {
    return q;
  }

  const parentP = getClosestParent(root, p);
  const parentQ = getClosestParent(root, q);

  return lowestCommonAncestor(root, parentP, parentQ);
}

export default lowestCommonAncestor;
