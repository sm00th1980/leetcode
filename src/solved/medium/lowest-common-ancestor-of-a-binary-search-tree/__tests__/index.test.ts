import { binaryTreeNode } from '../../../../utils/structures';
import lowestCommonAncestor from '..';

test('example 1', () => {
  const p = binaryTreeNode(2, binaryTreeNode(0), binaryTreeNode(4, binaryTreeNode(3), binaryTreeNode(5)));
  const q = binaryTreeNode(8, binaryTreeNode(7), binaryTreeNode(9));
  const root = binaryTreeNode(6, p, q);

  expect(lowestCommonAncestor(root, p, q)).toBe(root);
});

test('example 2', () => {
  const q = binaryTreeNode(4, binaryTreeNode(3), binaryTreeNode(5));
  const p = binaryTreeNode(2, binaryTreeNode(0), q);
  const root = binaryTreeNode(6, p, binaryTreeNode(8, binaryTreeNode(7), binaryTreeNode(9)));

  expect(lowestCommonAncestor(root, p, q)).toBe(p);
});

test('example 3', () => {
  const q = binaryTreeNode(1);
  const p = binaryTreeNode(2, q);

  const root = p;

  expect(lowestCommonAncestor(root, p, q)).toBe(root);
});

test('example 4', () => {
  const q = binaryTreeNode(1);
  const p = binaryTreeNode(2, null, q);

  const root = p;

  expect(lowestCommonAncestor(root, p, q)).toBe(root);
});

test('example 5', () => {
  const p = binaryTreeNode(0);
  const q = binaryTreeNode(2, p);

  const root = q;

  expect(lowestCommonAncestor(root, p, q)).toEqual(root);
});

test('example 6', () => {
  const p = binaryTreeNode(0);
  const q = binaryTreeNode(9);
  const root = binaryTreeNode(
    6,
    binaryTreeNode(2, p, binaryTreeNode(4, binaryTreeNode(3), binaryTreeNode(5))),
    binaryTreeNode(8, binaryTreeNode(7), q),
  );

  expect(lowestCommonAncestor(root, p, q)).toBe(root);
});

test('example 7', () => {
  const p = binaryTreeNode(3);
  const root = binaryTreeNode(2, null, p);
  const q = root;

  expect(lowestCommonAncestor(root, p, q)).toEqual(root);
});

test('example 8', () => {
  const p = binaryTreeNode(3);
  const q = binaryTreeNode(1);
  const root = binaryTreeNode(2, q, p);

  expect(lowestCommonAncestor(root, p, q)).toEqual(root);
});

test('example 9', () => {
  const p = binaryTreeNode(2);
  const root = binaryTreeNode(3, binaryTreeNode(1, null, p), binaryTreeNode(4));
  const q = root;

  expect(lowestCommonAncestor(root, p, q)?.val).toEqual(3);
});

test('example 10', () => {
  const p = binaryTreeNode(1);
  const q = binaryTreeNode(3, binaryTreeNode(2, p), binaryTreeNode(4));

  const root = binaryTreeNode(5, q, binaryTreeNode(6));

  const output = 3;

  expect(lowestCommonAncestor(root, p, q)?.val).toEqual(output);
});

test('example 11', () => {
  const p = binaryTreeNode(1);
  const q = binaryTreeNode(4);

  const root = binaryTreeNode(5, binaryTreeNode(3, binaryTreeNode(2, p), q), binaryTreeNode(6));

  const output = 3;

  expect(lowestCommonAncestor(root, p, q)?.val).toEqual(output);
});
