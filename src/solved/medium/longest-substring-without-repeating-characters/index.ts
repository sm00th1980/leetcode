function lengthOfLongestSubstring(s: string): number {
  if (s === '') {
    return 0;
  }

  const toMax = (substring: string) => {
    const setOfUniq = substring.split('').reduce(
      (acc, char) => {
        if (acc.finished) {
          return acc;
        }

        if (acc.value.has(char)) {
          return {
            ...acc,
            finished: true,
          };
        }

        if (!acc.value.has(char)) {
          acc.value.add(char);
        }

        return acc;
      },
      {
        finished: false,
        value: new Set<string>(),
      },
    );

    return setOfUniq.value.size;
  };

  const getMaxOfSubstring = (index: number) => {
    const substring = s.slice(index);
    return toMax(substring);
  };

  let max = 0;
  for (let index = 0; index <= s.length - 1; index++) {
    const max_ = getMaxOfSubstring(index);

    if (max_ > max) {
      max = max_;
    }
  }

  return max;
}

export default lengthOfLongestSubstring;
