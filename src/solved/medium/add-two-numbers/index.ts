import { listNode, ListNode } from '../../../utils/structures';
import { head, tail } from '../../../utils';

const getNumber = (linkedList: ListNode | null, start = ''): string => {
  if (!linkedList) {
    return start;
  }

  const next = linkedList && linkedList.next ? linkedList.next : null;
  return getNumber(next, `${linkedList?.val.toString()}${start}`);
};

const createList = (xs: number[], startNode: ListNode, node: ListNode | null = null): ListNode | null => {
  if (xs.length === 0) {
    return startNode;
  }
  const nextNode = listNode(head(xs));
  (node || startNode).next = nextNode;

  return createList(tail(xs), startNode, nextNode);
};

function addTwoNumbers(l1: ListNode | null, l2: ListNode | null): ListNode | null {
  const l1Number = l1 ? BigInt(getNumber(l1)) : BigInt(0);
  const l2Number = l2 ? BigInt(getNumber(l2)) : BigInt(0);

  const sum = (l1Number + l2Number)
    .toString()
    .split('')
    .map((digit) => parseInt(digit, 10))
    .reverse();

  const node = listNode(head(sum));

  return createList(tail(sum), node);
}

export default addTwoNumbers;
