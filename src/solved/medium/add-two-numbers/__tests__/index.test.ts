import { listNode } from '../../../../utils/structures';
import addTwoNumbers from '..';

test('example 1', () => {
  const l1 = listNode(2, listNode(4, listNode(3)));
  const l2 = listNode(5, listNode(6, listNode(4)));

  const output = listNode(7, listNode(0, listNode(8)));

  expect(addTwoNumbers(l1, l2)).toEqual(output);
});

test('example 2', () => {
  const l1 = listNode(0);
  const l2 = listNode(0);

  const output = listNode(0);

  expect(addTwoNumbers(l1, l2)).toEqual(output);
});

test('example 3', () => {
  const l1 = listNode(9, listNode(9, listNode(9, listNode(9, listNode(9, listNode(9, listNode(9)))))));
  const l2 = listNode(9, listNode(9, listNode(9, listNode(9))));

  const output = listNode(8, listNode(9, listNode(9, listNode(9, listNode(0, listNode(0, listNode(0, listNode(1))))))));

  expect(addTwoNumbers(l1, l2)).toEqual(output);
});

test('example 4', () => {
  const l1 = listNode(0);
  const l2 = listNode(1);

  const output = listNode(1);

  expect(addTwoNumbers(l1, l2)).toEqual(output);
});

test('example 6', () => {
  const l1 = null;
  const l2 = listNode(0);

  const output = listNode(0);

  expect(addTwoNumbers(l1, l2)).toEqual(output);
});

test('example 7', () => {
  const l1 = null;
  const l2 = listNode(1);

  const output = listNode(1);

  expect(addTwoNumbers(l1, l2)).toEqual(output);
});

test('example 8', () => {
  const l1 = listNode(9);
  const l2 = listNode(8, listNode(9));

  const output = listNode(7, listNode(0, listNode(1)));

  expect(addTwoNumbers(l1, l2)).toEqual(output);
});

test('example 9', () => {
  const l1 = listNode(9, listNode(9));
  const l2 = listNode(9);

  const output = listNode(8, listNode(0, listNode(1)));

  expect(addTwoNumbers(l1, l2)).toEqual(output);
});

test('example 10', () => {
  const l1 = listNode(9, listNode(9, listNode(9)));
  const l2 = listNode(9);

  const output = listNode(8, listNode(0, listNode(0, listNode(1))));

  expect(addTwoNumbers(l1, l2)).toEqual(output);
});

test('example 11', () => {
  const l1 = listNode(
    1,
    listNode(
      0,
      listNode(
        0,
        listNode(
          0,
          listNode(
            0,
            listNode(
              0,
              listNode(
                0,
                listNode(
                  0,
                  listNode(
                    0,
                    listNode(
                      0,
                      listNode(
                        0,
                        listNode(
                          0,
                          listNode(
                            0,
                            listNode(
                              0,
                              listNode(
                                0,
                                listNode(
                                  0,
                                  listNode(
                                    0,
                                    listNode(
                                      0,
                                      listNode(
                                        0,
                                        listNode(
                                          0,
                                          listNode(
                                            0,
                                            listNode(
                                              0,
                                              listNode(
                                                0,
                                                listNode(
                                                  0,
                                                  listNode(
                                                    0,
                                                    listNode(
                                                      0,
                                                      listNode(0, listNode(0, listNode(0, listNode(0, listNode(1))))),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
  );
  const l2 = listNode(5, listNode(6, listNode(4)));

  const output = listNode(
    6,
    listNode(
      6,
      listNode(
        4,
        listNode(
          0,
          listNode(
            0,
            listNode(
              0,
              listNode(
                0,
                listNode(
                  0,
                  listNode(
                    0,
                    listNode(
                      0,
                      listNode(
                        0,
                        listNode(
                          0,
                          listNode(
                            0,
                            listNode(
                              0,
                              listNode(
                                0,
                                listNode(
                                  0,
                                  listNode(
                                    0,
                                    listNode(
                                      0,
                                      listNode(
                                        0,
                                        listNode(
                                          0,
                                          listNode(
                                            0,
                                            listNode(
                                              0,
                                              listNode(
                                                0,
                                                listNode(
                                                  0,
                                                  listNode(
                                                    0,
                                                    listNode(
                                                      0,
                                                      listNode(0, listNode(0, listNode(0, listNode(0, listNode(1))))),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
  );

  expect(addTwoNumbers(l1, l2)).toEqual(output);
});
