import { unitToRoman, NUMBERS } from '..';

describe('UNIT', () => {
  test('example 0', () => {
    const input = { type: NUMBERS.UNIT, number: 0 };
    const output = '';

    expect(unitToRoman(input)).toEqual(output);
  });

  test('example 1', () => {
    const input = { type: NUMBERS.UNIT, number: 1 };
    const output = 'I';

    expect(unitToRoman(input)).toEqual(output);
  });

  test('example 2', () => {
    const input = { type: NUMBERS.UNIT, number: 2 };
    const output = 'II';

    expect(unitToRoman(input)).toEqual(output);
  });

  test('example 3', () => {
    const input = { type: NUMBERS.UNIT, number: 3 };
    const output = 'III';

    expect(unitToRoman(input)).toEqual(output);
  });

  test('example 4', () => {
    const input = { type: NUMBERS.UNIT, number: 4 };
    const output = 'IV';

    expect(unitToRoman(input)).toEqual(output);
  });

  test('example 5', () => {
    const input = { type: NUMBERS.UNIT, number: 5 };
    const output = 'V';

    expect(unitToRoman(input)).toEqual(output);
  });

  test('example 6', () => {
    const input = { type: NUMBERS.UNIT, number: 6 };
    const output = 'VI';

    expect(unitToRoman(input)).toEqual(output);
  });

  test('example 7', () => {
    const input = { type: NUMBERS.UNIT, number: 7 };
    const output = 'VII';

    expect(unitToRoman(input)).toEqual(output);
  });

  test('example 8', () => {
    const input = { type: NUMBERS.UNIT, number: 8 };
    const output = 'VIII';

    expect(unitToRoman(input)).toEqual(output);
  });

  test('example 9', () => {
    const input = { type: NUMBERS.UNIT, number: 9 };
    const output = 'IX';

    expect(unitToRoman(input)).toEqual(output);
  });
});

describe('DOZEN', () => {
  test('example 0', () => {
    const input = { type: NUMBERS.DOZEN, number: 0 };
    const output = '';

    expect(unitToRoman(input)).toEqual(output);
  });

  test('example 1', () => {
    const input = { type: NUMBERS.DOZEN, number: 1 };
    const output = 'X';

    expect(unitToRoman(input)).toEqual(output);
  });

  test('example 2', () => {
    const input = { type: NUMBERS.DOZEN, number: 2 };
    const output = 'XX';

    expect(unitToRoman(input)).toEqual(output);
  });

  test('example 3', () => {
    const input = { type: NUMBERS.DOZEN, number: 3 };
    const output = 'XXX';

    expect(unitToRoman(input)).toEqual(output);
  });

  test('example 4', () => {
    const input = { type: NUMBERS.DOZEN, number: 4 };
    const output = 'XL';

    expect(unitToRoman(input)).toEqual(output);
  });

  test('example 5', () => {
    const input = { type: NUMBERS.DOZEN, number: 5 };
    const output = 'L';

    expect(unitToRoman(input)).toEqual(output);
  });

  test('example 6', () => {
    const input = { type: NUMBERS.DOZEN, number: 6 };
    const output = 'LX';

    expect(unitToRoman(input)).toEqual(output);
  });

  test('example 7', () => {
    const input = { type: NUMBERS.DOZEN, number: 7 };
    const output = 'LXX';

    expect(unitToRoman(input)).toEqual(output);
  });

  test('example 8', () => {
    const input = { type: NUMBERS.DOZEN, number: 8 };
    const output = 'LXXX';

    expect(unitToRoman(input)).toEqual(output);
  });

  test('example 9', () => {
    const input = { type: NUMBERS.DOZEN, number: 9 };
    const output = 'XC';

    expect(unitToRoman(input)).toEqual(output);
  });
});

describe('HUNDRED', () => {
  test('example 0', () => {
    const input = { type: NUMBERS.HUNDRED, number: 0 };
    const output = '';

    expect(unitToRoman(input)).toEqual(output);
  });

  test('example 1', () => {
    const input = { type: NUMBERS.HUNDRED, number: 1 };
    const output = 'C';

    expect(unitToRoman(input)).toEqual(output);
  });

  test('example 2', () => {
    const input = { type: NUMBERS.HUNDRED, number: 2 };
    const output = 'CC';

    expect(unitToRoman(input)).toEqual(output);
  });

  test('example 3', () => {
    const input = { type: NUMBERS.HUNDRED, number: 3 };
    const output = 'CCC';

    expect(unitToRoman(input)).toEqual(output);
  });

  test('example 4', () => {
    const input = { type: NUMBERS.HUNDRED, number: 4 };
    const output = 'CD';

    expect(unitToRoman(input)).toEqual(output);
  });

  test('example 5', () => {
    const input = { type: NUMBERS.HUNDRED, number: 5 };
    const output = 'D';

    expect(unitToRoman(input)).toEqual(output);
  });

  test('example 6', () => {
    const input = { type: NUMBERS.HUNDRED, number: 6 };
    const output = 'DC';

    expect(unitToRoman(input)).toEqual(output);
  });

  test('example 7', () => {
    const input = { type: NUMBERS.HUNDRED, number: 7 };
    const output = 'DCC';

    expect(unitToRoman(input)).toEqual(output);
  });

  test('example 8', () => {
    const input = { type: NUMBERS.HUNDRED, number: 8 };
    const output = 'DCCC';

    expect(unitToRoman(input)).toEqual(output);
  });

  test('example 9', () => {
    const input = { type: NUMBERS.HUNDRED, number: 9 };
    const output = 'CM';

    expect(unitToRoman(input)).toEqual(output);
  });
});

describe('THOUSAND', () => {
  test('example 0', () => {
    const input = { type: NUMBERS.THOUSAND, number: 0 };
    const output = '';

    expect(unitToRoman(input)).toEqual(output);
  });

  test('example 1', () => {
    const input = { type: NUMBERS.THOUSAND, number: 1 };
    const output = 'M';

    expect(unitToRoman(input)).toEqual(output);
  });

  test('example 2', () => {
    const input = { type: NUMBERS.THOUSAND, number: 2 };
    const output = 'MM';

    expect(unitToRoman(input)).toEqual(output);
  });

  test('example 3', () => {
    const input = { type: NUMBERS.THOUSAND, number: 3 };
    const output = 'MMM';

    expect(unitToRoman(input)).toEqual(output);
  });
});
