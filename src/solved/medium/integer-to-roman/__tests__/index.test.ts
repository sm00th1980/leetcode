import intToRoman from '..';

test('example 1', () => {
  const input = 1;
  const output = 'I';

  expect(intToRoman(input)).toEqual(output);
});

test('example 2', () => {
  const input = 2;
  const output = 'II';

  expect(intToRoman(input)).toEqual(output);
});

test('example 3', () => {
  const input = 3;
  const output = 'III';

  expect(intToRoman(input)).toEqual(output);
});

test('example 4', () => {
  const input = 4;
  const output = 'IV';

  expect(intToRoman(input)).toEqual(output);
});

test('example 5', () => {
  const input = 5;
  const output = 'V';

  expect(intToRoman(input)).toEqual(output);
});

test('example 6', () => {
  const input = 6;
  const output = 'VI';

  expect(intToRoman(input)).toEqual(output);
});

test('example 7', () => {
  const input = 7;
  const output = 'VII';

  expect(intToRoman(input)).toEqual(output);
});

test('example 8', () => {
  const input = 8;
  const output = 'VIII';

  expect(intToRoman(input)).toEqual(output);
});

test('example 9', () => {
  const input = 9;
  const output = 'IX';

  expect(intToRoman(input)).toEqual(output);
});

test('example 10', () => {
  const input = 10;
  const output = 'X';

  expect(intToRoman(input)).toEqual(output);
});

test('example 11', () => {
  const input = 11;
  const output = 'XI';

  expect(intToRoman(input)).toEqual(output);
});

test('example 12', () => {
  const input = 12;
  const output = 'XII';

  expect(intToRoman(input)).toEqual(output);
});

test('example 13', () => {
  const input = 27;
  const output = 'XXVII';

  expect(intToRoman(input)).toEqual(output);
});

test('example 14', () => {
  const input = 58;
  const output = 'LVIII';

  expect(intToRoman(input)).toEqual(output);
});

test('example 15', () => {
  const input = 1994;
  const output = 'MCMXCIV';

  expect(intToRoman(input)).toEqual(output);
});
