import { toUnit, NUMBERS } from '..';

test('example 1', () => {
  const input = { value: 1, index: 0 };
  const output = {
    type: NUMBERS.UNIT,
    number: 1,
  };

  expect(toUnit(input)).toEqual(output);
});

test('example 2', () => {
  const input = { value: 12, index: 0 };
  const output = {
    type: NUMBERS.DOZEN,
    number: 1,
  };

  expect(toUnit(input)).toEqual(output);
});

test('example 3', () => {
  const input = { value: 123, index: 0 };
  const output = {
    type: NUMBERS.HUNDRED,
    number: 1,
  };

  expect(toUnit(input)).toEqual(output);
});

test('example 4', () => {
  const input = { value: 1234, index: 0 };
  const output = {
    type: NUMBERS.THOUSAND,
    number: 1,
  };

  expect(toUnit(input)).toEqual(output);
});

test('example 5', () => {
  const input = { value: 1234, index: 1 };
  const output = {
    type: NUMBERS.HUNDRED,
    number: 2,
  };

  expect(toUnit(input)).toEqual(output);
});

test('example 5', () => {
  const input = { value: 1234, index: 2 };
  const output = {
    type: NUMBERS.DOZEN,
    number: 3,
  };

  expect(toUnit(input)).toEqual(output);
});

test('example 6', () => {
  const input = { value: 1234, index: 3 };
  const output = {
    type: NUMBERS.UNIT,
    number: 4,
  };

  expect(toUnit(input)).toEqual(output);
});

test('example 6', () => {
  const input = { value: 1230, index: 3 };
  const output = {
    type: NUMBERS.UNIT,
    number: 0,
  };

  expect(toUnit(input)).toEqual(output);
});
