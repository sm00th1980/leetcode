import { values, reverse, repeat } from '../../../utils';

export const NUMBERS = {
  UNIT: 1,
  DOZEN: 10,
  HUNDRED: 100,
  THOUSAND: 1000,
};

export const toUnit = ({ value, index }: { value: number; index: number }) => {
  const stringifiedValue = value.toString();
  const numbers = reverse(values(NUMBERS).slice(0, stringifiedValue.length));

  const type = numbers[index];
  const number = Number(stringifiedValue[index]);

  return { type, number };
};

export const unitToRoman = ({ type, number }: { type: number; number: number }): string => {
  if (type === NUMBERS.UNIT) {
    if (number === 0) {
      return '';
    }

    if (number <= 3) {
      return repeat(number, 'I');
    }

    if (number === 4) {
      return 'IV';
    }

    if (number >= 5 && number < 9) {
      return 'V' + repeat(number - 5, 'I');
    }

    if (number === 9) {
      return 'IX';
    }
  }

  if (type === NUMBERS.DOZEN) {
    if (number === 0) {
      return '';
    }

    if (number <= 3) {
      return repeat(number, 'X');
    }

    if (number === 4) {
      return 'XL';
    }

    if (number >= 5 && number < 9) {
      return 'L' + repeat(number - 5, 'X');
    }

    if (number === 9) {
      return 'XC';
    }
  }

  if (type === NUMBERS.HUNDRED) {
    if (number === 0) {
      return '';
    }

    if (number <= 3) {
      return repeat(number, 'C');
    }

    if (number === 4) {
      return 'CD';
    }

    if (number >= 5 && number < 9) {
      return 'D' + repeat(number - 5, 'C');
    }

    if (number === 9) {
      return 'CM';
    }
  }

  if (type === NUMBERS.THOUSAND) {
    if (number === 0) {
      return '';
    }

    if (number <= 3) {
      return repeat(number, 'M');
    }

    throw Error('reached max thousand');
  }

  throw Error('failed to convert unit to roman');
};

function intToRoman(num: number): string {
  const numbers = num
    .toString()
    .split('')
    .map((x) => parseInt(x, 10));

  return numbers
    .map((_, index) => toUnit({ value: num, index }))
    .map(unitToRoman)
    .join('');
}

export default intToRoman;
