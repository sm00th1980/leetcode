/**
 Do not return anything, modify s in-place instead.
 */
function reverseString(s: string[]): void {
  const isEven = s.length % 2 === 0;
  for (let startIndex = 0; startIndex < s.length; startIndex++) {
    if (
      (startIndex * 2 < s.length && isEven) || //even
      (startIndex * 2 + 1 <= s.length && !isEven) //odd
    ) {
      const startLetter = s[startIndex];
      const endIndex = s.length - 1 - startIndex;
      const endLetter = s[endIndex];
      s[startIndex] = endLetter;
      s[endIndex] = startLetter;
    } else {
      break;
    }
  }
}

export default reverseString;
