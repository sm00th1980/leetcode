import reverseString from '..';

test('example 1', () => {
  const input = ['h', 'e', 'l', 'l', 'o'];
  const output = ['o', 'l', 'l', 'e', 'h'];

  reverseString(input);
  expect(input).toEqual(output);
});

test('example 2', () => {
  const input = ['H', 'a', 'n', 'n', 'a', 'h'];
  const output = ['h', 'a', 'n', 'n', 'a', 'H'];

  reverseString(input);
  expect(input).toEqual(output);
});

test('example 3', () => {
  const input = [
    'A',
    ' ',
    'm',
    'a',
    'n',
    ',',
    ' ',
    'a',
    ' ',
    'p',
    'l',
    'a',
    'n',
    ',',
    ' ',
    'a',
    ' ',
    'c',
    'a',
    'n',
    'a',
    'l',
    ':',
    ' ',
    'P',
    'a',
    'n',
    'a',
    'm',
    'a',
  ];
  const output = [
    'a',
    'm',
    'a',
    'n',
    'a',
    'P',
    ' ',
    ':',
    'l',
    'a',
    'n',
    'a',
    'c',
    ' ',
    'a',
    ' ',
    ',',
    'n',
    'a',
    'l',
    'p',
    ' ',
    'a',
    ' ',
    ',',
    'n',
    'a',
    'm',
    ' ',
    'A',
  ];

  reverseString(input);
  expect(input).toEqual(output);
});

test('example 4', () => {
  const input = [
    'A',
    ' ',
    'm',
    'a',
    'n',
    ',',
    ' ',
    'a',
    ' ',
    'p',
    'l',
    'a',
    'n',
    ',',
    ' ',
    'a',
    ' ',
    'c',
    'a',
    'n',
    'a',
    'l',
    ':',
    ' ',
    'P',
    'a',
    'n',
    'a',
    'm',
  ];
  const output = [
    'm',
    'a',
    'n',
    'a',
    'P',
    ' ',
    ':',
    'l',
    'a',
    'n',
    'a',
    'c',
    ' ',
    'a',
    ' ',
    ',',
    'n',
    'a',
    'l',
    'p',
    ' ',
    'a',
    ' ',
    ',',
    'n',
    'a',
    'm',
    ' ',
    'A',
  ];

  reverseString(input);
  expect(input).toEqual(output);
});
