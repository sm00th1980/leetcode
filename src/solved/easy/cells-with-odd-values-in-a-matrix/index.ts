export const getOddCount = (matrix: number[][]): number => {
  return matrix.reduce((rowSum, row) => {
    return (
      rowSum +
      row.reduce((acc, x) => {
        if (x % 2 === 0) {
          return acc;
        }

        return acc + 1;
      }, 0)
    );
  }, 0);
};

export const incrementRow = (matrix: number[][], rowIndex: number): number[][] => {
  return matrix.map((row, index) => {
    if (index === rowIndex) {
      return row.map((x) => x + 1);
    }

    return row;
  });
};

export const incrementColumn = (matrix: number[][], columnIndex: number): number[][] => {
  return matrix.map((row) => {
    return row.map((x, index) => {
      if (index === columnIndex) {
        return x + 1;
      }

      return x;
    });
  });
};

export const makeMatrix = (columns: number, rows: number): number[][] => {
  return Array(columns)
    .fill(undefined)
    .map(() => {
      return Array(rows)
        .fill(undefined)
        .map(() => 0);
    });
};

function oddCells(n: number, m: number, indices: number[][]): number {
  const filledMatrix = indices.reduce((acc, [rowIndex, columnIndex]) => {
    return incrementRow(incrementColumn(acc, columnIndex), rowIndex);
  }, makeMatrix(n, m));

  return getOddCount(filledMatrix);
}

export default oddCells;
