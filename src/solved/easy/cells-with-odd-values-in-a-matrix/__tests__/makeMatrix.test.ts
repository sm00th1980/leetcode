import { makeMatrix } from '..';

test('example 1', () => {
  const n = 2;
  const m = 3;
  const output = [
    [0, 0, 0],
    [0, 0, 0],
  ];

  expect(makeMatrix(n, m)).toEqual(output);
});

test('example 2', () => {
  const n = 2;
  const m = 2;
  const output = [
    [0, 0],
    [0, 0],
  ];

  expect(makeMatrix(n, m)).toEqual(output);
});
