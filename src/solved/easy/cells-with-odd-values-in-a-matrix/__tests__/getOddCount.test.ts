import { getOddCount } from '..';

test('example 1', () => {
  const matrix = [
    [1, 3, 1],
    [1, 3, 1],
  ];
  const output = 6;

  expect(getOddCount(matrix)).toEqual(output);
});

test('example 2', () => {
  const matrix = [
    [2, 2],
    [2, 2],
  ];
  const output = 0;

  expect(getOddCount(matrix)).toEqual(output);
});
