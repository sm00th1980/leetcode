import { incrementRow } from '..';

test('example 1', () => {
  const matrix = [
    [0, 0, 0],
    [0, 0, 0],
  ];
  const rowIndex = 0;
  const output = [
    [1, 1, 1],
    [0, 0, 0],
  ];

  expect(incrementRow(matrix, rowIndex)).toEqual(output);
});

test('example 2', () => {
  const matrix = [
    [0, 0],
    [0, 0],
  ];
  const rowIndex = 1;
  const output = [
    [0, 0],
    [1, 1],
  ];

  expect(incrementRow(matrix, rowIndex)).toEqual(output);
});
