import { incrementColumn } from '..';

test('example 1', () => {
  const matrix = [
    [0, 0, 0],
    [0, 0, 0],
  ];
  const columnIndex = 1;
  const output = [
    [0, 1, 0],
    [0, 1, 0],
  ];

  expect(incrementColumn(matrix, columnIndex)).toEqual(output);
});

test('example 2', () => {
  const matrix = [
    [0, 0],
    [0, 0],
  ];
  const columnIndex = 0;
  const output = [
    [1, 0],
    [1, 0],
  ];

  expect(incrementColumn(matrix, columnIndex)).toEqual(output);
});
