import oddCells from '..';

test('example 1', () => {
  const n = 2;
  const m = 3;
  const indices = [
    [0, 1],
    [1, 1],
  ];
  const output = 6;

  expect(oddCells(n, m, indices)).toEqual(output);
});

test('example 2', () => {
  const n = 2;
  const m = 2;
  const indices = [
    [1, 1],
    [0, 0],
  ];
  const output = 0;

  expect(oddCells(n, m, indices)).toEqual(output);
});
