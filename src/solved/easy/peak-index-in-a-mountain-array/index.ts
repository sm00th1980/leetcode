import { isNil } from '../../../utils';

const getPeak = (xs: number[], indexes: number[]): number => {
  if (xs.length === 1) {
    return indexes[0];
  }

  const middleIndex = Math.floor(xs.length / 2);

  const possiblePeak = xs[middleIndex];
  if (possiblePeak > xs[middleIndex - 1] && possiblePeak > xs[middleIndex + 1]) {
    return indexes[middleIndex];
  }

  if (possiblePeak > xs[middleIndex - 1] && isNil(xs[middleIndex + 1])) {
    return indexes[middleIndex];
  }

  if (isNil(xs[middleIndex - 1]) && possiblePeak > xs[middleIndex + 1]) {
    return indexes[middleIndex];
  }

  if (possiblePeak < xs[middleIndex + 1]) {
    return getPeak(xs.slice(middleIndex + 1), indexes.slice(middleIndex + 1));
  }

  if (xs[middleIndex - 1] > possiblePeak) {
    return getPeak(xs.slice(0, middleIndex), indexes.slice(0, middleIndex));
  }

  return -1;
};

function peakIndexInMountainArray(arr: number[]): number {
  return getPeak(
    arr,
    Array(arr.length)
      .fill(undefined)
      .map((_, index) => index),
  );
}

export default peakIndexInMountainArray;
