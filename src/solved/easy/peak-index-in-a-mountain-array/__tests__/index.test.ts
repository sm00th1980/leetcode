import peakIndexInMountainArray from '..';

test('example 1', () => {
  const input = [0, 1, 0];
  const output = 1;

  expect(peakIndexInMountainArray(input)).toEqual(output);
});

test('example 2', () => {
  const input = [0, 2, 1, 0];
  const output = 1;

  expect(peakIndexInMountainArray(input)).toEqual(output);
});

test('example 3', () => {
  const input = [0, 10, 5, 2];
  const output = 1;

  expect(peakIndexInMountainArray(input)).toEqual(output);
});

test('example 4', () => {
  const input = [3, 4, 5, 1];
  const output = 2;

  expect(peakIndexInMountainArray(input)).toEqual(output);
});

test('example 5', () => {
  const input = [24, 69, 100, 99, 79, 78, 67, 36, 26, 19];
  const output = 2;

  expect(peakIndexInMountainArray(input)).toEqual(output);
});

test('example 6', () => {
  const input = [40, 48, 61, 75, 100, 99, 98, 39, 30, 10];
  const output = 4;

  expect(peakIndexInMountainArray(input)).toEqual(output);
});

test('example 7', () => {
  const input = [18, 29, 38, 59, 98, 100, 99, 98, 90];
  const output = 5;

  expect(peakIndexInMountainArray(input)).toEqual(output);
});
