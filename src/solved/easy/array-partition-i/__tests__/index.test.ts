import arrayPairSum from '..';

test('example 1', () => {
  const input = [1, 4, 3, 2];
  const output = 4;

  expect(arrayPairSum(input)).toEqual(output);
});
