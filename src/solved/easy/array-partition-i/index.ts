import { chunks } from '.././../../utils';

function arrayPairSum(nums: number[]): number {
  return chunks(
    2,
    nums.sort((a, b) => a - b),
  ).reduce((acc, arr) => {
    return acc + Math.min(arr[0], arr[1]);
  }, 0);
}

export default arrayPairSum;
