import { equals, transposeMatrix } from '../../../utils';

const isSorted = (xs: string[]): boolean => {
  const sortedXs = [...xs].sort();

  return equals(xs, sortedXs);
};

function minDeletionSize(A: string[]): number {
  const matrix = A.map((value) => value.split(''));
  const notSorted = (row: string[]) => !isSorted(row);

  return transposeMatrix(matrix).filter(notSorted).length;
}

export default minDeletionSize;
