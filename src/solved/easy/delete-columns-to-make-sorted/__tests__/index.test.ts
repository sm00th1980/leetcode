import minDeletionSize from '..';

test('example 1', () => {
  const input = ['cba', 'daf', 'ghi'];
  const output = 1;

  expect(minDeletionSize(input)).toEqual(output);
});

test('example 2', () => {
  const input = ['a', 'b'];
  const output = 0;

  expect(minDeletionSize(input)).toEqual(output);
});

test('example 3', () => {
  const input = ['zyx', 'wvu', 'tsr'];
  const output = 3;

  expect(minDeletionSize(input)).toEqual(output);
});
