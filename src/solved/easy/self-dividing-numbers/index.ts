const allPass = (xs: boolean[]): boolean => {
  return xs.reduce((acc, value) => {
    if (!acc) {
      return false;
    }

    if (value) {
      return true;
    }

    return false;
  }, true);
};

const predicate = (x: number, digit: number): boolean => {
  if (digit === 0) {
    return false;
  }

  return x % digit === 0;
};

const isSelfDivided = (x: number): boolean => {
  const result = x
    .toString()
    .split('')
    .map((digit) => predicate(x, +digit));
  return allPass(result);
};

function selfDividingNumbers(left: number, right: number): number[] {
  const results: number[] = [];
  for (let i = left; i <= right; i++) {
    if (isSelfDivided(i)) {
      results.push(i);
    }
  }

  return results;
}

export default selfDividingNumbers;
