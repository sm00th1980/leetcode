type Triplet = [number, number, number];
type Pair = [number, number];

const isGoodTriplet = (triplet: Triplet, a: number, b: number, c: number): boolean => {
  const [i, j, k] = triplet;
  return Math.abs(i - j) <= a && Math.abs(j - k) <= b && Math.abs(i - k) <= c;
};

export interface PairResult {
  value: Pair;
  secondIndex: number;
}

export const generateTriplets = (pairResult: PairResult, xs: number[]): Triplet[] => {
  const initialValue: Triplet[] = [];
  return xs.slice(pairResult.secondIndex + 1).reduce((acc, third) => {
    acc.push([...pairResult.value, third]);
    return acc;
  }, initialValue);
};

export const generatePairs = (xs: number[]): PairResult[] => {
  const initialValue: PairResult[] = [];
  return xs.reduce((acc, first, index) => {
    xs.slice(index + 1).forEach((second, secondIndex) => {
      acc.push({
        value: [first, second],
        secondIndex: index + 1 + secondIndex,
      });
    });

    return acc;
  }, initialValue);
};

function countGoodTriplets(arr: number[], a: number, b: number, c: number): number {
  const pairs = generatePairs(arr);
  const initialValue: Triplet[] = [];
  const triplets = pairs.reduce((acc, pair) => {
    generateTriplets(pair, arr).forEach((triplet) => {
      acc.push(triplet);
    });

    return acc;
  }, initialValue);

  return triplets.filter((triplet) => isGoodTriplet(triplet, a, b, c)).length;
}

export default countGoodTriplets;
