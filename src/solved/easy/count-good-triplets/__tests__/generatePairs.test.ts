import { generatePairs } from '..';

test('example 1', () => {
  const input = [1, 2];
  const output = [
    {
      value: [1, 2],
      secondIndex: 1,
    },
  ];

  expect(generatePairs(input)).toEqual(output);
});

test('example 2', () => {
  const input = [1, 2, 3];
  const output = [
    {
      value: [1, 2],
      secondIndex: 1,
    },
    {
      value: [1, 3],
      secondIndex: 2,
    },
    {
      value: [2, 3],
      secondIndex: 2,
    },
  ];

  expect(generatePairs(input)).toEqual(output);
});

test('example 3', () => {
  const input = [1, 2, 3, 4];
  const output = [
    {
      value: [1, 2],
      secondIndex: 1,
    },
    {
      value: [1, 3],
      secondIndex: 2,
    },
    {
      value: [1, 4],
      secondIndex: 3,
    },
    {
      value: [2, 3],
      secondIndex: 2,
    },
    {
      value: [2, 4],
      secondIndex: 3,
    },
    {
      value: [3, 4],
      secondIndex: 3,
    },
  ];

  expect(generatePairs(input)).toEqual(output);
});
