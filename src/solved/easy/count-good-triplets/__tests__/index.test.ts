import countGoodTriplets from '..';

test('example 1', () => {
  const arr = [3, 0, 1, 1, 9, 7];
  const a = 7;
  const b = 2;
  const c = 3;
  const output = 4;

  expect(countGoodTriplets(arr, a, b, c)).toEqual(output);
});

test('example 2', () => {
  const arr = [1, 1, 2, 2, 3];
  const a = 0;
  const b = 0;
  const c = 1;
  const output = 0;

  expect(countGoodTriplets(arr, a, b, c)).toEqual(output);
});
