import { generateTriplets, PairResult } from '..';

test('example 1', () => {
  const pair: PairResult = {
    value: [1, 2],
    secondIndex: 1,
  };
  const xs = [1, 2, 3];
  const output = [[1, 2, 3]];

  expect(generateTriplets(pair, xs)).toEqual(output);
});

test('example 2', () => {
  const pair: PairResult = {
    value: [1, 2],
    secondIndex: 1,
  };
  const xs = [1, 2, 3, 4];
  const output = [
    [1, 2, 3],
    [1, 2, 4],
  ];

  expect(generateTriplets(pair, xs)).toEqual(output);
});

test('example 3', () => {
  const pair: PairResult = {
    value: [1, 3],
    secondIndex: 2,
  };
  const xs = [1, 2, 3, 4];
  const output = [[1, 3, 4]];

  expect(generateTriplets(pair, xs)).toEqual(output);
});

test('example 4', () => {
  const pair: PairResult = {
    value: [1, 4],
    secondIndex: 3,
  };
  const xs = [1, 2, 3, 4];
  const output: number[][] = [];

  expect(generateTriplets(pair, xs)).toEqual(output);
});

test('example 5', () => {
  const pair: PairResult = {
    value: [2, 3],
    secondIndex: 2,
  };
  const xs = [1, 2, 3, 4];
  const output = [[2, 3, 4]];

  expect(generateTriplets(pair, xs)).toEqual(output);
});
