import { head, tail, findIndex, reject, isEmpty } from '../../../utils';

const canConstructUsingLetters = (ransomNoteLetters: string[], magazineLetters: string[]): boolean => {
  if (isEmpty(ransomNoteLetters)) {
    return true;
  }

  const firstRansomNoteLetter = head(ransomNoteLetters);
  const firstIndex = findIndex((letter) => letter === firstRansomNoteLetter, magazineLetters);

  if (firstIndex === -1) {
    // failed to find matching letter
    return false;
  }

  const withoutFoundedLetter = reject((_, index) => index === firstIndex, magazineLetters);
  return canConstructUsingLetters(tail(ransomNoteLetters), withoutFoundedLetter);
};

function canConstruct(ransomNote: string, magazine: string): boolean {
  const ransomNoteLetters = ransomNote.split('');
  const magazineLetters = magazine.split('');

  return canConstructUsingLetters(ransomNoteLetters, magazineLetters);
}

export default canConstruct;
