import { Callable } from '../../../utils/types';

const debounce = (fn: Callable, timeout: number) => {
  let timer: NodeJS.Timeout;
  return function (...args: Array<unknown>) {
    clearTimeout(timer);
    timer = setTimeout(() => {
      fn(...args);
    }, timeout);
  };
};

const log = (n: number): void => {
  console.log(n);
};

const debouncedLog = debounce(log, 1000);

debouncedLog(1);
debouncedLog(2);
debouncedLog(3);

export default debounce;
