import { isNil } from '../../../utils';

type Point = {
  sr: number;
  sc: number;
};

const isValid = (point: Point, image: number[][]): boolean => {
  const currentColor = image?.[point.sr]?.[point.sc];

  if (isNil(currentColor)) {
    return false;
  }

  return true;
};

const isNeedToChangeColor = (image: number[][], point: Point, color: number): boolean => {
  const originalColor = image[point.sr][point.sc];

  return originalColor === color;
};

const toTop = (sr: number, sc: number): Point => ({
  sr: sr - 1,
  sc,
});

const toRight = (sr: number, sc: number): Point => ({
  sc: sc + 1,
  sr,
});

const toBottom = (sr: number, sc: number): Point => ({
  sc,
  sr: sr + 1,
});

const toLeft = (sr: number, sc: number): Point => ({
  sc: sc - 1,
  sr,
});

function floodFill(image: number[][], sr: number, sc: number, color: number): number[][] {
  if (!isValid({ sr, sc }, image)) {
    return image;
  }

  const currentColor = image[sr][sc];

  if (currentColor === color) {
    return image;
  }

  // update color
  image[sr][sc] = color;

  const top = toTop(sr, sc);
  const right = toRight(sr, sc);
  const bottom = toBottom(sr, sc);
  const left = toLeft(sr, sc);

  return [top, right, bottom, left]
    .filter((point) => isValid(point, image))
    .filter((point) => isNeedToChangeColor(image, point, currentColor))
    .reduce((acc, point) => {
      return floodFill(acc, point.sr, point.sc, color);
    }, image);
}

export default floodFill;
