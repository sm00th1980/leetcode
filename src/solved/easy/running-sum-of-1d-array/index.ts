interface Result {
  prev: number | undefined;
  result: number[];
}
function runningSum(nums: number[]): number[] {
  const initialValue: Result = {
    prev: undefined,
    result: [],
  };
  const { result } = nums.reduce((acc: Result, i: number) => {
    const { prev } = acc;

    if (typeof prev === 'undefined') {
      return {
        prev: i,
        result: [i],
      };
    }

    return {
      prev: prev + i,
      result: [...acc.result, prev + i],
    };
  }, initialValue);

  return result;
}

export default runningSum;
