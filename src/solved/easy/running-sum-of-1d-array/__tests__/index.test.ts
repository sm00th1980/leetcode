import runningSum from '..';

test('example 1', () => {
  const input = [1, 2, 3, 4];
  const output = [1, 3, 6, 10];

  expect(runningSum(input)).toEqual(output);
});

test('example 2', () => {
  const input = [1, 1, 1, 1, 1];
  const output = [1, 2, 3, 4, 5];

  expect(runningSum(input)).toEqual(output);
});

test('example 3', () => {
  const input = [3, 1, 2, 10, 1];
  const output = [3, 4, 6, 16, 17];

  expect(runningSum(input)).toEqual(output);
});

test('example 4', () => {
  const input = [
    0,
    63,
    -23,
    60,
    -27,
    -73,
    -53,
    -5,
    63,
    68,
    -85,
    -82,
    -1,
    -11,
    96,
    19,
    33,
    -72,
    -93,
    -44,
    -65,
    -60,
    17,
    95,
    -98,
    -43,
    -67,
  ];
  const output = [
    0,
    63,
    40,
    100,
    73,
    0,
    -53,
    -58,
    5,
    73,
    -12,
    -94,
    -95,
    -106,
    -10,
    9,
    42,
    -30,
    -123,
    -167,
    -232,
    -292,
    -275,
    -180,
    -278,
    -321,
    -388,
  ];

  expect(runningSum(input)).toEqual(output);
});

test('example 5', () => {
  const input = [0, 63, -23];
  const output = [0, 63, 40];

  expect(runningSum(input)).toEqual(output);
});
