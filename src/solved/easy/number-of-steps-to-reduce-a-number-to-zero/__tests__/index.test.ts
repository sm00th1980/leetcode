import numberOfSteps from '..';

test('example 1', () => {
  const n = 14;
  const output = 6;

  expect(numberOfSteps(n)).toEqual(output);
});

test('example 2', () => {
  const n = 8;
  const output = 4;

  expect(numberOfSteps(n)).toEqual(output);
});

test('example 3', () => {
  const n = 123;
  const output = 12;

  expect(numberOfSteps(n)).toEqual(output);
});
