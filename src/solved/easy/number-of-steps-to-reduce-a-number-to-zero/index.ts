function numberOfSteps(num: number): number {
  let steps = 0;
  let currentNum = num;

  while (currentNum > 0) {
    if (currentNum % 2 === 0) {
      currentNum /= 2;
    } else {
      currentNum--;
    }

    steps++;
  }

  return steps;
}

export default numberOfSteps;
