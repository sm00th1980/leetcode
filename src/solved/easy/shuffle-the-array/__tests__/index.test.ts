import shuffle from '..';

test('example 1', () => {
  const nums = [2, 5, 1, 3, 4, 7];
  const n = 3;
  const output = [2, 3, 5, 4, 1, 7];

  expect(shuffle(nums, n)).toEqual(output);
});

test('example 2', () => {
  const nums = [1, 2, 3, 4, 4, 3, 2, 1];
  const n = 4;
  const output = [1, 4, 2, 3, 3, 2, 4, 1];

  expect(shuffle(nums, n)).toEqual(output);
});

test('example 3', () => {
  const nums = [1, 1, 2, 2];
  const n = 2;
  const output = [1, 2, 1, 2];

  expect(shuffle(nums, n)).toEqual(output);
});
