function shuffle(nums: number[], n: number): number[] {
  const arr1 = nums.slice(0, n);
  const arr2 = nums.slice(n);

  const result: number[] = [];

  arr1.forEach((x, index) => {
    result.push(x);
    result.push(arr2[index]);
  });

  return result;
}

export default shuffle;
