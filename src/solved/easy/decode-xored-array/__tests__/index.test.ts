import decode from '..';

test('example 1', () => {
  const encoded = [1, 2, 3];
  const first = 1;
  const output = [1, 0, 2, 1];

  expect(decode(encoded, first)).toEqual(output);
});

test('example 2', () => {
  const encoded = [6, 2, 7, 3];
  const first = 4;
  const output = [4, 2, 0, 7, 4];

  expect(decode(encoded, first)).toEqual(output);
});
