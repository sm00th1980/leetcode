import { last } from '../../../utils';

function decode(encoded: number[], first: number): number[] {
  return encoded.reduce(
    (acc, i) => {
      const decoded = last(acc) ^ i;
      return [...acc, decoded];
    },
    [first],
  );
}

export default decode;
