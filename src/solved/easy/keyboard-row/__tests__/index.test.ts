import findWords from '..';

test('example 1', () => {
  const input = ['Hello', 'Alaska', 'Dad', 'Peace'];
  const output = ['Alaska', 'Dad'];

  expect(findWords(input)).toEqual(output);
});
