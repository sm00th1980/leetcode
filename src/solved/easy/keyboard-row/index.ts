const ROWS = [
  ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p'],
  ['a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l'],
  ['z', 'x', 'c', 'v', 'b', 'n', 'm'],
];

const inOneRow = (row: string[], word: string): boolean => {
  return word.split('').reduce((acc: boolean, letter) => {
    if (!acc) {
      return acc;
    }

    return row.includes(letter.toLowerCase());
  }, true);
};

function findWords(words: string[]): string[] {
  return words.filter((word) => inOneRow(ROWS[0], word) || inOneRow(ROWS[1], word) || inOneRow(ROWS[2], word));
}

export default findWords;
