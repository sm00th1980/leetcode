import { isNil } from '../../../utils';

function twoSum(nums: number[], target: number): number[] {
  let firstIndex = 0;
  let secondIndex = 1;

  while (nums[firstIndex] + nums[secondIndex] !== target) {
    if (isNil(nums[firstIndex])) {
      break;
    }

    if (isNil(nums[secondIndex])) {
      firstIndex = firstIndex + 1;
      secondIndex = firstIndex + 1;
      continue;
    }

    secondIndex = secondIndex + 1;
  }

  return [firstIndex, secondIndex];
}

export default twoSum;
