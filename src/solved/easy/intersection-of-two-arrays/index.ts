function intersection(nums1: number[], nums2: number[]): number[] {
  const longArray = nums1.length >= nums2.length ? nums1 : nums2;
  const shortArray = nums1.length >= nums2.length ? nums2 : nums1;

  const initialValue = new Set<number>();
  return Array.from(
    longArray.reduce((acc, value) => {
      if (shortArray.includes(value)) {
        return acc.add(value);
      }

      return acc;
    }, initialValue),
  );
}

export default intersection;
