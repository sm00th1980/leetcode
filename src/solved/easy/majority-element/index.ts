import { unique, last, getCount } from '../../../utils';

function majorityElement(nums: number[]): number {
  return last(
    unique(nums)
      .map((x) => ({ x, count: getCount(x, nums) }))
      .sort((a, b) => a.count - b.count),
  ).x;
}

export default majorityElement;
