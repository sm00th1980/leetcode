import majorityElement from '..';

test('example 1', () => {
  const input = [3, 2, 3];
  const output = 3;

  expect(majorityElement(input)).toEqual(output);
});

test('example 2', () => {
  const input = [2, 2, 1, 1, 1, 2, 2];
  const output = 2;

  expect(majorityElement(input)).toEqual(output);
});
