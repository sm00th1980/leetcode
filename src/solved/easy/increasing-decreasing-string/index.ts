import { head, last } from '../../../utils';

interface Result {
  result: string;
  rest: string;
}

interface ResultOne {
  letter: string;
  rest: string;
}

interface ResultMany {
  letter: string | undefined;
  rest: string;
}

const withoutIndex = (index: number, letters: string[]): string[] => {
  return letters.filter((_, idx) => idx !== index);
};

export const withoutLetter = (letter: string, source: string): string => {
  const index = source.split('').findIndex((value) => value === letter);
  return withoutIndex(index, source.split('')).join('');
};

export const pickSmallest = (source: string): ResultOne => {
  const letters = source.split('');
  letters.sort((a, b) => a.charCodeAt(0) - b.charCodeAt(0));

  const letter = head(letters);
  return {
    letter,
    rest: withoutLetter(letter, source),
  };
};

export const pickSmallestThanGreater = (char: string, source: string): ResultMany => {
  const letters = source.split('');
  const greaters = letters.filter((letter) => letter > char);
  if (greaters.length === 0) {
    return {
      letter: undefined,
      rest: source,
    };
  }
  const { letter } = pickSmallest(greaters.join(''));
  return {
    letter,
    rest: withoutLetter(letter, source),
  };
};

export const pickSmallests = (char: string, source: string): Result => {
  let output = '';

  let result = pickSmallestThanGreater(char, source);
  while (result.letter) {
    output = output + result.letter;
    result = pickSmallestThanGreater(result.letter, result.rest);
  }

  return {
    result: output,
    rest: result.rest,
  };
};

export const pickLargests = (char: string, source: string): Result => {
  let output = '';

  let result = pickLargestThanSmaller(char, source);
  while (result.letter) {
    output = output + result.letter;
    result = pickLargestThanSmaller(result.letter, result.rest);
  }

  return {
    result: output,
    rest: result.rest,
  };
};

export const pickLargest = (source: string): ResultOne => {
  const letters = source.split('');
  letters.sort((a, b) => a.charCodeAt(0) - b.charCodeAt(0));

  const letter = last(letters);
  return {
    letter,
    rest: withoutLetter(letter, source),
  };
};

export const pickLargestThanSmaller = (char: string, source: string): ResultMany => {
  const letters = source.split('');
  const smallers = letters.filter((letter) => letter < char);
  if (smallers.length === 0) {
    return {
      letter: undefined,
      rest: source,
    };
  }
  const { letter } = pickLargest(smallers.join(''));
  return {
    letter,
    rest: withoutLetter(letter, source),
  };
};

export const pickInc = (source: string): Result => {
  const { letter: smallest, rest } = pickSmallest(source);
  const result = pickSmallests(smallest, rest);

  return {
    result: smallest + result.result,
    rest: result.rest,
  };
};

export const pickDec = (source: string): Result => {
  const { letter: largest, rest } = pickLargest(source);
  const result = pickLargests(largest, rest);

  return {
    result: largest + result.result,
    rest: result.rest,
  };
};

export const pickIncDec = (source: string): Result => {
  const inc = pickInc(source);
  if (inc.rest === '') {
    return inc;
  }

  const dec = pickDec(inc.rest);

  return {
    result: inc.result + dec.result,
    rest: dec.rest,
  };
};

function sortString(s: string): string {
  let output = '';
  let rest = s;

  while (rest !== '') {
    const result = pickIncDec(rest);
    output = output + result.result;
    rest = result.rest;
  }

  return output;
}

export default sortString;
