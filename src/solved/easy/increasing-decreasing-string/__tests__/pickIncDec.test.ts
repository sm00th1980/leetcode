import { pickIncDec } from '..';

test('example 1', () => {
  const input = 'aaaabbbbcccc';

  expect(pickIncDec(input)).toEqual({
    result: 'abccba',
    rest: 'aabbcc',
  });
});

test('example 2', () => {
  const input = 'rat';

  expect(pickIncDec(input)).toEqual({
    result: 'art',
    rest: '',
  });
});

test('example 3', () => {
  const input = 'leetcode';

  expect(pickIncDec(input)).toEqual({
    result: 'cdelote',
    rest: 'e',
  });
});

test('example 4', () => {
  const input = 'ggggggg';

  expect(pickIncDec(input)).toEqual({
    result: 'gg',
    rest: 'ggggg',
  });
});

test('example 5', () => {
  const input = 'spo';

  expect(pickIncDec(input)).toEqual({
    result: 'ops',
    rest: '',
  });
});
