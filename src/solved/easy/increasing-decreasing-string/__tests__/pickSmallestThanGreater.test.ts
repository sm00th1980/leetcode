import { pickSmallestThanGreater } from '..';

test('example 1', () => {
  const letter = 'a';
  const source = 'aaabbbbcccc';

  expect(pickSmallestThanGreater(letter, source)).toEqual({
    letter: 'b',
    rest: 'aaabbbcccc',
  });
});

test('example 2', () => {
  const letter = 'b';
  const source = 'aaabbbbcccc';

  expect(pickSmallestThanGreater(letter, source)).toEqual({
    letter: 'c',
    rest: 'aaabbbbccc',
  });
});

test('example 3', () => {
  const letter = 'c';
  const source = 'aaabbbbcccc';

  expect(pickSmallestThanGreater(letter, source)).toEqual({
    letter: undefined,
    rest: 'aaabbbbcccc',
  });
});
