import { pickDec } from '..';

test('example 1', () => {
  const input = 'aaaabbbbcccc';

  expect(pickDec(input)).toEqual({
    result: 'cba',
    rest: 'aaabbbccc',
  });
});

test('example 2', () => {
  const input = 'rat';

  expect(pickDec(input)).toEqual({
    result: 'tra',
    rest: '',
  });
});

test('example 3', () => {
  const input = 'leetcode';

  expect(pickDec(input)).toEqual({
    result: 'toledc',
    rest: 'ee',
  });
});

test('example 4', () => {
  const input = 'ggggggg';

  expect(pickDec(input)).toEqual({
    result: 'g',
    rest: 'gggggg',
  });
});

test('example 5', () => {
  const input = 'spo';

  expect(pickDec(input)).toEqual({
    result: 'spo',
    rest: '',
  });
});
