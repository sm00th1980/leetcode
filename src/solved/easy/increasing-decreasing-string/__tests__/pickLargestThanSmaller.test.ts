import { pickLargestThanSmaller } from '..';

test('example 1', () => {
  const letter = 'c';
  const source = 'aaaabbbbcccc';

  expect(pickLargestThanSmaller(letter, source)).toEqual({
    letter: 'b',
    rest: 'aaaabbbcccc',
  });
});

test('example 2', () => {
  const letter = 'b';
  const source = 'aaaabbbbcccc';

  expect(pickLargestThanSmaller(letter, source)).toEqual({
    letter: 'a',
    rest: 'aaabbbbcccc',
  });
});

test('example 3', () => {
  const letter = 'a';
  const source = 'aaaabbbbcccc';

  expect(pickLargestThanSmaller(letter, source)).toEqual({
    letter: undefined,
    rest: 'aaaabbbbcccc',
  });
});
