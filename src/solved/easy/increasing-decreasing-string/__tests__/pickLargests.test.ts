import { pickLargests } from '..';

test('example 1', () => {
  const letter = 'a';
  const source = 'aaaabbbbcccc';

  expect(pickLargests(letter, source)).toEqual({
    result: '',
    rest: 'aaaabbbbcccc',
  });
});

test('example 2', () => {
  const letter = 'b';
  const source = 'aaaabbbbcccc';

  expect(pickLargests(letter, source)).toEqual({
    result: 'a',
    rest: 'aaabbbbcccc',
  });
});

test('example 3', () => {
  const letter = 'c';
  const source = 'aaaabbbbcccc';

  expect(pickLargests(letter, source)).toEqual({
    result: 'ba',
    rest: 'aaabbbcccc',
  });
});
