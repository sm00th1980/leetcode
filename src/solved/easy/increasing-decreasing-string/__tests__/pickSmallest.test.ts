import { pickSmallest } from '..';

test('example 1', () => {
  const input = 'aaaabbbbcccc';

  expect(pickSmallest(input)).toEqual({
    letter: 'a',
    rest: 'aaabbbbcccc',
  });
});

test('example 2', () => {
  const input = 'rat';

  expect(pickSmallest(input)).toEqual({
    letter: 'a',
    rest: 'rt',
  });
});

test('example 3', () => {
  const input = 'leetcode';

  expect(pickSmallest(input)).toEqual({
    letter: 'c',
    rest: 'leetode',
  });
});

test('example 4', () => {
  const input = 'ggggggg';

  expect(pickSmallest(input)).toEqual({
    letter: 'g',
    rest: 'gggggg',
  });
});

test('example 5', () => {
  const input = 'spo';

  expect(pickSmallest(input)).toEqual({
    letter: 'o',
    rest: 'sp',
  });
});
