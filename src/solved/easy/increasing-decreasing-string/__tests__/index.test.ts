import sortString from '..';

test('example 1', () => {
  const input = 'aaaabbbbcccc';
  const output = 'abccbaabccba';

  expect(sortString(input)).toEqual(output);
});

test('example 2', () => {
  const input = 'rat';
  const output = 'art';

  expect(sortString(input)).toEqual(output);
});

test('example 3', () => {
  const input = 'leetcode';
  const output = 'cdelotee';

  expect(sortString(input)).toEqual(output);
});

test('example 4', () => {
  const input = 'ggggggg';
  const output = 'ggggggg';

  expect(sortString(input)).toEqual(output);
});

test('example 5', () => {
  const input = 'spo';
  const output = 'ops';

  expect(sortString(input)).toEqual(output);
});
