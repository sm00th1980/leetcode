import { pickLargest } from '..';

test('example 1', () => {
  const input = 'aaaabbbbcccc';

  expect(pickLargest(input)).toEqual({
    letter: 'c',
    rest: 'aaaabbbbccc',
  });
});

test('example 2', () => {
  const input = 'rat';

  expect(pickLargest(input)).toEqual({
    letter: 't',
    rest: 'ra',
  });
});

test('example 3', () => {
  const input = 'leetcode';

  expect(pickLargest(input)).toEqual({
    letter: 't',
    rest: 'leecode',
  });
});

test('example 4', () => {
  const input = 'ggggggg';

  expect(pickLargest(input)).toEqual({
    letter: 'g',
    rest: 'gggggg',
  });
});

test('example 5', () => {
  const input = 'spo';

  expect(pickLargest(input)).toEqual({
    letter: 's',
    rest: 'po',
  });
});
