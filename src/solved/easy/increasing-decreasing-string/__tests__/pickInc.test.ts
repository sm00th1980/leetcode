import { pickInc } from '..';

test('example 1', () => {
  const input = 'aaaabbbbcccc';

  expect(pickInc(input)).toEqual({
    result: 'abc',
    rest: 'aaabbbccc',
  });
});

test('example 2', () => {
  const input = 'rat';

  expect(pickInc(input)).toEqual({
    result: 'art',
    rest: '',
  });
});

test('example 3', () => {
  const input = 'leetcode';

  expect(pickInc(input)).toEqual({
    result: 'cdelot',
    rest: 'ee',
  });
});

test('example 4', () => {
  const input = 'ggggggg';

  expect(pickInc(input)).toEqual({
    result: 'g',
    rest: 'gggggg',
  });
});

test('example 5', () => {
  const input = 'spo';

  expect(pickInc(input)).toEqual({
    result: 'ops',
    rest: '',
  });
});
