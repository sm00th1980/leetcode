import { withoutLetter } from '..';

test('example 1', () => {
  const letter = 'a';
  const source = 'aaaabbbbcccc';

  expect(withoutLetter(letter, source)).toEqual('aaabbbbcccc');
});

test('example 2', () => {
  const letter = 'b';
  const source = 'aaaabbbbcccc';

  expect(withoutLetter(letter, source)).toEqual('aaaabbbcccc');
});

test('example 3', () => {
  const letter = 'c';
  const source = 'aaaabbbbcccc';

  expect(withoutLetter(letter, source)).toEqual('aaaabbbbccc');
});

test('example 4', () => {
  const letter = 'a';
  const source = 'rat';

  expect(withoutLetter(letter, source)).toEqual('rt');
});

test('example 5', () => {
  const letter = 'c';
  const source = 'leetcode';

  expect(withoutLetter(letter, source)).toEqual('leetode');
});
