import { pickSmallests } from '..';

test('example 1', () => {
  const letter = 'a';
  const source = 'aaaabbbbcccc';

  expect(pickSmallests(letter, source)).toEqual({
    result: 'bc',
    rest: 'aaaabbbccc',
  });
});

test('example 2', () => {
  const letter = 'b';
  const source = 'aaaabbbbcccc';

  expect(pickSmallests(letter, source)).toEqual({
    result: 'c',
    rest: 'aaaabbbbccc',
  });
});

test('example 3', () => {
  const letter = 'c';
  const source = 'aaaabbbbcccc';

  expect(pickSmallests(letter, source)).toEqual({
    result: '',
    rest: 'aaaabbbbcccc',
  });
});
