import { repeat } from '../../../utils';

const getSum = (xs: number[]): number => {
  return xs.reduce((acc, x) => acc + x, 0);
};

const normalize = (x: string, y: string): string[] => {
  if (x.length < y.length) {
    return [[repeat(y.length - x.length, '0'), x].join(''), y];
  }

  if (x.length > y.length) {
    return [x, [repeat(x.length - y.length, '0'), y].join('')];
  }

  return [x, y];
};

function hammingDistance(x: number, y: number): number {
  const booleanX = x.toString(2);
  const booleanY = y.toString(2);

  const [normX, normY] = normalize(booleanX, booleanY);
  const diff = normX.split('').map((val, index) => {
    return val !== normY[index] ? 1 : 0;
  });

  return getSum(diff);
}

export default hammingDistance;
