import hammingDistance from '..';

test('example 1', () => {
  const x = 1;
  const y = 4;
  const output = 2;

  expect(hammingDistance(x, y)).toEqual(output);
});
