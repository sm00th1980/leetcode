import { TreeNode } from '../../../utils/structures';
import { isEmpty, flatten } from '../../../utils';

const getValue = (root: TreeNode): number[] => {
  if (isEmpty(root.children)) {
    return [root.val];
  }

  return flatten([...((root.children.map((child) => getValue(child)) as unknown) as number[]), root.val]);
};

function postorder(root: TreeNode | null): number[] {
  if (!root) {
    return [];
  }

  return getValue(root);
}

export default postorder;
