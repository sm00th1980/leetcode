import postorder from '..';
import { treeNode } from '../../../../utils/structures';

test('example 1', () => {
  const root = treeNode(1, [treeNode(3, [treeNode(5), treeNode(6)]), treeNode(2), treeNode(4)]);
  const output = [5, 6, 3, 2, 4, 1];

  expect(postorder(root)).toEqual(output);
});

test('example 2', () => {
  const root = treeNode(1, [
    treeNode(2),
    treeNode(3, [treeNode(6), treeNode(7, [treeNode(11, [treeNode(14)])])]),
    treeNode(4, [treeNode(8, [treeNode(12)])]),
    treeNode(5, [treeNode(9, [treeNode(13)]), treeNode(10)]),
  ]);
  const output = [2, 6, 14, 11, 7, 3, 12, 8, 4, 13, 9, 10, 5, 1];

  expect(postorder(root)).toEqual(output);
});

test('example 3', () => {
  const root = null;
  const output: number[] = [];

  expect(postorder(root)).toEqual(output);
});

test('example 4', () => {
  const root = treeNode(8, [treeNode(1, [treeNode(8), treeNode(5)])]);
  const output = [8, 5, 1, 8];
  expect(postorder(root)).toEqual(output);
});
