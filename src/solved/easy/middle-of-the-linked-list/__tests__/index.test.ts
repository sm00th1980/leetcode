import middleNode from '..';
import { toListNode } from '../../../../utils/structures';

test('example 1', () => {
  const root = toListNode([1, 2, 3, 4, 5]);

  const output = 3;

  expect(middleNode(root!)!.val).toEqual(output);
});

test('example 2', () => {
  const root = toListNode([1, 2, 3, 4, 5, 6]);

  const output = 4;

  expect(middleNode(root!)!.val).toEqual(output);
});

test('example 3', () => {
  const root = toListNode([1, 2]);

  const output = 2;

  expect(middleNode(root!)!.val).toEqual(output);
});

test('example 4', () => {
  const root = toListNode([1]);

  const output = 1;

  expect(middleNode(root!)!.val).toEqual(output);
});
