import { ListNode } from '../../../utils/structures';
import { isNil } from '../../../utils';

const getMiddle = <T>(xs: T[]): T => {
  const middleIndex = Math.floor(xs.length / 2);

  return xs[middleIndex];
};

function middleNode(head: ListNode | null): ListNode | null {
  if (isNil(head)) {
    return null;
  }

  const xs = [head];

  let next = head?.next;
  while (next) {
    xs.push(next);
    next = next.next;
  }

  return getMiddle(xs);
}

export default middleNode;
