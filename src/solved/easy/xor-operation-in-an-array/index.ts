function xorOperation(n: number, start: number): number {
  const array = Array(n)
    .fill(undefined)
    .map((_, index) => start + 2 * index);

  return array.slice(1).reduce((acc, value) => {
    return acc ^ value;
  }, array[0]);
}

export default xorOperation;
