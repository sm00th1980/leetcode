import xorOperation from '..';

test('example 1', () => {
  const n = 5;
  const start = 0;
  const output = 8;

  expect(xorOperation(n, start)).toEqual(output);
});

test('example 2', () => {
  const n = 4;
  const start = 3;
  const output = 8;

  expect(xorOperation(n, start)).toEqual(output);
});

test('example 3', () => {
  const n = 1;
  const start = 7;
  const output = 7;

  expect(xorOperation(n, start)).toEqual(output);
});

test('example 4', () => {
  const n = 10;
  const start = 5;
  const output = 2;

  expect(xorOperation(n, start)).toEqual(output);
});
