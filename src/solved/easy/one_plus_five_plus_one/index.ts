import { isNil } from '../../../utils';

type Plus = (value: number) => number;

export const one = (value: Plus | undefined): number => {
  if (isNil(value)) {
    return 1;
  }

  const plus = (value as unknown) as Plus;
  return plus(1);
};

export const five = (plus: Plus): number => {
  return plus(5);
};

export const plus = (n: number): Plus => {
  return (m) => m + n;
};
