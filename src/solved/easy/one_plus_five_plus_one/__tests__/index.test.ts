import { one, five, plus } from '..';

test('example 1', () => {
  const result = one(plus(five(plus(one(undefined)))));
  const output = 7;

  expect(result).toEqual(output);
});

test('example 2', () => {
  const result = five(plus(one(undefined)));
  const output = 6;

  expect(result).toEqual(output);
});
