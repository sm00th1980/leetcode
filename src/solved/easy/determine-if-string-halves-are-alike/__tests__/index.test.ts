import halvesAreAlike from '..';

test('example 1', () => {
  const input = 'book';
  const output = true;

  expect(halvesAreAlike(input)).toBe(output);
});

test('example 2', () => {
  const input = 'textbook';
  const output = false;

  expect(halvesAreAlike(input)).toBe(output);
});
