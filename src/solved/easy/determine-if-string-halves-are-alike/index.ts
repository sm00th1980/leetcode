const VOWELS = ['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'];

const getVowelsCount = (s: string): number => {
  return s.split('').filter((letter) => VOWELS.includes(letter)).length;
};

function halvesAreAlike(s: string): boolean {
  const middleIndex = s.length / 2;
  const firstHalf = s.slice(0, middleIndex);
  const secondHalf = s.slice(middleIndex);

  return getVowelsCount(firstHalf) === getVowelsCount(secondHalf);
}

export default halvesAreAlike;
