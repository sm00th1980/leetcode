type InitialValue = {
  map: Record<number, number>;
  contains: boolean;
};

function containsDuplicate(nums: number[]): boolean {
  const initialValue: InitialValue = {
    map: {},
    contains: false,
  };

  const result = nums.reduce((acc, value) => {
    if (acc.contains) {
      return acc;
    }

    const count = acc.map[value] || 0;
    if (count === 0) {
      acc.map[value] = 1;
      return acc;
    }

    return { ...acc, contains: true };
  }, initialValue);

  return result.contains;
}

export default containsDuplicate;
