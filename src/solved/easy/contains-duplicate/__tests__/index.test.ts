import containsDuplicate from '..';

test('example 1', () => {
  const nums = [1, 2, 3, 1];
  const output = true;

  expect(containsDuplicate(nums)).toBe(output);
});

test('example 2', () => {
  const nums = [1, 2, 3, 4];
  const output = false;

  expect(containsDuplicate(nums)).toBe(output);
});

test('example 3', () => {
  const nums = [1, 1, 1, 3, 3, 4, 3, 2, 4, 2];
  const output = true;

  expect(containsDuplicate(nums)).toBe(output);
});
