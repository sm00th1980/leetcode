import { sort } from '../../../utils';

const stringComparator = (a: string, b: string): number => a.localeCompare(b);

function isAnagram(s: string, t: string): boolean {
  const sorted1 = sort(stringComparator, s.split('')).join('');
  const sorted2 = sort(stringComparator, t.split('')).join('');

  return sorted1 === sorted2;
}

export default isAnagram;
