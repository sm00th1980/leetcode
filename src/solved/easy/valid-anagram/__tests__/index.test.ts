import isAnagram from '..';

test('example 1', () => {
  const s = 'anagram';
  const t = 'nagaram';
  const output = true;

  expect(isAnagram(s, t)).toBe(output);
});

test('example 2', () => {
  const s = 'rat';
  const t = 'car';
  const output = false;

  expect(isAnagram(s, t)).toBe(output);
});
