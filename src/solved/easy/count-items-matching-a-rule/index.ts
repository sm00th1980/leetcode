const getValue = (ruleKey: string, item: string[]) => {
  if (ruleKey === 'type') {
    return item[0];
  }

  if (ruleKey === 'color') {
    return item[1];
  }

  return item[2];
};

function countMatches(items: string[][], ruleKey: string, ruleValue: string): number {
  return items.filter((item) => getValue(ruleKey, item) === ruleValue).length;
}

export default countMatches;
