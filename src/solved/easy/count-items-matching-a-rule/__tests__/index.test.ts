import countMatches from '..';

test('example 1', () => {
  const items = [
    ['phone', 'blue', 'pixel'],
    ['computer', 'silver', 'lenovo'],
    ['phone', 'gold', 'iphone'],
  ];
  const ruleKey = 'color';
  const ruleValue = 'silver';

  expect(countMatches(items, ruleKey, ruleValue)).toBe(1);
});

test('example 2', () => {
  const items = [
    ['phone', 'blue', 'pixel'],
    ['computer', 'silver', 'phone'],
    ['phone', 'gold', 'iphone'],
  ];
  const ruleKey = 'type';
  const ruleValue = 'phone';

  expect(countMatches(items, ruleKey, ruleValue)).toBe(2);
});
