function sortArrayByParity(A: number[]): number[] {
  const result: number[] = [];
  A.forEach((value) => {
    if (value % 2 === 0) {
      result.unshift(value);
    } else {
      result.push(value);
    }
  });

  return result;
}

export default sortArrayByParity;
