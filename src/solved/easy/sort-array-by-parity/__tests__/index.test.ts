import sortArrayByParity from '..';

test('example 1', () => {
  const input = [3, 1, 2, 4];
  const output = [4, 2, 3, 1];

  expect(sortArrayByParity(input)).toEqual(output);
});
