import { isNil } from '../../../utils';

const memoize = (fn: (i: number) => number) => {
  const cache: Array<number> = [];
  return (n: number) => {
    const cachedValue = cache[n];
    if (isNil(cachedValue)) {
      const value = fn(n);
      cache[n] = value;
      return value;
    }

    return cachedValue;
  };
};

const memoizedClimbStairs = memoize(climbStairs);

function climbStairs(n: number): number {
  if (n === 1) {
    return 1;
  }

  if (n === 2) {
    return 2;
  }

  return memoizedClimbStairs(n - 2) + memoizedClimbStairs(n - 1);
}

export default climbStairs;
