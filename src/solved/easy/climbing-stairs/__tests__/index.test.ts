import climbStairs from '..';

test('example 1', () => {
  const n = 1;
  const output = 1;

  // Explanation: There is one way to climb to the top.
  // 1. 1 step

  expect(climbStairs(n)).toEqual(output);
});

test('example 2', () => {
  const n = 2;
  const output = 2;

  // Explanation: There are two ways to climb to the top.
  // 1. 1 step + 1 step
  // 2. 2 steps

  expect(climbStairs(n)).toEqual(output);
});

test('example 3', () => {
  const n = 3;
  const output = 3;

  // Explanation: There are three ways to climb to the top.
  // 1. 1 step + 1 step + 1 step
  // 2. 1 step + 2 steps
  // 3. 2 steps + 1 step

  expect(climbStairs(n)).toEqual(output);
});

test('example 4', () => {
  const n = 4;
  const output = 5;

  // Explanation: There are five ways to climb to the top.
  // 1. 1 step + 1 step + 1 step + 1 step
  // 2. 1 step + 1 step + 2 steps
  // 3. 1 step + 2 steps + 1 step
  // 4. 2 steps + 2 steps
  // 5. 2 steps + 1 step + 1 step

  expect(climbStairs(n)).toEqual(output);
});

test('example 5', () => {
  const n = 5;
  const output = 8;

  expect(climbStairs(n)).toEqual(output);
});

test('example 5', () => {
  const n = 44;
  const output = 1134903170;

  expect(climbStairs(n)).toEqual(output);
});
