class RecentCounter {
  recents: number[];

  constructor() {
    this.recents = [];
  }

  ping(t: number): number {
    this.recents.push(t);

    const start = t - 3000;
    const end = t;
    return this.recents.reduce((acc, value) => {
      if (start <= value && value <= end) {
        return acc + 1;
      }

      return acc;
    }, 0);
  }
}

export default RecentCounter;

/**
 * Your RecentCounter object will be instantiated and called as such:
 * var obj = new RecentCounter()
 * var param_1 = obj.ping(t)
 */
