import sortedArrayToBST from '..';
import { BinaryTreeNode, binaryTreeNode } from '../../../../utils/structures';

test('example 1', () => {
  const input = [-10, -3, 0, 5, 9];
  const output = new BinaryTreeNode(
    0,
    new BinaryTreeNode(-3, binaryTreeNode(-10)),
    new BinaryTreeNode(9, binaryTreeNode(5)),
  );

  expect(sortedArrayToBST(input)).toEqual(output);
});

test('example 2', () => {
  const input = [-3, 0, 5];
  const output = new BinaryTreeNode(0, binaryTreeNode(-3), binaryTreeNode(5));

  expect(sortedArrayToBST(input)).toEqual(output);
});

test('example 3', () => {
  const input = [-10, -3, 0, 5];
  const output = new BinaryTreeNode(0, new BinaryTreeNode(-3, binaryTreeNode(-10)), binaryTreeNode(5));

  expect(sortedArrayToBST(input)).toEqual(output);
});
