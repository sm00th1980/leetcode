import { split } from '..';

test('example 1', () => {
  const input = [-10, -3, 0, 5, 9];
  const output = {
    left: [-10, -3],
    middle: 0,
    right: [5, 9],
  };

  expect(split(input)).toEqual(output);
});

test('example 2', () => {
  const input = [-10, -3, 0, 5];
  const output = {
    left: [-10, -3],
    middle: 0,
    right: [5],
  };

  expect(split(input)).toEqual(output);
});

test('example 3', () => {
  const input = [1, 2];
  const output = {
    left: [1],
    middle: 2,
    right: [],
  };

  expect(split(input)).toEqual(output);
});

test('example 4', () => {
  const input = [1];
  const output = {
    left: [],
    middle: 1,
    right: [],
  };

  expect(split(input)).toEqual(output);
});
