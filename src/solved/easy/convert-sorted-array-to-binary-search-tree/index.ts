import { BinaryTreeNode } from '../../../utils/structures';
import { Option } from '../../../utils/types';

interface Result {
  left: number[];
  middle: number;
  right: number[];
}

export const split = (xs: number[]): Result => {
  const middleIndex = xs.length % 2 === 1 ? Math.floor(xs.length / 2) : Math.ceil(xs.length / 2);

  return {
    left: xs.slice(0, middleIndex),
    middle: xs[middleIndex],
    right: xs.slice(middleIndex + 1),
  };
};

function sortedArrayToBST(nums: number[]): Option<BinaryTreeNode> {
  if (nums.length === 0) {
    return null;
  }

  const { left, middle, right } = split(nums);

  return new BinaryTreeNode(middle, sortedArrayToBST(left), sortedArrayToBST(right));
}

export default sortedArrayToBST;
