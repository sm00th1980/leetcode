import { BinaryTreeNode as TreeNode } from '../../../utils/structures';
import { isNil } from '../../../utils';

type NullableTreeNode = TreeNode | null;

const getHeight = (root: TreeNode | null, height: number): number => {
  if (isNil(root)) {
    return height;
  }

  const leftHeight = getHeight(root?.left as NullableTreeNode, height + 1);
  const rightHeight = getHeight(root?.right as NullableTreeNode, height + 1);

  return Math.max(leftHeight, rightHeight);
};

function isBalanced(root: TreeNode | null): boolean {
  if (isNil(root)) {
    return true;
  }

  const leftBalanced = isBalanced(root?.left as NullableTreeNode);
  const rightBalanced = isBalanced(root?.right as NullableTreeNode);

  if (leftBalanced && rightBalanced) {
    const leftHeight = getHeight(root?.left as NullableTreeNode, 1);
    const rightHeight = getHeight(root?.right as NullableTreeNode, 1);

    if (leftHeight === rightHeight) {
      return true;
    }

    if (leftHeight + 1 === rightHeight) {
      return true;
    }

    if (leftHeight === rightHeight + 1) {
      return true;
    }
  }

  return false;
}

export default isBalanced;
