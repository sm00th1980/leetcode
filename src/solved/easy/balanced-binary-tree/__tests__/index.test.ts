import { binaryTreeNode } from '../../../../utils/structures';
import isBalanced from '..';

test('example 1', () => {
  const root = binaryTreeNode(3, binaryTreeNode(9), binaryTreeNode(20, binaryTreeNode(15), binaryTreeNode(7)));
  const output = true;

  expect(isBalanced(root)).toBe(output);
});

test('example 2', () => {
  const root = binaryTreeNode(
    1,
    binaryTreeNode(2, binaryTreeNode(3, binaryTreeNode(4), binaryTreeNode(4)), binaryTreeNode(3)),
    binaryTreeNode(2),
  );
  const output = false;

  expect(isBalanced(root)).toBe(output);
});

test('example 3', () => {
  const root = null;
  const output = true;

  expect(isBalanced(root)).toBe(output);
});

test('example 4', () => {
  const root = binaryTreeNode(1, binaryTreeNode(2));
  const output = true;

  expect(isBalanced(root)).toBe(output);
});

test('example 5', () => {
  const root = binaryTreeNode(1, binaryTreeNode(2, binaryTreeNode(3)));
  const output = false;

  expect(isBalanced(root)).toBe(output);
});

test('example 6', () => {
  const root = binaryTreeNode(1, binaryTreeNode(2, binaryTreeNode(4)), binaryTreeNode(3));
  const output = true;

  expect(isBalanced(root)).toBe(output);
});

test('example 7', () => {
  const root = binaryTreeNode(1, binaryTreeNode(2, binaryTreeNode(4, binaryTreeNode(5))), binaryTreeNode(3));
  const output = false;

  expect(isBalanced(root)).toBe(output);
});

test('example 8', () => {
  const root = binaryTreeNode(
    1,
    binaryTreeNode(2, binaryTreeNode(3), binaryTreeNode(4)),
    binaryTreeNode(2, null, binaryTreeNode(3, null, binaryTreeNode(4))),
  );
  const output = false;

  expect(isBalanced(root)).toBe(output);
});
