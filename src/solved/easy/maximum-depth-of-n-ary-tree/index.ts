import { TreeNode } from '../../../utils/structures';
import { isNil } from '../../../utils';

function maxDepth(root: TreeNode): number {
  if (isNil(root)) {
    return 0;
  }

  if (root.children.length === 0) {
    return 1;
  }

  const depths = root.children.map((child) => maxDepth(child) + 1);
  return Math.max(...depths);
}

export default maxDepth;
