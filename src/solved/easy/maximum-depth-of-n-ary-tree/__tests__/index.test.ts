import maxDepth from '..';
import { TreeNode, treeNode } from '../../../../utils/structures';

test('example 1', () => {
  const root = new TreeNode(1, [new TreeNode(3, [treeNode(5), treeNode(6)]), treeNode(2), treeNode(4)]);
  const output = 3;

  expect(maxDepth(root)).toEqual(output);
});

test('example 2', () => {
  const root = new TreeNode(1, [
    treeNode(2),
    new TreeNode(3, [treeNode(6), new TreeNode(7, [new TreeNode(11, [treeNode(14)])])]),
    new TreeNode(4, [new TreeNode(8, [treeNode(12)])]),
    new TreeNode(5, [new TreeNode(9, [treeNode(13)]), treeNode(10)]),
  ]);
  const output = 5;

  expect(maxDepth(root)).toEqual(output);
});
