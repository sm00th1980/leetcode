const invert = (value: number): number => {
  return value === 1 ? 0 : 1;
};

const reverse = (xs: number[]): number[] => {
  return [...xs].reverse();
};

function flipAndInvertImage(A: number[][]): number[][] {
  return A.map((row) => reverse(row)).map((row) => row.map(invert));
}

export default flipAndInvertImage;
