import flipAndInvertImage from '..';

test('example 1', () => {
  const input = [
    [1, 1, 0],
    [1, 0, 1],
    [0, 0, 0],
  ];
  const output = [
    [1, 0, 0],
    [0, 1, 0],
    [1, 1, 1],
  ];

  expect(flipAndInvertImage(input)).toEqual(output);
});

test('example 2', () => {
  const input = [
    [1, 1, 0, 0],
    [1, 0, 0, 1],
    [0, 1, 1, 1],
    [1, 0, 1, 0],
  ];
  const output = [
    [1, 1, 0, 0],
    [0, 1, 1, 0],
    [0, 0, 0, 1],
    [1, 0, 1, 0],
  ];

  expect(flipAndInvertImage(input)).toEqual(output);
});
