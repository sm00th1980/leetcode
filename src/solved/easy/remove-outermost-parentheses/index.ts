import { chunks } from '../../../utils';

const isPrimitive = (xs: string): boolean => {
  interface Result {
    L: number;
    R: number;
  }

  const initialValue: Result = {
    L: 0,
    R: 0,
  };

  const { L, R } = xs.split('').reduce((acc, letter) => {
    if (letter === '(') {
      return { ...acc, L: acc.L + 1 };
    }

    if (letter === ')') {
      return { ...acc, R: acc.R + 1 };
    }

    return acc;
  }, initialValue);

  return L === R;
};

const extract = (str: string): string => {
  const xs = str.split('');
  const firstIndex = 0;
  const lastIndex = xs.length - 1;

  return xs.filter((_, index) => index !== firstIndex && index !== lastIndex).join('');
};

function removeOuterParentheses(S: string): string {
  interface Result {
    str: string;
    primitives: string[];
  }

  const initialValue: Result = {
    str: '',
    primitives: [],
  };

  const { primitives } = chunks(2, S.split(''))
    .map((chunk) => chunk.join(''))
    .reduce((acc, chunk) => {
      const { str } = acc;
      const newString = `${str}${chunk}`;
      if (isPrimitive(newString)) {
        return {
          str: '',
          primitives: [...acc.primitives, newString],
        };
      }

      return { ...acc, str: newString };
    }, initialValue);

  return primitives.map(extract).join('');
}

export default removeOuterParentheses;
