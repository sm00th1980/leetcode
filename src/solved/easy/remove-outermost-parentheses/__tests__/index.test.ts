import removeOuterParentheses from '..';

test('example 1', () => {
  const input = '(()())(())';
  const output = '()()()';

  expect(removeOuterParentheses(input)).toEqual(output);
});

test('example 2', () => {
  const input = '(()())(())(()(()))';
  const output = '()()()()(())';

  expect(removeOuterParentheses(input)).toEqual(output);
});

test('example 3', () => {
  const input = '()()';
  const output = '';

  expect(removeOuterParentheses(input)).toEqual(output);
});
