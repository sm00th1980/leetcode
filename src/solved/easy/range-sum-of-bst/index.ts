import { isNil } from '../../../utils';

export class TreeNode {
  val: number;
  left: TreeNode | null;
  right: TreeNode | null;
  constructor(val?: number, left?: TreeNode | null, right?: TreeNode | null) {
    this.val = val === undefined ? 0 : val;
    this.left = left === undefined ? null : left;
    this.right = right === undefined ? null : right;
  }
}

export const node = (value: number): TreeNode => {
  return new TreeNode(value, null, null);
};

function rangeSumBST(root: TreeNode | null, L: number, R: number): number {
  if (isNil(root)) {
    return 0;
  }

  if (root!.val >= L && root!.val <= R) {
    return root!.val + rangeSumBST(root!.left, L, R) + rangeSumBST(root!.right, L, R);
  }

  return rangeSumBST(root!.left, L, R) + rangeSumBST(root!.right, L, R);
}

export default rangeSumBST;
