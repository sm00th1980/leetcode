import rangeSumBST, { TreeNode, node } from '..';

test('example 1', () => {
  const root = new TreeNode(10, new TreeNode(5, node(3), node(7)), new TreeNode(15, null, node(18)));

  const L = 7;
  const R = 15;

  const output = 32;

  expect(rangeSumBST(root, L, R)).toEqual(output);
});

test('example 2', () => {
  const root = new TreeNode(
    10,
    new TreeNode(5, new TreeNode(3, node(1), null), new TreeNode(7, node(6), null)),
    new TreeNode(15, node(13), node(18)),
  );

  const L = 6;
  const R = 10;

  const output = 23;

  expect(rangeSumBST(root, L, R)).toEqual(output);
});
