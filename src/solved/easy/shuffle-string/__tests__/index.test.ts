import restoreString from '..';

test('example 1', () => {
  const s = 'codeleet';
  const indices = [4, 5, 6, 7, 0, 2, 1, 3];
  const output = 'leetcode';

  expect(restoreString(s, indices)).toEqual(output);
});

test('example 2', () => {
  const s = 'abc';
  const indices = [0, 1, 2];
  const output = 'abc';

  expect(restoreString(s, indices)).toEqual(output);
});

test('example 3', () => {
  const s = 'aiohn';
  const indices = [3, 1, 4, 2, 0];
  const output = 'nihao';

  expect(restoreString(s, indices)).toEqual(output);
});

test('example 4', () => {
  const s = 'aaiougrt';
  const indices = [4, 0, 2, 6, 7, 3, 1, 5];
  const output = 'arigatou';

  expect(restoreString(s, indices)).toEqual(output);
});

test('example 5', () => {
  const s = 'art';
  const indices = [1, 0, 2];
  const output = 'rat';

  expect(restoreString(s, indices)).toEqual(output);
});
