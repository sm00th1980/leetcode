function restoreString(s: string, indices: number[]): string {
  const initialValue: string[] = [];
  return s
    .split('')
    .reduce((acc, letter, index) => {
      const idx = indices[index];
      acc[idx] = letter;
      return acc;
    }, initialValue)
    .join('');
}

export default restoreString;
