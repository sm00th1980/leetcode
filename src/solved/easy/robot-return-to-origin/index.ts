interface Result {
  L: number;
  U: number;
}

const getCounts = (xs: string): Result => {
  const initialValue = { L: 0, U: 0 };

  return xs.split('').reduce((acc, letter) => {
    if (letter === 'L') {
      return { ...acc, L: acc.L + 1 };
    }

    if (letter === 'R') {
      return { ...acc, L: acc.L - 1 };
    }

    if (letter === 'U') {
      return { ...acc, U: acc.U + 1 };
    }

    if (letter === 'D') {
      return { ...acc, U: acc.U - 1 };
    }

    return acc;
  }, initialValue);
};

function judgeCircle(moves: string): boolean {
  const { L, U } = getCounts(moves);

  return L === 0 && U === 0;
}

export default judgeCircle;
