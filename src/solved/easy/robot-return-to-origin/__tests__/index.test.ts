import judgeCircle from '..';

test('example 1', () => {
  const moves = 'UD';
  const output = true;

  expect(judgeCircle(moves)).toEqual(output);
});

test('example 2', () => {
  const moves = 'LL';
  const output = false;

  expect(judgeCircle(moves)).toEqual(output);
});

test('example 3', () => {
  const moves = 'RRDD';
  const output = false;

  expect(judgeCircle(moves)).toEqual(output);
});

test('example 4', () => {
  const moves = 'LDRRLRUULR';
  const output = false;

  expect(judgeCircle(moves)).toEqual(output);
});

test('example 5', () => {
  const moves = 'UDLR';
  const output = true;

  expect(judgeCircle(moves)).toEqual(output);
});

test('example 6', () => {
  const moves = 'URDL';
  const output = true;

  expect(judgeCircle(moves)).toEqual(output);
});

test('example 7', () => {
  const moves = 'URRDLL';
  const output = true;

  expect(judgeCircle(moves)).toEqual(output);
});
