function fizzBuzz(n: number): string[] {
  return Array(n)
    .fill(undefined)
    .map((_, index) => index + 1)
    .map((value) => {
      if (value % 3 === 0 && value % 5 === 0) {
        return 'FizzBuzz';
      }

      if (value % 3 === 0) {
        return 'Fizz';
      }

      if (value % 5 === 0) {
        return 'Buzz';
      }

      return value.toString();
    });
}

export default fizzBuzz;
