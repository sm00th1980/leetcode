import fizzBuzz from '..';

test('example 1', () => {
  const n = 15;
  const output = [
    '1',
    '2',
    'Fizz',
    '4',
    'Buzz',
    'Fizz',
    '7',
    '8',
    'Fizz',
    'Buzz',
    '11',
    'Fizz',
    '13',
    '14',
    'FizzBuzz',
  ];

  expect(fizzBuzz(n)).toEqual(output);
});
