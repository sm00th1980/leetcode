import searchBST, { TreeNode, leaf } from '..';

test('example 1', () => {
  const root = new TreeNode(4, new TreeNode(2, leaf(1), leaf(3)), leaf(7));
  const value = 2;
  const output = new TreeNode(2, leaf(1), leaf(3));

  expect(searchBST(root, value)).toEqual(output);
});

test('example 2', () => {
  const root = new TreeNode(4, new TreeNode(2, leaf(1), leaf(3)), leaf(7));
  const value = 5;
  const output = null;

  expect(searchBST(root, value)).toBe(output);
});

test('example 3', () => {
  const root = new TreeNode(
    3,
    new TreeNode(4, new TreeNode(-7, leaf(-7), null), new TreeNode(-6, new TreeNode(-5, leaf(-4), null), null)),
    leaf(5),
  );
  const value = -4;
  const output = leaf(-4);

  expect(searchBST(root, value)).toEqual(output);
});
