import { isNil } from '../../../utils';
import { Option } from '../../../utils/types';

export class TreeNode {
  val: number;
  left: Option<TreeNode>;
  right: Option<TreeNode>;
  constructor(val?: number, left?: Option<TreeNode>, right?: Option<TreeNode>) {
    this.val = val === undefined ? 0 : val;
    this.left = left === undefined ? null : left;
    this.right = right === undefined ? null : right;
  }
}

export const leaf = (value: number): TreeNode => {
  return new TreeNode(value, null, null);
};

function searchBST(root: Option<TreeNode>, val: number): Option<TreeNode> {
  if (isNil(root)) {
    return null;
  }

  if (root?.val === val) {
    return root;
  }

  const valInLeft = searchBST(root?.left, val);
  if (!isNil(valInLeft)) {
    return valInLeft;
  }

  return searchBST(root?.right, val);
}

export default searchBST;
