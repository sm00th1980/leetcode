function repeatedNTimes(A: number[]): number {
  const sortedXs = A.sort((a, b) => a - b);
  return sortedXs.find((x, index) => x === sortedXs[index - 1] || x === sortedXs[index + 1])!;
}

export default repeatedNTimes;
