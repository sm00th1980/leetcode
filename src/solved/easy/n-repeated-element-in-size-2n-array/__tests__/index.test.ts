import repeatedNTimes from '..';

test('example 1', () => {
  const input = [1, 2, 3, 3];
  const output = 3;

  expect(repeatedNTimes(input)).toEqual(output);
});

test('example 2', () => {
  const input = [2, 1, 2, 5, 3, 2];
  const output = 2;

  expect(repeatedNTimes(input)).toEqual(output);
});

test('example 3', () => {
  const input = [5, 1, 5, 2, 5, 3, 5, 4];
  const output = 5;

  expect(repeatedNTimes(input)).toEqual(output);
});
