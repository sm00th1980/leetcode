import preorder from '..';
import { treeNode } from '../../../../utils/structures';

test('example 1', () => {
  const root = treeNode(1, [treeNode(3, [treeNode(5), treeNode(6)]), treeNode(2), treeNode(4)]);
  const output = [1, 3, 5, 6, 2, 4];

  expect(preorder(root)).toEqual(output);
});

test('example 2', () => {
  const root = treeNode(1, [
    treeNode(2),
    treeNode(3, [treeNode(6), treeNode(7, [treeNode(11, [treeNode(14)])])]),
    treeNode(4, [treeNode(8, [treeNode(12)])]),
    treeNode(5, [treeNode(9, [treeNode(13)]), treeNode(10)]),
  ]);
  const output = [1, 2, 3, 6, 7, 11, 14, 4, 8, 12, 5, 9, 13, 10];

  expect(preorder(root)).toEqual(output);
});

test('example 3', () => {
  const root = null;
  const output: number[] = [];

  expect(preorder(root)).toEqual(output);
});

test('example 4', () => {
  const root = treeNode(8, [treeNode(1, [treeNode(8), treeNode(5)])]);
  const output = [8, 1, 8, 5];
  expect(preorder(root)).toEqual(output);
});
