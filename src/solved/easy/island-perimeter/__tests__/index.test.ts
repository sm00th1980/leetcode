import islandPerimeter from '..';

test('example 1', () => {
  const grid = [
    [0, 1, 0, 0],
    [1, 1, 1, 0],
    [0, 1, 0, 0],
    [1, 1, 0, 0],
  ];
  const output = 16;

  expect(islandPerimeter(grid)).toEqual(output);
});

test('example 2', () => {
  const grid = [[1]];
  const output = 4;

  expect(islandPerimeter(grid)).toEqual(output);
});

test('example 3', () => {
  const grid = [[1, 0]];
  const output = 4;

  expect(islandPerimeter(grid)).toEqual(output);
});
