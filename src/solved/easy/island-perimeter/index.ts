import { getSum } from '../../../utils';

const hasLandAbove = (x: number, y: number, grid: number[][]): number => {
  const cellAbove = (grid[x] || [])[y - 1];
  return isLand(cellAbove) ? 0 : 1;
};

const hasLandBelow = (x: number, y: number, grid: number[][]): number => {
  const cellBelow = (grid[x] || [])[y + 1];
  return isLand(cellBelow) ? 0 : 1;
};

const hasLandLeft = (x: number, y: number, grid: number[][]): number => {
  const cellLeft = (grid[x - 1] || [])[y];
  return isLand(cellLeft) ? 0 : 1;
};

const hasLandRight = (x: number, y: number, grid: number[][]): number => {
  const cellRight = (grid[x + 1] || [])[y];
  return isLand(cellRight) ? 0 : 1;
};

const isLand = (cell: number): boolean => {
  return cell === 1;
};

function islandPerimeter(grid: number[][]): number {
  return grid.reduce((accRow, row, rowIndex) => {
    return row.reduce((accColumn, cell, columnIndex) => {
      if (isLand(cell)) {
        const around = [
          hasLandAbove(rowIndex, columnIndex, grid),
          hasLandBelow(rowIndex, columnIndex, grid),
          hasLandLeft(rowIndex, columnIndex, grid),
          hasLandRight(rowIndex, columnIndex, grid),
        ];

        return getSum(around) + accColumn;
      }

      return accColumn;
    }, accRow);
  }, 0);
}

export default islandPerimeter;
