import interpret from '..';

test('example 1', () => {
  const command = 'G()(al)';
  const output = 'Goal';

  expect(interpret(command)).toEqual(output);
});

test('example 2', () => {
  const command = 'G()()()()(al)';
  const output = 'Gooooal';

  expect(interpret(command)).toEqual(output);
});

test('example 3', () => {
  const command = '(al)G(al)()()G';
  const output = 'alGalooG';

  expect(interpret(command)).toEqual(output);
});
