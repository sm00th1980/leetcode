import { compose } from '../../../utils';

const replace = (from: string, to: string) => (input: string): string => {
  return input.split(from).join(to);
};

function interpret(command: string): string {
  return compose(replace('()', 'o'), replace('(al)', 'al'))(command);
}

export default interpret;
