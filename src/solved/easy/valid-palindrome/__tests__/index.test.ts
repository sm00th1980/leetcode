import isPalindrome from '..';

test('example 1', () => {
  const input = 'A man, a plan, a canal: Panama';
  const output = true;

  expect(isPalindrome(input)).toBe(output);
});

test('example 2', () => {
  const input = 'race a car';
  const output = false;

  expect(isPalindrome(input)).toBe(output);
});

test('example 3', () => {
  const input = '';
  const output = true;

  expect(isPalindrome(input)).toBe(output);
});
