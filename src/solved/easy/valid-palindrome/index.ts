import { isDigit, isLetter } from '../../../utils';

function isPalindrome(s: string): boolean {
  const onlyDigitsAndLetters = s
    .toLowerCase()
    .split('')
    .filter((v) => isDigit(v) || isLetter(v));

  return onlyDigitsAndLetters.reduce<boolean>((acc, value, index, array) => {
    if (!acc) {
      return false;
    }

    const reversedIndex = onlyDigitsAndLetters.length - index - 1;

    return value === array[reversedIndex];
  }, true);
}

export default isPalindrome;
