import { ListNode, listNode } from '../../../utils/structures';
import { isNil, sort, head, tail, isEmpty } from '../../../utils';
import { Option } from '../../../utils/types';

const toValues = (head: Option<ListNode>, result: number[] = []): number[] => {
  if (isNil(head)) {
    return result;
  }

  const newResult = [...result, head!.val];

  return toValues(head!.next, newResult);
};

const toListNode = (xs: number[]): Option<ListNode> => {
  if (isEmpty(xs)) {
    return null;
  }

  return listNode(head(xs), toListNode(tail(xs)));
};

function mergeTwoLists(list1: Option<ListNode>, list2: Option<ListNode>): Option<ListNode> {
  const values1 = toValues(list1);
  const values2 = toValues(list2);

  const combinedResult = sort((a, b) => a - b, [...values1, ...values2]);

  return toListNode(combinedResult);
}

export default mergeTwoLists;
