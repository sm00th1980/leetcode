import mergeTwoLists from '..';
import { listNode } from '../../../../utils/structures';

test('example 1', () => {
  const list1 = listNode(1, listNode(2, listNode(4)));
  const list2 = listNode(1, listNode(3, listNode(4)));

  const output = listNode(1, listNode(1, listNode(2, listNode(3, listNode(4, listNode(4))))));

  expect(mergeTwoLists(list1, list2)).toEqual(output);
});

test('example 2', () => {
  const list1 = null;
  const list2 = null;

  const output = null;

  expect(mergeTwoLists(list1, list2)).toEqual(output);
});

test('example 3', () => {
  const list1 = null;
  const list2 = listNode(0);

  const output = listNode(0);

  expect(mergeTwoLists(list1, list2)).toEqual(output);
});
