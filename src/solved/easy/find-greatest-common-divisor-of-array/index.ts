import { getMax, getMin } from '../../../utils';

function findGCD(nums: number[]): number {
  const max = getMax(nums) || 0;
  const min = getMin(nums) || 0;
  const divisors = Array(max)
    .fill(undefined)
    .map((_, index) => index + 1);

  return divisors.reduce((acc, divisor) => {
    return min % divisor === 0 && max % divisor === 0 && acc < divisor ? divisor : acc;
  }, 1);
}

export default findGCD;
