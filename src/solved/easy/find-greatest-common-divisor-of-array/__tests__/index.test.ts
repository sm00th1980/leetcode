import findGCD from '..';

test('example 1', () => {
  const input = [2, 5, 6, 9, 10];
  const output = 2;

  expect(findGCD(input)).toEqual(output);
});

test('example 2', () => {
  const input = [7, 5, 6, 8, 3];
  const output = 1;

  expect(findGCD(input)).toEqual(output);
});

test('example 3', () => {
  const input = [3, 3];
  const output = 3;

  expect(findGCD(input)).toEqual(output);
});
