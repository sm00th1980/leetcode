import sortArrayByParityII from '..';

test('example 1', () => {
  const input = [4, 2, 5, 7];
  const output = [4, 5, 2, 7];

  expect(sortArrayByParityII(input)).toEqual(output);
});
