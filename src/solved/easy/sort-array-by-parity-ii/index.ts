function sortArrayByParityII(A: number[]): number[] {
  const evens = A.filter((value) => value % 2 === 0);
  const odds = A.filter((value) => value % 2 !== 0);

  return [...Array(A.length)].map((_, index) => {
    if (index % 2 === 0) {
      return evens.shift() as number;
    } else {
      return odds.shift() as number;
    }
  });
}

export default sortArrayByParityII;
