import { sort, head } from '../../../utils';

function maxProduct(nums: number[]): number {
  const descend = (a: number, b: number) => b - a;

  const sorted = sort(descend, nums);

  const first = head(sorted);
  const second = sorted[1];

  return (first - 1) * (second - 1);
}

export default maxProduct;
