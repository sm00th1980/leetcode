import maxProduct from '..';

test('example 1', () => {
  const input = [3, 4, 5, 2];
  const output = 12;

  expect(maxProduct(input)).toEqual(output);
});

test('example 2', () => {
  const input = [1, 5, 4, 5];
  const output = 16;

  expect(maxProduct(input)).toEqual(output);
});

test('example 3', () => {
  const input = [3, 7];
  const output = 12;

  expect(maxProduct(input)).toEqual(output);
});
