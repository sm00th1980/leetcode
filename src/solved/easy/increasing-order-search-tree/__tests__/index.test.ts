import increasingBST from '..';
import { BinaryTreeNode, binaryTreeNode } from '../../../../utils/structures';

test('example 1', () => {
  const input = new BinaryTreeNode(
    5,
    new BinaryTreeNode(3, new BinaryTreeNode(2, binaryTreeNode(1)), binaryTreeNode(4)),
    new BinaryTreeNode(6, null, new BinaryTreeNode(8, binaryTreeNode(7), binaryTreeNode(9))),
  );

  const output = new BinaryTreeNode(
    1,
    null,
    new BinaryTreeNode(
      2,
      null,
      new BinaryTreeNode(
        3,
        null,
        new BinaryTreeNode(
          4,
          null,
          new BinaryTreeNode(
            5,
            null,
            new BinaryTreeNode(6, null, new BinaryTreeNode(7, null, new BinaryTreeNode(8, null, binaryTreeNode(9)))),
          ),
        ),
      ),
    ),
  );

  expect(increasingBST(input)).toEqual(output);
});
