import { isNil, head, tail } from '../../../utils';
import { BinaryTreeNode } from '../../../utils/structures';
import { Option } from '../../../utils/types';

const getValues = (root: Option<BinaryTreeNode>, values: Set<number>): number[] => {
  if (isNil(root)) {
    return Array.from(values);
  }

  values.add(root!.val);

  const leftValues = getValues(root?.left, values);
  const rightValues = getValues(root?.right, values);

  leftValues.forEach((value) => {
    values.add(value);
  });

  rightValues.forEach((value) => {
    values.add(value);
  });

  return Array.from(values);
};

const makeBST = (xs: number[]): Option<BinaryTreeNode> => {
  if (xs.length <= 0) {
    return null;
  }

  return new BinaryTreeNode(head(xs), null, makeBST(tail(xs)));
};

function increasingBST(root: Option<BinaryTreeNode>): Option<BinaryTreeNode> {
  if (isNil(root)) {
    return null;
  }

  const values = getValues(root, new Set<number>()).sort((a, b) => a - b);
  return makeBST(values);
}

export default increasingBST;
