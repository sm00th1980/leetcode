const getMax = (xs: number[]): number => {
  return xs.reduce((acc, x) => {
    if (x > acc) {
      return x;
    }

    return acc;
  }, 0);
};

function kidsWithCandies(candies: number[], extraCandies: number): boolean[] {
  const maxCandies = getMax(candies);
  return candies.map((x) => x + extraCandies >= maxCandies);
}

export default kidsWithCandies;
