function maximum69Number(num: number): number {
  const str = num.toString().split('');
  const firstIndex = str.findIndex((x) => x === '6');
  if (firstIndex === -1) {
    return num;
  }

  return +(str.slice(0, firstIndex).join('') + '9' + str.slice(firstIndex + 1).join(''));
}

export default maximum69Number;
