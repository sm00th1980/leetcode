import maximum69Number from '..';

test('example 1', () => {
  const input = 9669;
  const output = 9969;

  expect(maximum69Number(input)).toEqual(output);
});

test('example 2', () => {
  const input = 9996;
  const output = 9999;

  expect(maximum69Number(input)).toEqual(output);
});

test('example 3', () => {
  const input = 9999;
  const output = 9999;

  expect(maximum69Number(input)).toEqual(output);
});
