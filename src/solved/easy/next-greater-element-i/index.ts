interface Result {
  found: boolean;
  next: number;
}

const findNextGreater = (x: number, xs: number[]): number => {
  const index = xs.findIndex((val) => val === x);
  const rightSlice = xs.slice(index);

  const initialValue: Result = {
    found: false,
    next: -1,
  };

  const result = rightSlice.reduce((acc, value) => {
    const { found } = acc;
    if (found) {
      return acc;
    }

    if (value > x) {
      return { found: true, next: value };
    }

    return acc;
  }, initialValue);

  return result.next;
};

function nextGreaterElement(nums1: number[], nums2: number[]): number[] {
  return nums1.map((value) => findNextGreater(value, nums2));
}

export default nextGreaterElement;
