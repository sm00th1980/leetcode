import nextGreaterElement from '..';

test('example 1', () => {
  const nums1 = [4, 1, 2];
  const nums2 = [1, 3, 4, 2];
  const output = [-1, 3, -1];

  expect(nextGreaterElement(nums1, nums2)).toEqual(output);
});

test('example 2', () => {
  const nums1 = [2, 4];
  const nums2 = [1, 2, 3, 4];
  const output = [3, -1];

  expect(nextGreaterElement(nums1, nums2)).toEqual(output);
});
