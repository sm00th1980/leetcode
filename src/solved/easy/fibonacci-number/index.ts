const getFib = (currentN: number, targetN: number, fibN_2: number, fibN_1: number): number => {
  if (currentN === targetN) {
    return fibN_2 + fibN_1;
  }

  return getFib(currentN + 1, targetN, fibN_1, fibN_2 + fibN_1);
};

function fib(N: number): number {
  if (N <= 0) {
    return 0;
  }

  if (N === 1) {
    return 1;
  }

  return getFib(2, N, 0, 1);
}

export default fib;
