import fib from '..';

test('example 1', () => {
  const n = 2;
  const result = 1; // [0,1]

  expect(fib(n)).toEqual(result);
});

test('example 2', () => {
  const n = 3;
  const result = 2; // [0,1]

  expect(fib(n)).toEqual(result);
});

test('example 3', () => {
  const n = 4;
  const result = 3; // [0,1]

  expect(fib(n)).toEqual(result);
});
