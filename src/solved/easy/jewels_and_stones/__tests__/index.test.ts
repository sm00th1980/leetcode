import numJewelsInStones from '..';

test('example 1', () => {
  const J = 'aA';
  const S = 'aAAbbbb';

  expect(numJewelsInStones(J, S)).toBe(3);
});

test('example 2', () => {
  const J = 'z';
  const S = 'ZZ';

  expect(numJewelsInStones(J, S)).toBe(0);
});
