function numJewelsInStones(J: string, S: string): number {
  const jewels = J.split('');
  const stones = S.split('');

  return stones.reduce((acc, stone) => {
    if (jewels.includes(stone)) {
      return acc + 1;
    }

    return acc;
  }, 0);
}

export default numJewelsInStones;
