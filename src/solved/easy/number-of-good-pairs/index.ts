const countPairs = (index: number, val: number, nums: number[]): number => {
  return nums.reduce((acc, value, currentIndex) => {
    if (value === val && index < currentIndex) {
      return acc + 1;
    }

    return acc;
  }, 0);
};

function numIdenticalPairs(nums: number[]): number {
  return nums.reduce((acc, val, index) => {
    return acc + countPairs(index, val, nums);
  }, 0);
}

export default numIdenticalPairs;
