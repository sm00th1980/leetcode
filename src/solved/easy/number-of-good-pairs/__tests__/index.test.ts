import numIdenticalPairs from '..';

test('example 1', () => {
  const nums = [1, 2, 3, 1, 1, 3];
  const output = 4;

  expect(numIdenticalPairs(nums)).toEqual(output);
});

test('example 2', () => {
  const nums = [1, 1, 1, 1];
  const output = 6;

  expect(numIdenticalPairs(nums)).toEqual(output);
});

test('example 3', () => {
  const nums = [1, 2, 3];
  const output = 0;

  expect(numIdenticalPairs(nums)).toEqual(output);
});
