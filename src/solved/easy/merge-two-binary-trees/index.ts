/* eslint-disable @typescript-eslint/no-non-null-assertion */

import { isNil } from '../../../utils';

export class TreeNode {
  val: number;
  left: TreeNode | null;
  right: TreeNode | null;
  constructor(val?: number, left?: TreeNode | null, right?: TreeNode | null) {
    this.val = val === undefined ? 0 : val;
    this.left = left === undefined ? null : left;
    this.right = right === undefined ? null : right;
  }
}

export const node = (value: number): TreeNode => {
  return new TreeNode(value, null, null);
};

function mergeTrees(t1: TreeNode | null, t2: TreeNode | null): TreeNode | null {
  if (isNil(t1) && isNil(t2)) {
    return null;
  }

  if (!isNil(t1) && isNil(t2)) {
    return t1;
  }

  if (isNil(t1) && !isNil(t2)) {
    return t2;
  }

  return new TreeNode(t1!.val + t2!.val, mergeTrees(t1!.left, t2!.left), mergeTrees(t1!.right, t2!.right));
}

export default mergeTrees;
