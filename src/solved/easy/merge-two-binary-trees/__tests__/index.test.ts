import mergeTrees, { TreeNode, node } from '..';

test('example 1', () => {
  const tree1 = new TreeNode(1, new TreeNode(3, node(5), null), node(2));

  const tree2 = new TreeNode(2, new TreeNode(1, null, node(4)), new TreeNode(3, null, node(7)));

  const output = new TreeNode(3, new TreeNode(4, node(5), node(4)), new TreeNode(5, null, node(7)));

  expect(mergeTrees(tree1, tree2)).toEqual(output);
});
