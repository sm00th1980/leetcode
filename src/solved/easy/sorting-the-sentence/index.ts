import { sort } from '../../../utils';

type Word = {
  word: string;
  order: number;
};

const extractWord = (s: string): Word => {
  const wordRe = /^\D*/;
  const word = s.match(wordRe)![0];

  const orderRe = /\d$/;
  const order = Number(s.match(orderRe)![0]);

  return { word, order };
};

function sortSentence(s: string): string {
  const sortByOrder = (a: Word, b: Word) => a.order - b.order;

  return sort(
    sortByOrder,
    s.split(' ').map((item) => extractWord(item)),
  )
    .map(({ word }) => word)
    .join(' ');
}

export default sortSentence;
