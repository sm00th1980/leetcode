import sortSentence from '..';

test('example 1', () => {
  const input = 'is2 sentence4 This1 a3';
  const output = 'This is a sentence';

  expect(sortSentence(input)).toEqual(output);
});

test('example 2', () => {
  const input = 'Myself2 Me1 I4 and3';
  const output = 'Me Myself and I';

  expect(sortSentence(input)).toEqual(output);
});
