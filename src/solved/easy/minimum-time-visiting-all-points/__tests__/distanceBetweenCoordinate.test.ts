import { distanceBetweenCoordinate } from '..';

test('example 1', () => {
  const coord1 = 1;
  const coord2 = 3;
  const output = 2;

  expect(distanceBetweenCoordinate(coord1, coord2)).toEqual(output);
});

test('example 2', () => {
  const coord1 = -2;
  const coord2 = 2;
  const output = 4;

  expect(distanceBetweenCoordinate(coord1, coord2)).toEqual(output);
});

test('example 3', () => {
  const coord1 = -2;
  const coord2 = -4;
  const output = 2;

  expect(distanceBetweenCoordinate(coord1, coord2)).toEqual(output);
});

test('example 4', () => {
  const coord1 = 2;
  const coord2 = -2;
  const output = 4;

  expect(distanceBetweenCoordinate(coord1, coord2)).toEqual(output);
});

test('example 5', () => {
  const coord1 = 0;
  const coord2 = -2;
  const output = 2;

  expect(distanceBetweenCoordinate(coord1, coord2)).toEqual(output);
});

test('example 6', () => {
  const coord1 = 0;
  const coord2 = 2;
  const output = 2;

  expect(distanceBetweenCoordinate(coord1, coord2)).toEqual(output);
});
