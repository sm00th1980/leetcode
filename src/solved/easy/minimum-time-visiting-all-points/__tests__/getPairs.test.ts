import { getPairs } from '..';

test('example 1', () => {
  const input = [0, 1, 2];
  const output = [
    [0, 1],
    [1, 2],
  ];

  expect(getPairs(input)).toEqual(output);
});

test('example 2', () => {
  const input = [0, 1, 2, 3];
  const output = [
    [0, 1],
    [1, 2],
    [2, 3],
  ];

  expect(getPairs(input)).toEqual(output);
});
