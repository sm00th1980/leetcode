import minTimeToVisitAllPoints from '..';

test('example 1', () => {
  const points = [
    [1, 1],
    [3, 4],
    [-1, 0],
  ];
  const output = 7;

  expect(minTimeToVisitAllPoints(points)).toEqual(output);
});

test('example 2', () => {
  const points = [
    [3, 2],
    [-2, 2],
  ];
  const output = 5;

  expect(minTimeToVisitAllPoints(points)).toEqual(output);
});
