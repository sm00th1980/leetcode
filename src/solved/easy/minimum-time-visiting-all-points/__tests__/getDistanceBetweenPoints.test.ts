import { getDistanceBetweenPoints } from '..';

test('example 1', () => {
  const point1 = [0, 0];
  const point2 = [0, 1];
  const output = 1;

  expect(getDistanceBetweenPoints(point1, point2)).toEqual(output);
});

test('example 2', () => {
  const point1 = [0, 0];
  const point2 = [1, 0];
  const output = 1;

  expect(getDistanceBetweenPoints(point1, point2)).toEqual(output);
});

test('example 3', () => {
  const point1 = [0, 0];
  const point2 = [1, 1];
  const output = 1;

  expect(getDistanceBetweenPoints(point1, point2)).toEqual(output);
});

test('example 4', () => {
  const point1 = [0, 0];
  const point2 = [1, -1];
  const output = 1;

  expect(getDistanceBetweenPoints(point1, point2)).toEqual(output);
});

test('example 5', () => {
  const point1 = [0, 0];
  const point2 = [-1, -1];
  const output = 1;

  expect(getDistanceBetweenPoints(point1, point2)).toEqual(output);
});

test('example 6', () => {
  const point1 = [0, 0];
  const point2 = [-1, 1];
  const output = 1;

  expect(getDistanceBetweenPoints(point1, point2)).toEqual(output);
});

test('example 7', () => {
  const point1 = [0, 0];
  const point2 = [-2, -2];
  const output = 2;

  expect(getDistanceBetweenPoints(point1, point2)).toEqual(output);
});
