import { getMoveTo } from '..';

test('example 1', () => {
  const point1 = [0, 0];
  const point2 = [1, 1];
  const output = 'EN';

  expect(getMoveTo(point1, point2)).toEqual(output);
});

test('example 2', () => {
  const point1 = [0, 0];
  const point2 = [1, -1];
  const output = 'SE';

  expect(getMoveTo(point1, point2)).toEqual(output);
});

test('example 3', () => {
  const point1 = [0, 0];
  const point2 = [-1, -1];
  const output = 'WS';

  expect(getMoveTo(point1, point2)).toEqual(output);
});

test('example 4', () => {
  const point1 = [0, 0];
  const point2 = [-1, 1];
  const output = 'NW';

  expect(getMoveTo(point1, point2)).toEqual(output);
});
