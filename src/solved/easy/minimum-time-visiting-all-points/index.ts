import { chunks, getSum } from '../../../utils';

type MoveTo = 'NW' | 'WS' | 'SE' | 'EN';

export const getMoveTo = (point1: number[], point2: number[]): MoveTo => {
  const [x1, y1] = point1;
  const [x2, y2] = point2;

  if (x1 < x2 && y1 < y2) {
    return 'EN';
  }

  if (x1 < x2 && y1 > y2) {
    return 'SE';
  }

  if (x1 > x2 && y1 > y2) {
    return 'WS';
  }

  return 'NW';
};

export const distanceBetweenCoordinate = (coordinate1: number, coordinate2: number): number => {
  if (coordinate1 > 0 && coordinate2 > 0) {
    return Math.abs(coordinate1 - coordinate2);
  }

  if (coordinate1 < 0 && coordinate2 < 0) {
    return Math.abs(coordinate1 - coordinate2);
  }

  return Math.abs(coordinate1) + Math.abs(coordinate2);
};

export const getDistanceBetweenPoints = (point1: number[], point2: number[]): number => {
  const [x1, y1] = point1;
  const [x2, y2] = point2;

  if (x1 === x2) {
    return distanceBetweenCoordinate(y1, y2);
  }

  if (y1 === y2) {
    return distanceBetweenCoordinate(x1, x2);
  }

  const moveTo = getMoveTo(point1, point2);
  if (moveTo === 'EN') {
    return 1 + getDistanceBetweenPoints([x1 + 1, y1 + 1], point2);
  }

  if (moveTo === 'SE') {
    return 1 + getDistanceBetweenPoints([x1 + 1, y1 - 1], point2);
  }

  if (moveTo === 'WS') {
    return 1 + getDistanceBetweenPoints([x1 - 1, y1 - 1], point2);
  }

  // NW
  return 1 + getDistanceBetweenPoints([x1 - 1, y1 + 1], point2);
};

export const getPairs = <T>(xs: T[]): T[][] => {
  const initialValue: T[] = [];

  const doubled = xs.reduce((acc, value, index) => {
    if (index === 0) {
      return [value];
    }

    if (index === xs.length - 1) {
      return [...acc, value];
    }

    return [...acc, value, value];
  }, initialValue);

  return chunks(2, doubled);
};

function minTimeToVisitAllPoints(points: number[][]): number {
  return getSum(getPairs(points).map((pair) => getDistanceBetweenPoints(pair[0], pair[1])));
}

export default minTimeToVisitAllPoints;
