import { isEmpty, assert } from '../../../utils';

const getMiddleIndex = (size: number): number => {
  assert(size <= 0, 'Size should be more zero');
  return Math.floor(size / 2);
};

const NOT_FOUND_VALUE = -1;

const binarySearch = (nums: number[], target: number, index: number): number => {
  if (isEmpty(nums)) {
    return NOT_FOUND_VALUE;
  }

  const middleIndex = getMiddleIndex(nums.length);
  const middleValue = nums[middleIndex];

  if (middleValue === target) {
    return middleIndex + index;
  }

  if (target < middleValue) {
    const newNums = nums.slice(0, middleIndex);

    return binarySearch(newNums, target, index);
  }

  const newNums = nums.slice(middleIndex + 1);
  const newIndex = middleIndex + index + 1;

  return binarySearch(newNums, target, newIndex);
};

function search(nums: number[], target: number): number {
  return binarySearch(nums, target, 0);
}

export default search;
