import search from '..';

test('example 1', () => {
  const nums = [-1, 0, 3, 5, 9, 12];
  const target = 9;
  const output = 4;

  expect(search(nums, target)).toBe(output);
});

test('example 2', () => {
  const nums = [-1, 0, 3, 5, 9, 12];
  const target = 2;
  const output = -1;

  expect(search(nums, target)).toBe(output);
});

test('example 3', () => {
  const nums = [-1, 0, 3, 5, 9, 12, 13, 17, 100, 101];
  const target = 13;
  const output = 6;

  expect(search(nums, target)).toBe(output);
});

test('example 4', () => {
  const nums = [-100, -2, -1, 0, 3, 5, 9, 12, 13, 17, 100, 101];
  const target = 13;
  const output = 8;

  expect(search(nums, target)).toBe(output);
});

test('example 5', () => {
  const nums = [-100, -2, -1, 0, 3, 5, 9, 12, 13, 17, 100, 101];
  const target = 8;
  const output = -1;

  expect(search(nums, target)).toBe(output);
});
