import { listNode } from '../../../../utils/structures';
import deleteNode from '..';

test('example 1', () => {
  const node = listNode(5, listNode(1, listNode(9)));
  const linkedList = listNode(4, node);

  deleteNode(node);

  expect(linkedList).toEqual(listNode(4, listNode(1, listNode(9))));
});

test('example 1', () => {
  const node = listNode(1, listNode(9));
  const linkedList = listNode(4, listNode(5, node));

  deleteNode(node);

  expect(linkedList).toEqual(listNode(4, listNode(5, listNode(9))));
});
