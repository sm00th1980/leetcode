import { ListNode } from '../../../utils/structures';

function deleteNode(root: ListNode | null): void {
  if (root && root.next) {
    root.val = root.next.val;
    root.next = root.next.next;
  }
}

export default deleteNode;
