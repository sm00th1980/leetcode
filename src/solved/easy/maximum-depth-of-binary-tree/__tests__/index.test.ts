import { toBinaryTree } from '../../../../utils/structures';
import maxDepth from '..';

test('example 1', () => {
  const root = toBinaryTree([3, 9, 20, null, null, 15, 7]);
  const output = 3;

  expect(maxDepth(root)).toEqual(output);
});

test('example 2', () => {
  const root = toBinaryTree([1]);
  const output = 1;

  expect(maxDepth(root)).toEqual(output);
});

test('example 3', () => {
  const root = toBinaryTree([3, 9]);
  const output = 2;

  expect(maxDepth(root)).toEqual(output);
});

test('example 4', () => {
  const root = toBinaryTree([3, null, 9]);
  const output = 2;

  expect(maxDepth(root)).toEqual(output);
});

test('example 5', () => {
  const root = toBinaryTree([3, 20, null, 15]);
  const output = 3;

  expect(maxDepth(root)).toEqual(output);
});

test('example 6', () => {
  const root = toBinaryTree([3, 9, 1]);
  const output = 2;

  expect(maxDepth(root)).toEqual(output);
});

test('example 7', () => {
  const root = toBinaryTree([3, 20, 21, 15, null, 15]);
  const output = 3;

  expect(maxDepth(root)).toEqual(output);
});

test('example 8', () => {
  const root = toBinaryTree([3, 20, 21, 15, null, 15, 4]);
  const output = 3;

  expect(maxDepth(root)).toEqual(output);
});

test('example 9', () => {
  const root = toBinaryTree([3, 4, 5, -7, -6, null, null, -7, null, -5, null, -4]);
  const output = 5;

  expect(maxDepth(root)).toEqual(output);
});
