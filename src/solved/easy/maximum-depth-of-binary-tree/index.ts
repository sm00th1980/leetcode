import { isNil } from '../../../utils';
import { BinaryTreeNode as TreeNode } from '../../../utils/structures';
import { Nullable } from '../../../utils/types';

const depth = (root: TreeNode | null, acc = 1): number => {
  if (isNil(root)) {
    return acc;
  }

  const left = depth(root!.left as Nullable<TreeNode>, acc + 1);
  const right = depth(root!.right as Nullable<TreeNode>, acc + 1);

  return Math.max(left, right);
};

function maxDepth(root: TreeNode | null): number {
  if (isNil(root)) {
    return 0;
  }

  const left = depth(root!.left as Nullable<TreeNode>);
  const right = depth(root!.right as Nullable<TreeNode>);

  return Math.max(left, right);
}

export default maxDepth;
