import { Callable } from '../../../utils/types';

const throttle = (fn: Callable, timeout: number) => {
  let ableToRun = true;
  return function (...args: Array<unknown>) {
    const timer = setTimeout(() => {
      ableToRun = true;
    }, timeout);

    if (ableToRun) {
      clearTimeout(timer);
      fn(...args);
      ableToRun = false;
    }
  };
};

const log = (n: number) => {
  console.log(n);
};

const throttledLog = throttle(log, 1000);

throttledLog(1);
throttledLog(2);
throttledLog(3);
