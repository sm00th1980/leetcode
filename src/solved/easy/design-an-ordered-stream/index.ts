import { isNil, all, complement } from '../../../utils';

const isNotNil = complement(isNil);

const nextPointer = (values: string[]): number => {
  if (all(isNotNil, values)) {
    return values.length;
  }

  const initialValue = { found: false, pointer: 0 };
  const result = values.reduce((acc, value, index) => {
    if (acc.found) {
      return acc;
    }

    if (isNil(value)) {
      return { found: true, pointer: index };
    }

    return acc;
  }, initialValue);

  return result.pointer;
};

class OrderedStream {
  pointer = 0;
  values: string[] = [];

  constructor(n: number) {
    this.values = Array(n).fill(undefined);
  }

  insert(idKey: number, value: string): string[] {
    this.values[idKey - 1] = value;

    if (this.pointer === idKey - 1) {
      const newPointer = nextPointer(this.values);

      const slice = this.values.slice(this.pointer, newPointer);
      this.pointer = newPointer;

      return slice;
    }

    return [];
  }
}

export default OrderedStream;
