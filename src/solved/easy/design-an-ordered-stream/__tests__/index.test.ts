import OrderedStream from '..';

test('example 1', () => {
  const os = new OrderedStream(5);

  expect(os.insert(3, 'ccccc')).toEqual([]); // Inserts (3, "ccccc"), returns [].
  expect(os.insert(1, 'aaaaa')).toEqual(['aaaaa']); // Inserts (1, "aaaaa"), returns ["aaaaa"].
  expect(os.insert(2, 'bbbbb')).toEqual(['bbbbb', 'ccccc']); // Inserts (2, "bbbbb"), returns ["bbbbb", "ccccc"].
  expect(os.insert(5, 'eeeee')).toEqual([]); // Inserts (5, "eeeee"), returns [].
  expect(os.insert(4, 'ddddd')).toEqual(['ddddd', 'eeeee']); // Inserts (4, "ddddd"), returns ["ddddd", "eeeee"].
});
