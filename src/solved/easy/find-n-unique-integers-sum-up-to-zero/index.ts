import { head, last } from '../../../utils';

function sumZero(n: number): number[] {
  if (n <= 1) {
    return [0];
  }

  const xs = Array(n)
    .fill(undefined)
    .map((_, index) => index);

  const middleIndex = Math.floor(xs.length / 2);
  const middle = xs[middleIndex];

  if (n % 2 !== 0) {
    return xs.map((value) => value - middle);
  } else {
    const arr = xs.map((value) => value - middle);
    arr[arr.length - 1] = last(arr) + Math.abs(head(arr));
    return arr;
  }
}

export default sumZero;
