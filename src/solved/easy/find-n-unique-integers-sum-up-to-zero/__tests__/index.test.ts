import sumZero from '..';
import { getSum } from '../../../../utils';

test('example 1', () => {
  const input = 5;
  const output = sumZero(input);

  expect(getSum(output)).toBe(0);
  expect(output.length).toBe(input);
});

test('example 2', () => {
  const input = 3;
  const output = sumZero(input);

  expect(getSum(output)).toBe(0);
  expect(output.length).toBe(input);
});

test('example 3', () => {
  const input = 1;
  const output = sumZero(input);

  expect(getSum(output)).toBe(0);
  expect(output.length).toBe(input);
});

test('example 4', () => {
  const input = 4;
  const output = sumZero(input);

  expect(getSum(output)).toBe(0);
  expect(output.length).toBe(input);
});
