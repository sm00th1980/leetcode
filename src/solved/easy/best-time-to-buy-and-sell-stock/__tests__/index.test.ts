import maxProfit from '..';

test('example 1', () => {
  const input = [7, 1, 5, 3, 6, 4];

  const output = 5;

  expect(maxProfit(input)).toEqual(output);
});

test('example 2', () => {
  const input = [7, 6, 4, 3, 1];

  const output = 0;

  expect(maxProfit(input)).toEqual(output);
});

test('example 3', () => {
  const input = [7, 6, 4, 1, 3];

  const output = 2;

  expect(maxProfit(input)).toEqual(output);
});

test('example 4', () => {
  const input = [2, 4, 1];

  const output = 2;

  expect(maxProfit(input)).toEqual(output);
});

test('example 5', () => {
  const input = [2, 4, 1, 2];

  const output = 2;

  expect(maxProfit(input)).toEqual(output);
});

test('example 6', () => {
  const input = [3, 2, 6, 5, 0, 3];

  const output = 4;

  expect(maxProfit(input)).toEqual(output);
});

test('example 7', () => {
  const input = [3, 2, 0, 0, 6, 5, 1, 3, 1, 2, 7, 1];

  const output = 7;

  expect(maxProfit(input)).toEqual(output);
});

test('example 8', () => {
  const input = [3, 2, 6, 5, 0, 5];

  const output = 5;

  expect(maxProfit(input)).toEqual(output);
});
