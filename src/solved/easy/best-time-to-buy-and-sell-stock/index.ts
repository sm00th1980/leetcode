import { getMax, toNumberWithDefaultZero, head } from '../../../utils';

const getProfitBy = (prices: number[]) => {
  const minPrice = toNumberWithDefaultZero(head(prices));
  const priceMoreThanMin = prices.filter((price) => price > minPrice);
  const maxPrice = toNumberWithDefaultZero(getMax(priceMoreThanMin));

  return [minPrice, maxPrice];
};

function maxProfit(prices: number[]): number {
  const result = prices.reduce(
    (acc, price, index) => {
      if (price >= acc.minPrice) {
        return acc;
      }

      const [minPrice, maxPrice] = getProfitBy(prices.slice(index));

      return {
        profit: Math.max(acc.profit, maxPrice - minPrice),
        minPrice,
        maxPrice,
      };
    },
    {
      profit: 0,
      minPrice: Number.MAX_SAFE_INTEGER,
      maxPrice: Number.MAX_SAFE_INTEGER,
    },
  );

  return result.profit;
}

export default maxProfit;
