function heightChecker(heights: number[]): number {
  const sortedHeights = [...heights].sort((a, b) => a - b);
  return heights.reduce((acc, value, index) => {
    const sortedValue = sortedHeights[index];
    return value === sortedValue ? acc : acc + 1;
  }, 0);
}

export default heightChecker;
