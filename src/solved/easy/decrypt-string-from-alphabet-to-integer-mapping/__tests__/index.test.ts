import freqAlphabets from '..';

test('example 1', () => {
  const s = '10#11#12';
  const output = 'jkab';

  expect(freqAlphabets(s)).toEqual(output);
});

test('example 2', () => {
  const s = '1326#';
  const output = 'acz';

  expect(freqAlphabets(s)).toEqual(output);
});

test('example 3', () => {
  const s = '25#';
  const output = 'y';

  expect(freqAlphabets(s)).toEqual(output);
});

test('example 4', () => {
  const s = '12345678910#11#12#13#14#15#16#17#18#19#20#21#22#23#24#25#26#';
  const output = 'abcdefghijklmnopqrstuvwxyz';

  expect(freqAlphabets(s)).toEqual(output);
});
