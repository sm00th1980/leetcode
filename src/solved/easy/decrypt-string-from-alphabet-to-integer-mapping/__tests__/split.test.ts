import { split } from '..';

test('example 1', () => {
  const input = '10#';
  const output = [
    {
      index: '10',
    },
  ];

  expect(split(input)).toEqual(output);
});

test('example 2', () => {
  const input = '1326#';
  const output = [
    {
      index: '1',
    },
    {
      index: '3',
    },
    {
      index: '26',
    },
  ];

  expect(split(input)).toEqual(output);
});

test('example 3', () => {
  const input = '11#12';
  const output = [
    {
      index: '11',
    },
    {
      index: '1',
    },
    {
      index: '2',
    },
  ];

  expect(split(input)).toEqual(output);
});

test('example 4', () => {
  const input = '10#11#12';
  const output = [
    {
      index: '10',
    },
    {
      index: '11',
    },
    {
      index: '1',
    },
    {
      index: '2',
    },
  ];

  expect(split(input)).toEqual(output);
});

test('example 5', () => {
  const input = '12345678910#';
  const output = [
    {
      index: '1',
    },
    {
      index: '2',
    },
    {
      index: '3',
    },
    {
      index: '4',
    },
    {
      index: '5',
    },
    {
      index: '6',
    },
    {
      index: '7',
    },
    {
      index: '8',
    },
    {
      index: '9',
    },
    {
      index: '10',
    },
  ];

  expect(split(input)).toEqual(output);
});

test('example 6', () => {
  const input = '1326#24';
  const output = [
    {
      index: '1',
    },
    {
      index: '3',
    },
    {
      index: '26',
    },
    {
      index: '2',
    },
    {
      index: '4',
    },
  ];

  expect(split(input)).toEqual(output);
});
