import { last } from '../../../utils';

const DICTIONARY: { [letter: string]: string } = {
  1: 'a',
  2: 'b',
  3: 'c',
  4: 'd',
  5: 'e',
  6: 'f',
  7: 'g',
  8: 'h',
  9: 'i',
  10: 'j',
  11: 'k',
  12: 'l',
  13: 'm',
  14: 'n',
  15: 'o',
  16: 'p',
  17: 'q',
  18: 'r',
  19: 's',
  20: 't',
  21: 'u',
  22: 'v',
  23: 'w',
  24: 'x',
  25: 'y',
  26: 'z',
};

const withoutLast = (str: string) => str.slice(0, -1);

const parseHash = (str: string) => {
  if (str.length === 2) {
    return [{ index: str }];
  }

  const hash = str.slice(-2);
  const letters = str.slice(0, -2);

  return [...letters.split('').map((index) => ({ index })), { index: hash }];
};

interface Result {
  index: string;
}

export const split = (str: string): Result[] => {
  if (last(str.split('')) === '#') {
    const initialValue: Result[] = [];
    return withoutLast(str)
      .split('#')
      .reduce((acc, value) => {
        return [...acc, ...parseHash(value)];
      }, initialValue);
  }

  const lastIndexOfHash = str.lastIndexOf('#');
  const result = split(str.slice(0, lastIndexOfHash + 1));

  const lastLetters = str
    .slice(lastIndexOfHash + 1)
    .split('')
    .map((index) => ({ index }));

  return [...result, ...lastLetters];
};

function freqAlphabets(s: string): string {
  return split(s)
    .map(({ index }) => DICTIONARY[index])
    .join('');
}

export default freqAlphabets;
