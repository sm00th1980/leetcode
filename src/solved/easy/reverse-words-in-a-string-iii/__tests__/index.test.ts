import reverseWords from '..';

test('example 1', () => {
  const input = "Let's take LeetCode contest";
  const output = "s'teL ekat edoCteeL tsetnoc";

  expect(reverseWords(input)).toEqual(output);
});
