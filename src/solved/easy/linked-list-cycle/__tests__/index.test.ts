import { listNode } from '../../../../utils/structures';
import hasCycle from '..';

test('example 1', () => {
  const lastNode = listNode(-4);
  const head = listNode(3, listNode(2, listNode(0, lastNode)));
  lastNode.next = head.next;

  const output = true;

  expect(hasCycle(head)).toBe(output);
});

test('example 2', () => {
  const lastNode = listNode(2);
  const head = listNode(1, lastNode);
  lastNode.next = head.next;

  const output = true;

  expect(hasCycle(head)).toBe(output);
});

test('example 3', () => {
  const head = listNode(1);

  const output = false;

  expect(hasCycle(head)).toBe(output);
});

test('example 4', () => {
  const head = listNode(1, listNode(2));

  const output = false;

  expect(hasCycle(head)).toBe(output);
});

test('example 5', () => {
  const head = listNode(1, listNode(1));

  const output = false;

  expect(hasCycle(head)).toBe(output);
});

test('example 6', () => {
  const head = listNode(1, listNode(1, listNode(1)));

  const output = false;

  expect(hasCycle(head)).toBe(output);
});
