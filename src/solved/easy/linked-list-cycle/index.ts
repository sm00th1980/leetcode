import { ListNode } from '../../../utils/structures';
import { isNil } from '../../../utils';
import { Option } from '../../../utils/types';

const isPresent = (head: ListNode, set: Set<ListNode>) => {
  return set.has(head);
};

const containCycle = (head: Option<ListNode>, set: Set<ListNode>): boolean => {
  if (isNil(head)) {
    return false;
  }

  if (isPresent(head!, set)) {
    return true;
  }

  set.add(head!);

  return containCycle(head!.next, set);
};

function hasCycle(head: ListNode | null): boolean {
  return containCycle(head, new Set<ListNode>());
}

export default hasCycle;
