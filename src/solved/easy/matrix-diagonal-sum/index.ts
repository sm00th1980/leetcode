import { rotateMatrixClockwise } from '../../../utils';

const getPrimaryDiagonalSum = (matrix: number[][]): number => {
  return matrix.reduce((acc, _, index) => {
    return acc + matrix[index][index];
  }, 0);
};

const getCenter = (matrix: number[][]): number => {
  if (matrix.length % 2 === 0) {
    return 0;
  }

  const index = Math.floor(matrix.length / 2);

  return matrix[index][index];
};

function diagonalSum(mat: number[][]): number {
  const primarySum = getPrimaryDiagonalSum(mat);
  const secondarySum = getPrimaryDiagonalSum(rotateMatrixClockwise(mat));
  const center = getCenter(mat);

  return primarySum + secondarySum - center;
}

export default diagonalSum;
