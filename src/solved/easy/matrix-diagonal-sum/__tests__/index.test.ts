import diagonalSum from '..';

test('example 1', () => {
  const input = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
  ];
  const output = 25;

  expect(diagonalSum(input)).toEqual(output);
});

test('example 2', () => {
  const input = [
    [1, 1, 1, 1],
    [1, 1, 1, 1],
    [1, 1, 1, 1],
    [1, 1, 1, 1],
  ];
  const output = 8;

  expect(diagonalSum(input)).toEqual(output);
});

test('example 3', () => {
  const input = [[5]];
  const output = 5;

  expect(diagonalSum(input)).toEqual(output);
});

test('example 4', () => {
  const input = [
    [7, 3, 1, 9],
    [3, 4, 6, 9],
    [6, 9, 6, 6],
    [9, 5, 8, 5],
  ];
  const output = 55;

  expect(diagonalSum(input)).toEqual(output);
});
