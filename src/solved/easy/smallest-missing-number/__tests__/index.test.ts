import solution from '..';

test('example 1', () => {
  const input = [1, 3, 6, 4, 1, 2];
  const output = 5;

  expect(solution(input)).toBe(output);
});

test('example 2', () => {
  const input = [1, 2, 3];
  const output = 4;

  expect(solution(input)).toBe(output);
});

test('example 3', () => {
  const input = [-1, -3];
  const output = 1;

  expect(solution(input)).toBe(output);
});

test('example 4', () => {
  const input = [0, 1, 2, 6, 9];
  const output = 3;

  expect(solution(input)).toBe(output);
});

test('example 5', () => {
  const input = [0, 1, 2, 3, 4, 5, 6, 7, 10];
  const output = 8;

  expect(solution(input)).toBe(output);
});
