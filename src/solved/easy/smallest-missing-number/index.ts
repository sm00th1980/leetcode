import { isNil, unique, last, isEmpty } from '../../../utils';

function solution(A: number[]): number {
  A.sort((a, b) => a - b);
  const xs = unique(A.filter((value) => value > 0));

  if (isEmpty(xs)) {
    return 1;
  }

  let result = undefined;
  for (let i = 0; i < xs.length; i++) {
    const value = xs[i];
    const possibleMissing = i + 1;

    if (value !== possibleMissing) {
      result = possibleMissing;
      break;
    }
  }

  if (isNil(result)) {
    return last(xs) + 1;
  }

  return result as number;
}

export default solution;
