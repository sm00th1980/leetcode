import isValid from '..';

test('example 1', () => {
  const input = '()';
  const output = true;

  expect(isValid(input)).toEqual(output);
});

test('example 2', () => {
  const input = '()[]{}';
  const output = true;

  expect(isValid(input)).toEqual(output);
});

test('example 3', () => {
  const input = '(]';
  const output = false;

  expect(isValid(input)).toEqual(output);
});

test('example 4', () => {
  const input = '([])';
  const output = true;

  expect(isValid(input)).toEqual(output);
});

test('example 5', () => {
  const input = '';
  const output = true;

  expect(isValid(input)).toEqual(output);
});

test('example 6', () => {
  const input = '([{}])';
  const output = true;

  expect(isValid(input)).toEqual(output);
});

test('example 7', () => {
  const input = '[';
  const output = false;

  expect(isValid(input)).toEqual(output);
});

test('example 8', () => {
  const input = '((';
  const output = false;

  expect(isValid(input)).toEqual(output);
});

test('example 9', () => {
  const input = '){';
  const output = false;

  expect(isValid(input)).toEqual(output);
});

test('example 10', () => {
  const input = '([}}])';
  const output = false;

  expect(isValid(input)).toEqual(output);
});
