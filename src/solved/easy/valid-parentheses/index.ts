import { Stack } from '../../../utils/structures';
import { Option } from '../../../utils/types';

const isOpen = (s: string) => ['(', '{', '['].includes(s);
const isClose = (s: string) => [')', '}', ']'].includes(s);

const isMatched = (bracket1: Option<string>, bracket2: Option<string>): boolean => {
  if (bracket1 === '(' && bracket2 === ')') {
    return true;
  }

  if (bracket1 === '{' && bracket2 === '}') {
    return true;
  }

  if (bracket1 === '[' && bracket2 === ']') {
    return true;
  }

  return false;
};

type InitialValue = {
  result: boolean;
  stack: Stack<string>;
};

function isValid(s: string): boolean {
  const letters = s.split('');

  if (letters.length === 0) {
    return true;
  }

  if (letters.length % 2 !== 0) {
    return false;
  }

  const initialValue: InitialValue = {
    result: true,
    stack: new Stack<string>(),
  };

  const res = letters.reduce<InitialValue>((acc, letter) => {
    if (isClose(letter) && acc.stack.isEmpty()) {
      return {
        ...acc,
        result: false,
      };
    }

    if (isClose(letter) && !isMatched(acc.stack.peek(), letter)) {
      return {
        ...acc,
        result: false,
      };
    }

    if (isOpen(letter)) {
      acc.stack.push(letter);
      return acc;
    }

    if (isMatched(acc.stack.peek(), letter)) {
      acc.stack.pop();
    }

    return acc;
  }, initialValue);

  return res.result && res.stack.isEmpty();
}

export default isValid;
