export const getSubdomains = (domain: string): string[] => {
  const initialValue: string[] = [];
  return domain.split('.').reduce((acc, _, index, arr) => {
    acc.push(arr.slice(index).join('.'));
    return acc;
  }, initialValue);
};

interface DomainCount {
  domain: string;
  count: number;
}

const getDomainCount = (value: string): DomainCount[] => {
  const [count, domain] = value.split(' ');

  return getSubdomains(domain).map((domainName) => ({ domain: domainName, count: +count }));
};

function subdomainVisits(cpdomains: string[]): string[] {
  const initialValue: { [domain: string]: number } = {};

  const result = cpdomains.reduce((acc, value) => {
    const domains = getDomainCount(value);
    domains.forEach((domainCount) => {
      const oldCount = acc[domainCount.domain] || 0;
      acc[domainCount.domain] = oldCount + domainCount.count;
    });
    return acc;
  }, initialValue);

  return Object.keys(result)
    .map((key) => {
      const count = result[key];
      return `${count} ${key}`;
    })
    .sort();
}

export default subdomainVisits;
