import subdomainVisits from '..';

test('example 1', () => {
  const input = ['9001 discuss.leetcode.com'];
  const output = ['9001 discuss.leetcode.com', '9001 leetcode.com', '9001 com'].sort();

  expect(subdomainVisits(input)).toEqual(output);
});

test('example 2', () => {
  const input = ['900 google.mail.com', '50 yahoo.com', '1 intel.mail.com', '5 wiki.org'];
  const output = [
    '901 mail.com',
    '50 yahoo.com',
    '900 google.mail.com',
    '5 wiki.org',
    '5 org',
    '1 intel.mail.com',
    '951 com',
  ].sort();

  expect(subdomainVisits(input)).toEqual(output);
});
