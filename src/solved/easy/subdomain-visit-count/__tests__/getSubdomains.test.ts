import { getSubdomains } from '..';

test('example 1', () => {
  const input = 'discuss.leetcode.com';
  const output = ['discuss.leetcode.com', 'leetcode.com', 'com'];

  expect(getSubdomains(input)).toEqual(output);
});

test('example 2', () => {
  const input = 'yahoo.com';
  const output = ['yahoo.com', 'com'];

  expect(getSubdomains(input)).toEqual(output);
});
