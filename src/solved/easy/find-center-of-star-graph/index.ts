import { all, tail } from '../../../utils';

function findCenter(edges: number[][]): number {
  const [x1, x2] = edges[0];
  const isPresentX1 = (edge: number[]) => edge[0] === x1 || edge[1] === x1;

  return all(isPresentX1, tail(edges)) ? x1 : x2;
}

export default findCenter;
