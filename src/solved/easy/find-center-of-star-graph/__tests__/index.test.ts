import findCenter from '..';

test('example 1', () => {
  const edges = [
    [1, 2],
    [2, 3],
    [4, 2],
  ];
  const output = 2;

  expect(findCenter(edges)).toEqual(output);
});

test('example 2', () => {
  const edges = [
    [1, 2],
    [5, 1],
    [1, 3],
    [1, 4],
  ];
  const output = 1;

  expect(findCenter(edges)).toEqual(output);
});
