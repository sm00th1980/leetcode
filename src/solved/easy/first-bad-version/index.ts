import { last, isEmpty, head } from '../../../utils';

type GoodOrBad = 'GOOD' | 'BAD';

const isGood = (value: unknown) => value === 'GOOD';
const isBad = (value: unknown) => value === 'BAD';

export type Versions = Record<number, GoodOrBad>;

export const getLeftMiddleVersion = (version: number, versions: Versions) => {
  const versionNumbers = Object.keys(versions)
    .map((v) => Number(v))
    .filter((v) => v < version);

  if (isEmpty(versionNumbers)) {
    return Math.ceil(version / 2);
  }

  return Math.ceil((last(versionNumbers) + version) / 2);
};

export const getRightMiddleVersion = (version: number, versions: Versions) => {
  const versionNumbers = Object.keys(versions)
    .map((v) => Number(v))
    .filter((v) => v > version);

  const first = head(versionNumbers);

  return Math.floor((first + version) / 2);
};

const getNextValue = (version: number, versions: Versions) => versions[version + 1];
const getPrevValue = (version: number, versions: Versions) => versions[version - 1];

const solution = function (isBadVersion: any) {
  const versions: Versions = {};

  const getFirstBadVersion = (n: number): number => {
    const curentValue = isBadVersion(n) ? 'BAD' : 'GOOD';
    versions![n] = curentValue; // keep it in cache

    if (n === 1 && isBad(curentValue)) {
      return 1;
    }

    const nextValue = getNextValue(n, versions);
    if (isGood(curentValue) && isBad(nextValue)) {
      return n + 1;
    }

    const prevValue = getPrevValue(n, versions);
    if (isGood(prevValue) && isBad(curentValue)) {
      return n;
    }

    // not enough data for calculate proper result
    if (isGood(curentValue)) {
      // current version is good -> go to right
      const versionFromRightSide = getRightMiddleVersion(n, versions);
      return getFirstBadVersion(versionFromRightSide);
    }

    // current version is bad -> go to left
    const versionFromLeftSide = getLeftMiddleVersion(n, versions);

    return getFirstBadVersion(versionFromLeftSide);
  };

  return getFirstBadVersion;
};

export default solution;
