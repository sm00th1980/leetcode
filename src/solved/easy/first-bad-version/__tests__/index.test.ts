import solution from '..';

test('example 1', () => {
  const n = 5;
  const badVersion = 4;

  const isBadVersion = jest.fn((version: number): boolean => version >= badVersion);

  expect(solution(isBadVersion)(n)).toBe(badVersion);
  expect(isBadVersion).toHaveBeenCalledTimes(3);
});

test('example 2', () => {
  const n = 1;
  const badVersion = 1;

  const isBadVersion = jest.fn((version: number): boolean => version >= badVersion);
  expect(solution(isBadVersion)(n)).toBe(badVersion);
  expect(isBadVersion).toHaveBeenCalledTimes(1);
});

test('example 3', () => {
  const n = 2;
  const badVersion = 1;

  const isBadVersion = jest.fn((version: number): boolean => version >= badVersion);
  expect(solution(isBadVersion)(n)).toBe(badVersion);
  expect(isBadVersion).toHaveBeenCalledTimes(2);
});

test('example 4', () => {
  const n = 10;
  const badVersion = 3;

  const isBadVersion = jest.fn((version: number): boolean => version >= badVersion);
  expect(solution(isBadVersion)(n)).toBe(badVersion);
});

test('example 5', () => {
  const n = 10;
  const badVersion = 5;

  const isBadVersion = jest.fn((version: number): boolean => version >= badVersion);
  expect(solution(isBadVersion)(n)).toBe(badVersion);
});

test('example 6', () => {
  const n = 20;
  const badVersion = 15;

  const isBadVersion = jest.fn((version: number): boolean => version >= badVersion);
  expect(solution(isBadVersion)(n)).toBe(badVersion);
});

test('example 7', () => {
  const n = 2126753390;
  const badVersion = 1702766719;

  const isBadVersion = jest.fn((version: number): boolean => version >= badVersion);
  expect(solution(isBadVersion)(n)).toBe(badVersion);
});
