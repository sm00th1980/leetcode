import { getRightMiddleVersion, Versions } from '..';

test('example 1', () => {
  const version = 5;
  const versions: Versions = { 10: 'BAD' };

  const output = getRightMiddleVersion(version, versions);

  expect(output).toBe(7);
});

test('example 2', () => {
  const version = 5;
  const versions: Versions = { 5: 'GOOD', 10: 'BAD' };

  const output = getRightMiddleVersion(version, versions);

  expect(output).toBe(7);
});

test('example 3', () => {
  const version = 5;
  const versions: Versions = { 10: 'BAD', 5: 'GOOD' };

  const output = getRightMiddleVersion(version, versions);

  expect(output).toBe(7);
});

test('example 4', () => {
  const version = 5;
  const versions: Versions = { 1: 'GOOD', 5: 'GOOD', 10: 'BAD' };

  const output = getRightMiddleVersion(version, versions);

  expect(output).toBe(7);
});

test('example 5', () => {
  const version = 5;
  const versions: Versions = { 1: 'GOOD', 10: 'BAD' };

  const output = getRightMiddleVersion(version, versions);

  expect(output).toBe(7);
});

test('example 6', () => {
  const version = 1000;
  const versions: Versions = { 1000: 'GOOD', 2000: 'BAD' };

  const output = getRightMiddleVersion(version, versions);

  expect(output).toBe(1500);
});

test('example 7', () => {
  const version = 1000;
  const versions: Versions = { 2000: 'BAD' };

  const output = getRightMiddleVersion(version, versions);

  expect(output).toBe(1500);
});

test('example 8', () => {
  const version = 2126753390;
  const versions: Versions = { 2126753390: 'GOOD', 2126753410: 'BAD' };

  const output = getRightMiddleVersion(version, versions);

  expect(output).toBe(2126753400);
});

test('example 9', () => {
  const version = 2126753390;
  const versions: Versions = { 2126753410: 'BAD' };

  const output = getRightMiddleVersion(version, versions);

  expect(output).toBe(2126753400);
});

test('example 10', () => {
  const version = 5;
  const versions: Versions = { 1: 'GOOD', 5: 'GOOD', 10: 'BAD', 20: 'BAD' };

  const output = getRightMiddleVersion(version, versions);

  expect(output).toBe(7);
});

test('example 11', () => {
  const version = 5;
  const versions: Versions = { 1: 'GOOD', 10: 'BAD', 20: 'BAD' };

  const output = getRightMiddleVersion(version, versions);

  expect(output).toBe(7);
});
