import { getLeftMiddleVersion, Versions } from '..';

test('example 1', () => {
  const version = 5;
  const versions: Versions = {};

  const output = getLeftMiddleVersion(version, versions);

  expect(output).toBe(3);
});

test('example 2', () => {
  const version = 5;
  const versions: Versions = { 5: 'BAD' };

  const output = getLeftMiddleVersion(version, versions);

  expect(output).toBe(3);
});

test('example 3', () => {
  const version = 6;
  const versions: Versions = {};

  const output = getLeftMiddleVersion(version, versions);

  expect(output).toBe(3);
});

test('example 4', () => {
  const version = 6;
  const versions: Versions = { 1: 'GOOD', 2: 'GOOD' };

  const output = getLeftMiddleVersion(version, versions);

  expect(output).toBe(4);
});

test('example 5', () => {
  const version = 1000;
  const versions: Versions = {};

  const output = getLeftMiddleVersion(version, versions);

  expect(output).toBe(500);
});

test('example 6', () => {
  const version = 2126753390;
  const versions: Versions = {};

  const output = getLeftMiddleVersion(version, versions);

  expect(output).toBe(version / 2);
});

test('example 7', () => {
  const version = 6;
  const versions: Versions = { 1: 'GOOD', 2: 'GOOD', 10: 'BAD' };

  const output = getLeftMiddleVersion(version, versions);

  expect(output).toBe(4);
});

test('example 8', () => {
  const version = 6;
  const versions: Versions = { 1: 'GOOD', 2: 'GOOD', 6: 'GOOD', 10: 'BAD' };

  const output = getLeftMiddleVersion(version, versions);

  expect(output).toBe(4);
});
