import toLowerCase from '..';

test('example 1', () => {
  const input = 'Hello';

  expect(toLowerCase(input)).toBe('hello');
});

test('example 2', () => {
  const input = 'here';

  expect(toLowerCase(input)).toBe('here');
});

test('example 3', () => {
  const input = 'LOVELY';

  expect(toLowerCase(input)).toBe('lovely');
});

test('example 4', () => {
  const input = 'PiTAs';

  expect(toLowerCase(input)).toBe('pitas');
});
