import { dec2bin, bin2dec } from '../../../utils';

const invert = (binary: string): string => {
  return binary
    .split('')
    .map((value) => (value === '1' ? '0' : '1'))
    .join('');
};

function findComplement(num: number): number {
  return bin2dec(invert(dec2bin(num)));
}

export default findComplement;
