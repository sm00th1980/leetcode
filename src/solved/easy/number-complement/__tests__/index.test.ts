import findComplement from '..';

test('example 1', () => {
  const num = 5;
  const output = 2;

  expect(findComplement(num)).toEqual(output);
});

test('example 2', () => {
  const num = 1;
  const output = 0;

  expect(findComplement(num)).toEqual(output);
});
