import minOperations from '..';

test('example 1', () => {
  const input = [1, 1, 1];
  const output = 3;

  expect(minOperations(input)).toEqual(output);
});

test('example 2', () => {
  const input = [1, 5, 2, 4, 1];
  const output = 14;

  expect(minOperations(input)).toEqual(output);
});

test('example 3', () => {
  const input = [8];
  const output = 0;

  expect(minOperations(input)).toEqual(output);
});
