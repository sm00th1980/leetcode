import { tail, head, last, getSum } from '../../../utils';

function minOperations(nums: number[]): number {
  const increasedNums = tail(nums).reduce(
    (acc, x) => {
      const lastNum = last(acc);
      if (lastNum < x) {
        return [...acc, x];
      }

      return [...acc, lastNum + 1];
    },
    [head(nums)],
  );

  return getSum(increasedNums.map((x, index) => x - nums[index]));
}

export default minOperations;
