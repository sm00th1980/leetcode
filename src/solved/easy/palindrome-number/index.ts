import { reverse, equals } from '../../../utils';

export const splitNumberToArray = (x: number): Array<number> => {
  const length = x.toString().length;

  const digits = reverse(
    Array(length)
      .fill(undefined)
      .map((_, i) => Math.pow(10, i)),
  );

  const initialValue: {
    rest: number;
    result: number[];
  } = {
    rest: x,
    result: [],
  };

  const { result } = digits.reduce((acc, currentValue) => {
    const intValue = Math.floor(acc.rest / currentValue);

    return {
      rest: acc.rest - intValue * currentValue,
      result: [...acc.result, intValue],
    };
  }, initialValue);

  return result;
};

// Follow up: Could you solve it without converting the integer to a string?

function isPalindrome(x: number): boolean {
  if (x < 0) {
    return false;
  }

  const original = splitNumberToArray(x);
  const reversed = reverse(original);

  return equals(original, reversed);
}

export default isPalindrome;
