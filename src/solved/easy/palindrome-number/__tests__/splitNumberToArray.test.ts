import { splitNumberToArray } from '..';

test('example 1', () => {
  const input = 123;
  const output = [1, 2, 3];

  expect(splitNumberToArray(input)).toEqual(output);
});

test('example 2', () => {
  const input = 1234;
  const output = [1, 2, 3, 4];

  expect(splitNumberToArray(input)).toEqual(output);
});

test('example 3', () => {
  const input = 10;
  const output = [1, 0];

  expect(splitNumberToArray(input)).toEqual(output);
});

test('example 4', () => {
  const input = 1234554321;
  const output = [1, 2, 3, 4, 5, 5, 4, 3, 2, 1];

  expect(splitNumberToArray(input)).toEqual(output);
});
