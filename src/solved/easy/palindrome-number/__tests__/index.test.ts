import isPalindrome from '..';

test('example 1', () => {
  const input = 121;
  const output = true;

  expect(isPalindrome(input)).toEqual(output);
});

test('example 2', () => {
  const input = -121;
  const output = false;

  expect(isPalindrome(input)).toEqual(output);
});

test('example 3', () => {
  const input = 10;
  const output = false;

  expect(isPalindrome(input)).toEqual(output);
});

test('example 4', () => {
  const input = 1234554321;
  const output = true;

  expect(isPalindrome(input)).toEqual(output);
});
