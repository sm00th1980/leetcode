import diStringMatch from '..';

test('example 1', () => {
  const input = 'IDID';
  const output = [0, 3, 1, 4, 2];

  expect(diStringMatch(input)).toEqual(output);
});

test('example 2', () => {
  const input = 'III';
  const output = [0, 1, 2, 3];

  expect(diStringMatch(input)).toEqual(output);
});

test('example 3', () => {
  const input = 'DDI';
  const output = [3, 1, 0, 2];

  expect(diStringMatch(input)).toEqual(output);
});

test('example 4', () => {
  const input = 'IDD';
  const output = [1, 3, 2, 0];

  expect(diStringMatch(input)).toEqual(output);
});
