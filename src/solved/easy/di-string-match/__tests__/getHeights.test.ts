import { getHeights } from '..';

test('example 1', () => {
  const input = 'IDID';
  const output = [0, 1, 0, 1, 0];

  expect(getHeights(input)).toEqual(output);
});

test('example 2', () => {
  const input = 'III';
  const output = [0, 1, 2, 3];

  expect(getHeights(input)).toEqual(output);
});

test('example 3', () => {
  const input = 'DDI';
  const output = [0, -1, -2, -1];

  expect(getHeights(input)).toEqual(output);
});

test('example 4', () => {
  const input = 'IDD';
  const output = [0, 1, 0, -1];

  expect(getHeights(input)).toEqual(output);
});
