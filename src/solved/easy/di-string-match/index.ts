import { last } from '../../../utils';

export const getHeights = (source: string): number[] => {
  return source.split('').reduce(
    (acc, letter) => {
      const prevHeight = last(acc);
      const nextHeight = letter === 'I' ? prevHeight + 1 : prevHeight - 1;

      return [...acc, nextHeight];
    },
    [0],
  );
};

function diStringMatch(S: string): number[] {
  return getHeights(S)
    .map((height, index) => ({ height, index }))
    .sort((h1, h2) => h1.height - h2.height)
    .reduce((acc, height, index) => {
      acc[height.index] = index;
      return acc;
    }, [] as number[]);
}

export default diStringMatch;
