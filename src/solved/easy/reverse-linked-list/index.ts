import { ListNode } from '../../../utils/structures';
import { isNil, head, tail, isEmpty } from '../../../utils';
import { Option } from '../../../utils/types';

export const getValues = (head: Option<ListNode>): number[] => {
  if (isNil(head)) {
    return [];
  }

  let next: Option<ListNode> = head!;
  const xs: number[] = [next.val];
  while ((next = next.next)) {
    xs.unshift(next.val);
  }
  return xs;
};

export const makeListRec = (values: number[]): Option<ListNode> => {
  if (isEmpty(values)) {
    return null;
  }

  return new ListNode(head(values), makeListRec(tail(values)));
};

export const makeList = (values: number[]): Option<ListNode> => {
  if (isEmpty(values)) {
    return null;
  }

  const root = new ListNode(values[0]);
  let nextHead = root;
  const xs = tail(values);
  let i: Option<number> = head(values);

  while (xs.length > 0) {
    i = xs.shift();
    nextHead.next = new ListNode(i, null);
    nextHead = nextHead.next;
  }

  return root;
};

function reverseList(head: Option<ListNode>): Option<ListNode> {
  if (isNil(head)) {
    return null;
  }

  const reversed = getValues(head);
  return makeList(reversed);
}

export default reverseList;
