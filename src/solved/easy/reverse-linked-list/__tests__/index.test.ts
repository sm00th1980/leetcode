import reverseList, { makeListRec, makeList } from '..';

test('example 1', () => {
  const list = [1, 2, 3, 4, 5];
  const root = makeList(list);
  const output = makeList([...list].reverse());

  expect(reverseList(root)).toEqual(output);
});

test('example 2', () => {
  const root = null;
  const output = null;

  expect(reverseList(root)).toEqual(output);
});

test('example 4', () => {
  const list = Array(1000)
    .fill(undefined)
    .map((_, index) => index + 1);
  const root = makeList(list);
  const output = makeList([...list].reverse());

  expect(reverseList(root)).toEqual(output);
});

test('example 5', () => {
  const list = Array(4000)
    .fill(undefined)
    .map((_, index) => index + 1);

  const reversed = Array(4000)
    .fill(undefined)
    .map((_, index) => index + 1)
    .reverse();

  const root = makeList(list);
  const output = makeList(reversed);

  expect(reverseList(root)).toEqual(output);
});
