import { getValues, makeList } from '..';

test('example 1', () => {
  const list = [1, 2, 3, 4, 5];
  const root = makeList(list);
  const output = [...list].reverse();

  expect(getValues(root)).toEqual(output);
});

test('example 2', () => {
  const list = Array(1000)
    .fill(undefined)
    .map((_, index) => index + 1);

  const root = makeList(list);
  const output = [...list].reverse();

  expect(getValues(root)).toEqual(output);
});

test('example 3', () => {
  const list = Array(5000)
    .fill(undefined)
    .map((_, index) => index + 1);

  const root = makeList(list);
  const output = [...list].reverse();

  expect(getValues(root)).toEqual(output);
});

test('example 4', () => {
  const list = Array(10000)
    .fill(undefined)
    .map((_, index) => index + 1);

  const root = makeList(list);
  const output = [...list].reverse();

  expect(getValues(root)).toEqual(output);
});
