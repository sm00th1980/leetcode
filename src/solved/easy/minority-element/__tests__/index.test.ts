import minorityElement from '..';

test('example 1', () => {
  const input = [2, 3, 3];
  const output = 2;

  expect(minorityElement(input)).toEqual(output);
});

test('example 2', () => {
  const input = [1, 1, 1, 2, 2, 2, 2];
  const output = 1;

  expect(minorityElement(input)).toEqual(output);
});

test('example 3', () => {
  const input = [1, 1, 1, 2, 2, 2, 2, 3];
  const output = 3;

  expect(minorityElement(input)).toEqual(output);
});

test('example 4', () => {
  const input = [1, 1, 1, 2, 2, 2, 2, 3, 3, 4];
  const output = 4;

  expect(minorityElement(input)).toEqual(output);
});
