import { unique, head, getCount } from '../../../utils';

function minorityElement(nums: number[]): number {
  return head(
    unique(nums)
      .map((x) => ({ x, count: getCount(x, nums) }))
      .sort((a, b) => a.count - b.count),
  ).x;
}

export default minorityElement;
