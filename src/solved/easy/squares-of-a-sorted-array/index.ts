function sortedSquares(A: number[]): number[] {
  return A.map((x) => x * x).sort((a, b) => a - b);
}

export default sortedSquares;
