import diameterOfBinaryTree from '..';
import { toBinaryTree, toLevels } from '../../../../utils/structures';

test('example 1', () => {
  const root = toBinaryTree([1, 2, 3, 4, 5]);
  const output = 3;

  expect(diameterOfBinaryTree(root)).toEqual(output);
});

test('example 2', () => {
  const root = toBinaryTree([1, 2]);
  const output = 1;

  expect(diameterOfBinaryTree(root)).toEqual(output);
});

test('example 3', () => {
  const root = toBinaryTree([1, 2, 3]);
  const output = 2;

  expect(diameterOfBinaryTree(root)).toEqual(output);
});

test('example 4', () => {
  const root = toBinaryTree([1]);
  const output = 0;

  expect(diameterOfBinaryTree(root)).toEqual(output);
});

test('example 5', () => {
  const root = toBinaryTree([]);
  const output = 0;

  expect(diameterOfBinaryTree(root)).toEqual(output);
});

test('example 6', () => {
  const inputs = [
    4,
    -7,
    -3,
    null,
    null,
    -9,
    -3,
    9,
    -7,
    -4,
    null,
    6,
    null,
    -6,
    -6,
    null,
    null,
    0,
    6,
    5,
    null,
    9,
    null,
    null,
    -1,
    -4,
    null,
    null,
    null,
    -2,
  ];

  const root = toBinaryTree(inputs);
  const output = 8;

  expect(diameterOfBinaryTree(root)).toEqual(output);
});
