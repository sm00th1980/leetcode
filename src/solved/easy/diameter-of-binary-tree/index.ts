/* eslint @typescript-eslint/ban-ts-comment: off */

import { BinaryTreeNode as TreeNode } from '../../../utils/structures';
import { isNil, flatten, getMax } from '../../../utils';
import { Option } from '../../../utils/types';

const getHeight = (root: Option<TreeNode>, height = 1): number => {
  if (isNil(root)) {
    return 0;
  }

  const leftHeight = getHeight(root?.left);
  const rightHeight = getHeight(root?.right);

  const maxHeight = Math.max(leftHeight, rightHeight);

  return height + maxHeight;
};

// @ts-ignore
const flattenTree = (root: Option<TreeNode>) => {
  if (isNil(root)) {
    return null;
  }

  return [root, flattenTree(root?.left), flattenTree(root?.right)];
};

function diameterOfBinaryTree(root: TreeNode | null): number {
  if (isNil(root)) {
    return 0;
  }

  const nodes: TreeNode[] = flatten(flattenTree(root));

  const heights = nodes.map((node) => {
    const leftHeight = getHeight(node?.left);
    const rightHeight = getHeight(node?.right);

    return leftHeight + rightHeight;
  });

  return getMax(heights) || 0;
}

export default diameterOfBinaryTree;
