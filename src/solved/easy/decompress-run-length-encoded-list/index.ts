import { chunks } from '../../../utils';

function decompressRLElist(nums: number[]): number[] {
  const pairs = chunks<number>(2, nums);
  const initialValue: number[] = [];
  return pairs
    .map(([freq, val]) => Array(freq).fill(val))
    .reduce((acc, xs) => {
      xs.forEach((x) => {
        acc.push(x);
      });

      return acc;
    }, initialValue);
}

export default decompressRLElist;
