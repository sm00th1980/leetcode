import decompressRLElist from '..';

test('example 1', () => {
  const nums = [1, 2, 3, 4];
  const output = [2, 4, 4, 4];

  expect(decompressRLElist(nums)).toEqual(output);
});

test('example 2', () => {
  const nums = [1, 1, 2, 3];
  const output = [1, 3, 3];

  expect(decompressRLElist(nums)).toEqual(output);
});
