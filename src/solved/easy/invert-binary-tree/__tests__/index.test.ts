import invertTree from '..';
import { BinaryTreeNode, binaryTreeNode } from '../../../../utils/structures';

test('example 1', () => {
  const root = new BinaryTreeNode(
    4,
    new BinaryTreeNode(2, binaryTreeNode(1), binaryTreeNode(3)),
    new BinaryTreeNode(7, binaryTreeNode(6), binaryTreeNode(9)),
  );
  const output = new BinaryTreeNode(
    4,
    new BinaryTreeNode(7, binaryTreeNode(9), binaryTreeNode(6)),
    new BinaryTreeNode(2, binaryTreeNode(3), binaryTreeNode(1)),
  );

  expect(invertTree(root)).toEqual(output);
});

test('example 2', () => {
  const root = new BinaryTreeNode(4, binaryTreeNode(2), binaryTreeNode(7));
  const output = new BinaryTreeNode(4, binaryTreeNode(7), binaryTreeNode(2));

  expect(invertTree(root)).toEqual(output);
});

test('example 3', () => {
  const root = new BinaryTreeNode(3, new BinaryTreeNode(2, null, new BinaryTreeNode(4, binaryTreeNode(1), null)), null);
  const output = new BinaryTreeNode(3, null, new BinaryTreeNode(2, new BinaryTreeNode(4, null, binaryTreeNode(1))));

  expect(invertTree(root)).toEqual(output);
});

test('example 4', () => {
  const root = new BinaryTreeNode(4, new BinaryTreeNode(1, new BinaryTreeNode(2, binaryTreeNode(3), null), null), null);
  const output = new BinaryTreeNode(
    4,
    null,
    new BinaryTreeNode(1, null, new BinaryTreeNode(2, null, binaryTreeNode(3))),
  );

  expect(invertTree(root)).toEqual(output);
});
