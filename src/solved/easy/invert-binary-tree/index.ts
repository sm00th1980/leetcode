import { isNil } from '../../../utils';
import { Option } from '../../../utils/types';
import { BinaryTreeNode } from '../../../utils/structures';

function invertTree(root: Option<BinaryTreeNode>): Option<BinaryTreeNode> {
  if (isNil(root)) {
    return null;
  }

  return new BinaryTreeNode(root?.val, invertTree(root?.right), invertTree(root?.left));
}

export default invertTree;
