import addBinary from '..';

test('example 1', () => {
  const a = '11';
  const b = '1';
  const output = '100';

  expect(addBinary(a, b)).toEqual(output);
});

test('example 2', () => {
  const a = '11';
  const b = '11';
  const output = '110';

  expect(addBinary(a, b)).toEqual(output);
});

test('example 3', () => {
  const a = '1010';
  const b = '1011';
  const output = '10101';

  expect(addBinary(a, b)).toEqual(output);
});

test('example 4', () => {
  const a = '1';
  const b = '1';
  const output = '10';

  expect(addBinary(a, b)).toEqual(output);
});

test('example 5', () => {
  const a = '0';
  const b = '0';
  const output = '0';

  expect(addBinary(a, b)).toEqual(output);
});

test('example 6', () => {
  const a = '10100000100100110110010000010101111011011001101110111111111101000000101111001110001111100001101';
  const b = '110101001011101110001111100110001010100001101011101010000011011011001011101111001100000011011110011';
  const output = '110111101100010011000101110110100000011101000101011001000011011000001100011110011010010011000000000';

  expect(addBinary(a, b)).toEqual(output);
});
