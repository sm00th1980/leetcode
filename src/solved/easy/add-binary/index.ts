import { repeat, reverse } from '../../../utils';

type Binary = '0' | '1';

const sumBinary = (
  a: Binary,
  b: Binary,
  overflow: boolean,
): {
  value: Binary;
  overflow: boolean;
} => {
  if (!overflow) {
    if (a === '0' && b === '0') {
      return { value: '0', overflow: false };
    }

    if (a === '1' && b === '0') {
      return { value: '1', overflow: false };
    }

    if (a === '0' && b === '1') {
      return { value: '1', overflow: false };
    }

    return { value: '0', overflow: true };
  }

  // with overflow
  if (a === '0' && b === '0') {
    return { value: '1', overflow: false };
  }

  if (a === '1' && b === '0') {
    return { value: '0', overflow: true };
  }

  if (a === '0' && b === '1') {
    return { value: '0', overflow: true };
  }

  return { value: '1', overflow: true };
};

const normalizeBinaryToLength = (a: string, size: number) => {
  if (a.length >= size) {
    return a;
  }

  return repeat(size - a.length, '0') + a;
};

type InitialValue = {
  value: Array<Binary>;
  overflow: boolean;
};

function addBinary(a: string, b: string): string {
  const maxLength = Math.max(a.length, b.length);

  const normalizedA = normalizeBinaryToLength(a, maxLength);
  const normalizedB = normalizeBinaryToLength(b, maxLength);

  const digitsA = reverse(normalizedA.split('')) as Binary[];
  const digitsB = reverse(normalizedB.split('')) as Binary[];

  const initialValue: InitialValue = {
    value: [],
    overflow: false,
  };

  const result = digitsA.reduce((acc, currentValue, currentIndex) => {
    const sum = sumBinary(currentValue, digitsB[currentIndex], acc.overflow);

    acc.value = [sum.value, ...acc.value];
    acc.overflow = sum.overflow;

    return acc;
  }, initialValue);

  if (result.overflow) {
    return `1${result.value.join('')}`;
  }

  return result.value.join('');
}

export default addBinary;
