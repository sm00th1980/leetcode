import balancedStringSplit from '..';

test('example 1', () => {
  const s = 'RLRRLLRLRL';
  const output = 4;

  expect(balancedStringSplit(s)).toEqual(output);
});

test('example 2', () => {
  const s = 'RLLLLRRRLR';
  const output = 3;

  expect(balancedStringSplit(s)).toEqual(output);
});

test('example 3', () => {
  const s = 'LLLLRRRR';
  const output = 1;

  expect(balancedStringSplit(s)).toEqual(output);
});

test('example 4', () => {
  const s = 'RLRRRLLRLL';
  const output = 2;

  expect(balancedStringSplit(s)).toEqual(output);
});
