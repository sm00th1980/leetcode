const isBalanced = (str: string): boolean => {
  if (str === '') {
    return false;
  }
  const initialValue = { L: 0, R: 0 };
  const { L, R } = str.split('').reduce((acc, letter) => {
    if (letter === 'L') {
      return {
        ...acc,
        L: acc.L + 1,
      };
    }

    if (letter === 'R') {
      return {
        ...acc,
        R: acc.R + 1,
      };
    }

    return acc;
  }, initialValue);

  return L === R;
};

interface Substring {
  stop: boolean;
  result: string;
}

const makeString = (first: string, s: string): string => {
  const second = s.slice(first.length, first.length * 2);
  return [first, second].join('');
};

const getNextIndex = (index: number, str: string): number => {
  const substring = str.slice(index);
  const initialValue: Substring = { stop: false, result: '' };
  const { result: firstPart } = substring.split('').reduce((acc, letter) => {
    if (acc.stop) {
      return acc;
    }

    const { result: first } = acc;
    const checkString = makeString(first, substring);

    if (isBalanced(checkString)) {
      return { ...acc, stop: true };
    }

    return { ...acc, result: `${acc.result}${letter}` };
  }, initialValue);

  const result = makeString(firstPart, substring);

  return index + result.length;
};

function balancedStringSplit(s: string): number {
  let index = 0;
  let results = 0;
  while (index < s.length) {
    index = getNextIndex(index, s);
    results++;
  }

  return results;
}

export default balancedStringSplit;
