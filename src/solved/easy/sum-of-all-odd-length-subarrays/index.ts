import { getSum } from '../../../utils';

export const getAllOdds = (num: number, odds: number[] = []): number[] => {
  if (num <= 1) {
    return [1, ...odds].sort((a, b) => a - b);
  }

  if (num % 2 === 0) {
    return getAllOdds(num - 1, odds);
  }

  return getAllOdds(num - 1, [...odds, num]);
};

export const subArrays = (xs: number[]): number[][] => {
  const result: number[][] = [];

  getAllOdds(xs.length).forEach((size) => {
    const slices = xs.map((_, index) => xs.slice(index, index + size)).filter((x) => x.length === size);

    slices.forEach((slice) => {
      result.push(slice);
    });
  });

  return result;
};

function sumOddLengthSubarrays(arr: number[]): number {
  return getSum(subArrays(arr).map((xs) => getSum(xs)));
}

export default sumOddLengthSubarrays;
