import { getAllOdds } from '..';

test('example 1', () => {
  const input = 2;
  const output = [1];

  expect(getAllOdds(input)).toEqual(output);
});

test('example 2', () => {
  const input = 3;
  const output = [1, 3];

  expect(getAllOdds(input)).toEqual(output);
});

test('example 3', () => {
  const input = 4;
  const output = [1, 3];

  expect(getAllOdds(input)).toEqual(output);
});

test('example 5', () => {
  const input = 5;
  const output = [1, 3, 5];

  expect(getAllOdds(input)).toEqual(output);
});

test('example 6', () => {
  const input = 6;
  const output = [1, 3, 5];

  expect(getAllOdds(input)).toEqual(output);
});

test('example 7', () => {
  const input = 7;
  const output = [1, 3, 5, 7];

  expect(getAllOdds(input)).toEqual(output);
});
