import sumOddLengthSubarrays from '..';

test('example 1', () => {
  const input = [1, 4, 2, 5, 3];
  const output = 58;

  expect(sumOddLengthSubarrays(input)).toEqual(output);
});

test('example 2', () => {
  const input = [1, 2];
  const output = 3;

  expect(sumOddLengthSubarrays(input)).toEqual(output);
});

test('example 3', () => {
  const input = [10, 11, 12];
  const output = 66;

  expect(sumOddLengthSubarrays(input)).toEqual(output);
});

test('example 4', () => {
  const input = [7, 6, 8, 6];
  const output = 68;

  expect(sumOddLengthSubarrays(input)).toEqual(output);
});
