import { subArrays } from '..';

test('example 1', () => {
  const input: number[] = [];
  const output: number[] = [];

  expect(subArrays(input)).toEqual(output);
});

test('example 2', () => {
  const input = [1];
  const output = [[1]];

  expect(subArrays(input)).toEqual(output);
});

test('example 3', () => {
  const input = [1, 2];
  const output = [[1], [2]];

  expect(subArrays(input)).toEqual(output);
});

test('example 4', () => {
  const input = [10, 11, 12];
  const output = [[10], [11], [12], [10, 11, 12]];

  expect(subArrays(input)).toEqual(output);
});
