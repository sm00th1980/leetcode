import subtractProductAndSum from '..';

test('example 1', () => {
  const n = 234;
  const output = 15;

  expect(subtractProductAndSum(n)).toEqual(output);
});

test('example 2', () => {
  const n = 4421;
  const output = 21;

  expect(subtractProductAndSum(n)).toEqual(output);
});
