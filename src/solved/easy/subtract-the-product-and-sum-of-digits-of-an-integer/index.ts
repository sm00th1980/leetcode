function subtractProductAndSum(n: number): number {
  const nums = n
    .toString()
    .split('')
    .map((x) => +x);
  const product = nums.reduce((acc, x) => {
    return acc * x;
  }, 1);
  const sum = nums.reduce((acc, x) => acc + x, 0);

  return product - sum;
}

export default subtractProductAndSum;
