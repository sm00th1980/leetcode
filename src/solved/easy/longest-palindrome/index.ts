import { unique, getSum, reject, getCount, findIndex, getMax } from '../../../utils';

const isOdd = (n: number) => n % 2 !== 0;
const isEven = (n: number) => n % 2 === 0;
const onlyOdds = (xs: number[]) => xs.filter(isOdd);
const onlyEvens = (xs: number[]) => xs.filter(isEven);

function longestPalindrome(s: string): number {
  const letters = s.split('');
  const counts = unique(letters).map((letter) => getCount<string>(letter, letters));

  const odds = onlyOdds(counts);
  const evens = onlyEvens(counts);

  const longestOddCount = getMax(odds) || 0;

  const indexOfLongestOddCount = findIndex((v) => v === longestOddCount, odds);

  const sumOfEvens = getSum(evens);
  const oddsWithoutLongest = reject((_, index) => index === indexOfLongestOddCount, odds);
  const sumOfOdds = getSum(oddsWithoutLongest.map((v) => v - 1));

  return longestOddCount + sumOfEvens + sumOfOdds;
}

export default longestPalindrome;
