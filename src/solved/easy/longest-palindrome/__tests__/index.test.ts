import longestPalindrome from '..';

test('example 1', () => {
  const s = 'abccccdd';
  const output = 7;

  //   Explanation: One longest palindrome that can be built is "dccaccd", whose length is 7.

  expect(longestPalindrome(s)).toEqual(output);
});

test('example 2', () => {
  const s = 'a';
  const output = 1;

  //   Explanation: The longest palindrome that can be built is "a", whose length is 1.

  expect(longestPalindrome(s)).toEqual(output);
});

test('example 3', () => {
  const s = 'cccccdd';
  const output = 7;

  //   Explanation: One longest palindrome that can be built is "dcccccd", whose length is 7.

  expect(longestPalindrome(s)).toEqual(output);
});

test('example 4', () => {
  const s = 'aaaaabbbccccdd';
  const output = 13;

  //   Explanation: One longest palindrome that can be built is "bdccaaaaaccdb", whose length is 7.

  expect(longestPalindrome(s)).toEqual(output);
});

test('example 5', () => {
  const s =
    'ibvjkmpyzsifuxcabqqpahjdeuzaybqsrsmbfplxycsafogotliyvhxjtkrbzqxlyfwujzhkdafhebvsdhkkdbhlhmaoxmbkqiwiusngkbdhlvxdyvnjrzvxmukvdfobzlmvnbnilnsyrgoygfdzjlymhprcpxsnxpcafctikxxybcusgjwmfklkffehbvlhvxfiddznwumxosomfbgxoruoqrhezgsgidgcfzbtdftjxeahriirqgxbhicoxavquhbkaomrroghdnfkknyigsluqebaqrtcwgmlnvmxoagisdmsokeznjsnwpxygjjptvyjjkbmkxvlivinmpnpxgmmorkasebngirckqcawgevljplkkgextudqaodwqmfljljhrujoerycoojwwgtklypicgkyaboqjfivbeqdlonxeidgxsyzugkntoevwfuxovazcyayvwbcqswzhytlmtmrtwpikgacnpkbwgfmpavzyjoxughwhvlsxsgttbcyrlkaarngeoaldsdtjncivhcfsaohmdhgbwkuemcembmlwbwquxfaiukoqvzmgoeppieztdacvwngbkcxknbytvztodbfnjhbtwpjlzuajnlzfmmujhcggpdcwdquutdiubgcvnxvgspmfumeqrofewynizvynavjzkbpkuxxvkjujectdyfwygnfsukvzflcuxxzvxzravzznpxttduajhbsyiywpqunnarabcroljwcbdydagachbobkcvudkoddldaucwruobfylfhyvjuynjrosxczgjwudpxaqwnboxgxybnngxxhibesiaxkicinikzzmonftqkcudlzfzutplbycejmkpxcygsafzkgudy';
  const output = 867;

  expect(longestPalindrome(s)).toEqual(output);
});
