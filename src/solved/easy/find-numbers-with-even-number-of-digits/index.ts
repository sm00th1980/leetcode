function findNumbers(nums: number[]): number {
  return nums.reduce((acc, number) => {
    if (number.toString().length % 2 === 0) {
      return acc + 1;
    }

    return acc;
  }, 0);
}

export default findNumbers;
