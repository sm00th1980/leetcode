function countNegatives(grid: number[][]): number {
  return grid.reduce((total, row) => {
    return row.reduce((acc, value) => {
      return value < 0 ? acc + 1 : acc;
    }, total);
  }, 0);
}

export default countNegatives;
