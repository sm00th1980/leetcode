import countNegatives from '..';

test('example 1', () => {
  const grid = [
    [4, 3, 2, -1],
    [3, 2, 1, -1],
    [1, 1, -1, -2],
    [-1, -1, -2, -3],
  ];
  const output = 8;

  expect(countNegatives(grid)).toEqual(output);
});

test('example 2', () => {
  const grid = [
    [3, 2],
    [1, 0],
  ];
  const output = 0;

  expect(countNegatives(grid)).toEqual(output);
});

test('example 3', () => {
  const grid = [
    [1, -1],
    [-1, -1],
  ];
  const output = 3;

  expect(countNegatives(grid)).toEqual(output);
});

test('example 4', () => {
  const grid = [[-1]];
  const output = 1;

  expect(countNegatives(grid)).toEqual(output);
});
