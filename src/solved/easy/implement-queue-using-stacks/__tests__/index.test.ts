import MyQueue from '..';

test('example 1', () => {
  const myQueue = new MyQueue();
  myQueue.push(1); // queue is: [1]
  myQueue.push(2); // queue is: [1, 2] (leftmost is front of the queue)

  expect(myQueue.peek()).toBe(1); // return 1
  expect(myQueue.pop()).toBe(1); // return 1, queue is [2]

  expect(myQueue.empty()).toBe(false); // return false
});

test('example 2', () => {
  const myQueue = new MyQueue();
  myQueue.push(1); // queue is: [1]
  myQueue.push(2); // queue is: [1, 2] (leftmost is front of the queue)
  myQueue.push(3); // queue is: [1, 2, 3] (leftmost is front of the queue)

  expect(myQueue.peek()).toBe(1); // return 1
  expect(myQueue.pop()).toBe(1); // return 1, queue is [2, 3]

  expect(myQueue.empty()).toBe(false); // return false

  expect(myQueue.peek()).toBe(2); // return 2
  expect(myQueue.pop()).toBe(2); // return 2, queue is [3]

  expect(myQueue.empty()).toBe(false); // return false

  expect(myQueue.peek()).toBe(3); // return 3
  expect(myQueue.pop()).toBe(3); // return 3, queue is []

  expect(myQueue.empty()).toBe(true); // return false
});
