import { last, isNil } from '../../../utils';

const NO_VALUE = undefined;

class Stack {
  elements: Array<number> = [];

  push(element: number) {
    this.elements.push(element);
  }

  pop() {
    if (this.empty()) {
      return NO_VALUE;
    }

    return this.elements.pop();
  }

  peek() {
    if (this.empty()) {
      return NO_VALUE;
    }

    return last(this.elements);
  }

  size() {
    return this.elements.length;
  }

  empty() {
    return this.elements.length <= 0;
  }
}

class MyQueue {
  inputStack: Stack = new Stack();
  outputStack: Stack = new Stack();

  push(x: number): void {
    this.inputStack.push(x);
  }

  pop(): number {
    if (this.empty()) {
      // no values in both stacks
      return (NO_VALUE as unknown) as number;
    }

    if (!this.outputStack.empty()) {
      // we have value in output stack, use it as source
      return this.outputStack.pop() as number;
    }

    // output stack is empty, but inputStack has something.
    // take everything from inputStack and push to output stack

    const values: Array<number> = [];
    while (!this.inputStack.empty()) {
      const value = this.inputStack.pop();
      if (!isNil(value)) {
        values.push(value!);
      }
    }

    values.forEach((value) => {
      this.outputStack.push(value);
    });

    return this.outputStack.pop() as number;
  }

  peek(): number {
    if (this.empty()) {
      // no values in both stacks
      return (NO_VALUE as unknown) as number;
    }

    if (!this.outputStack.empty()) {
      // we have value in output stack, use it as source
      return this.outputStack.peek() as number;
    }

    // output stack is empty, but inputStack has something.
    // take everything from inputStack and push to output stack

    const values: Array<number> = [];
    while (!this.inputStack.empty()) {
      const value = this.inputStack.pop();
      if (!isNil(value)) {
        values.push(value!);
      }
    }

    values.forEach((value) => {
      this.outputStack.push(value);
    });

    return this.outputStack.peek() as number;
  }

  empty(): boolean {
    return this.inputStack.empty() && this.outputStack.empty();
  }
}

export default MyQueue;
