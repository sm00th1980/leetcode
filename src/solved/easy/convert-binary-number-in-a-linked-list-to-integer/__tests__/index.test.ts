import getDecimalValue from '..';
import { listNode } from '../../../../utils/structures';

test('example 1', () => {
  const head = listNode(1, listNode(0, listNode(1)));
  const output = 5;

  expect(getDecimalValue(head)).toEqual(output);
});

test('example 2', () => {
  const head = listNode(0);
  const output = 0;

  expect(getDecimalValue(head)).toEqual(output);
});

test('example 3', () => {
  const head = listNode(1);
  const output = 1;

  expect(getDecimalValue(head)).toEqual(output);
});

test('example 4', () => {
  const head = listNode(
    1,
    listNode(
      0,
      listNode(
        0,
        listNode(
          1,
          listNode(
            0,
            listNode(
              0,
              listNode(
                1,
                listNode(1, listNode(1, listNode(0, listNode(0, listNode(0, listNode(0, listNode(0, listNode(0)))))))),
              ),
            ),
          ),
        ),
      ),
    ),
  );
  const output = 18880;

  expect(getDecimalValue(head)).toEqual(output);
});

test('example 5', () => {
  const head = listNode(0, listNode(0));
  const output = 0;

  expect(getDecimalValue(head)).toEqual(output);
});
