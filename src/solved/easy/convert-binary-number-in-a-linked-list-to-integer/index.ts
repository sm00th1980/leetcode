import { ListNode } from '../../../utils/structures';
import { Option } from '../../../utils/types';
import { isNil } from '../../../utils';

const getNumbers = (head: Option<ListNode>): string => {
  if (isNil(head)) {
    return '';
  }

  return head?.val.toString() + getNumbers(head?.next);
};

function getDecimalValue(head: Option<ListNode>): number {
  return parseInt(getNumbers(head), 2);
}

export default getDecimalValue;
