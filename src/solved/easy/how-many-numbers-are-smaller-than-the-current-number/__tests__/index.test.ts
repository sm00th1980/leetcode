import smallerNumbersThanCurrent from '..';

test('example 1', () => {
  const nums = [8, 1, 2, 2, 3];
  const output = [4, 0, 1, 1, 3];

  expect(smallerNumbersThanCurrent(nums)).toEqual(output);
});

test('example 2', () => {
  const nums = [6, 5, 4, 8];
  const output = [2, 1, 0, 3];

  expect(smallerNumbersThanCurrent(nums)).toEqual(output);
});

test('example 3', () => {
  const nums = [7, 7, 7, 7];
  const output = [0, 0, 0, 0];

  expect(smallerNumbersThanCurrent(nums)).toEqual(output);
});
