const getSmallerCount = (index: number, x: number, xs: number[]): number => {
  return xs.reduce((acc, value, currentIndex) => {
    if (x > value && index !== currentIndex) {
      return acc + 1;
    }

    return acc;
  }, 0);
};

function smallerNumbersThanCurrent(nums: number[]): number[] {
  return nums.map((value, index) => getSmallerCount(index, value, nums));
}

export default smallerNumbersThanCurrent;
