const getMiddle = (xs: number[]): number[] => {
  if (xs.length === 3) {
    return xs;
  }

  const middleIndex = Math.floor(xs.length / 2);
  return [xs[middleIndex - 1], xs[middleIndex], xs[middleIndex + 1]];
};

export const takeLeft = (xs: number[]): number[] => {
  const middleIndex = Math.floor(xs.length / 2);
  const [beforeMiddle, middle] = getMiddle(xs);
  if (beforeMiddle === middle) {
    return xs.slice(0, middleIndex + 1);
  }
  return xs.slice(0, middleIndex);
};

export const takeRight = (xs: number[]): number[] => {
  const middleIndex = Math.floor(xs.length / 2);
  const [beforeMiddle, middle] = getMiddle(xs);
  if (beforeMiddle === middle) {
    return xs.slice(middleIndex + 1);
  }

  return xs.slice(middleIndex);
};

const findSingleNumber = (xs: number[]): number => {
  if (xs.length === 1) {
    return xs[0];
  }
  const [beforeMiddle, middle, afterMiddle] = getMiddle(xs);

  if (beforeMiddle < middle && middle < afterMiddle) {
    return middle;
  }

  if (xs.length === 3) {
    return middle === afterMiddle ? beforeMiddle : afterMiddle;
  }

  const left = takeLeft(xs);
  const right = takeRight(xs);

  const evenPairs = ((xs.length - 1) / 2) % 2 === 0;
  if (evenPairs) {
    return middle === afterMiddle ? findSingleNumber(right) : findSingleNumber(left);
  } else {
    return middle === afterMiddle ? findSingleNumber(left) : findSingleNumber(right);
  }
};

function singleNumber(nums: number[]): number {
  nums.sort((a, b) => a - b);
  return findSingleNumber(nums);
}

export default singleNumber;
