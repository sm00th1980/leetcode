import { takeLeft } from '..';

test('example 1', () => {
  const input = [-2, 1, 1, 3, 3, 4, 4];
  const output = [-2, 1, 1];

  expect(takeLeft(input)).toEqual(output);
});

test('example 2', () => {
  const input = [-1, 1, 1, 2, 2];
  const output = [-1, 1, 1];

  expect(takeLeft(input)).toEqual(output);
});

test('example 3', () => {
  const input = [-1, 1, 1, 2, 2, 3, 3];
  const output = [-1, 1, 1];

  expect(takeLeft(input)).toEqual(output);
});

test('example 4', () => {
  const input = [-1, 1, 1, 2, 2, 3, 3, 4, 4];
  const output = [-1, 1, 1, 2, 2];

  expect(takeLeft(input)).toEqual(output);
});
