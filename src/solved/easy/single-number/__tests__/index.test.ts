import singleNumber from '..';

test('example 0', () => {
  const input = [1, 2, 2];
  const output = 1;

  expect(singleNumber(input)).toEqual(output);
});

test('example 1', () => {
  const input = [2, 2, 1];
  const output = 1;

  expect(singleNumber(input)).toEqual(output);
});

test('example 2', () => {
  const input = [1, 1, 2, 2, 4];
  const output = 4;

  expect(singleNumber(input)).toEqual(output);
});

test('example 3', () => {
  const input = [-1, 1, 1, 3, 3];
  const output = -1;

  expect(singleNumber(input)).toEqual(output);
});

test('example 4', () => {
  const input = [-2, -2, 1, 1, 3, 3, 4];
  const output = 4;

  expect(singleNumber(input)).toEqual(output);
});

test('example 5', () => {
  const input = [-2, 1, 1, 3, 3, 4, 4];
  const output = -2;

  expect(singleNumber(input)).toEqual(output);
});

test('example 4', () => {
  const input = [1];
  const output = 1;

  expect(singleNumber(input)).toEqual(output);
});

test('example 5', () => {
  const input = [17, 12, 5, -6, 12, 4, 17, -5, 2, -3, 2, 4, 5, 16, -3, -4, 15, 15, -4, -5, -6];
  const output = 16;

  expect(singleNumber(input)).toEqual(output);
});
