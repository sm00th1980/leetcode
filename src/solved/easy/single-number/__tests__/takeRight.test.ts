import { takeRight } from '..';

test('example 1', () => {
  const input = [1, 1, 2, 2, 4];
  const output = [2, 2, 4];

  expect(takeRight(input)).toEqual(output);
});

test('example 2', () => {
  const input = [1, 1, 3, 3, 4, 4, 5];
  const output = [4, 4, 5];

  expect(takeRight(input)).toEqual(output);
});

test('example 3', () => {
  const input = [-6, -6, -5, -5, -4, -4, -3, -3, 2, 2, 4, 4, 5, 5, 12, 12, 15, 15, 16, 17, 17];
  const output = [4, 4, 5, 5, 12, 12, 15, 15, 16, 17, 17];

  expect(takeRight(input)).toEqual(output);
});
