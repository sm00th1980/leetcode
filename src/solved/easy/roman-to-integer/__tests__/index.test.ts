import romanToInt from '..';

test('example 1', () => {
  const input = 'III';
  const output = 3;

  expect(romanToInt(input)).toEqual(output);
});

test('example 2', () => {
  const input = 'LVIII';
  const output = 58;

  expect(romanToInt(input)).toEqual(output);
});

test('example 3', () => {
  const input = 'MCMXCIV';
  const output = 1994;

  expect(romanToInt(input)).toEqual(output);
});

test('example 4', () => {
  const input = 'II';
  const output = 2;

  expect(romanToInt(input)).toEqual(output);
});

test('example 5', () => {
  const input = 'XII';
  const output = 12;

  expect(romanToInt(input)).toEqual(output);
});

test('example 6', () => {
  const input = 'XXVII';
  const output = 27;

  expect(romanToInt(input)).toEqual(output);
});
