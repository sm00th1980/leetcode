import { getSum, isNil } from '../../../utils';

const VALUES = {
  I: 1,
  IV: 4,
  V: 5,
  IX: 9,
  X: 10,
  XL: 40,
  L: 50,
  XC: 90,
  C: 100,
  CD: 400,
  D: 500,
  CM: 900,
  M: 1000,
};

type KEYS = keyof typeof VALUES;

const TWO_LETTER_KEYS = Object.keys(VALUES).filter((key) => key.length >= 2);

const mapRomanToInteger = (xs: KEYS[]): number[] => xs.map((x) => VALUES[x]);

const rejectNils = (xs: any[]) => xs.filter((x) => !isNil(x));

type InitialValue = {
  index: number;
  result: number[];
};

const initialValue: InitialValue = {
  index: 0,
  result: [],
};

function romanToInt(s: string): number {
  const { result } = s.split('').reduce<InitialValue>((acc) => {
    const twoLettersValue = s.substring(acc.index, acc.index + 2);

    if (TWO_LETTER_KEYS.includes(twoLettersValue)) {
      return {
        index: acc.index + 2,
        result: [...acc.result, twoLettersValue],
      } as InitialValue;
    }

    return {
      index: acc.index + 1,
      result: [...acc.result, twoLettersValue[0]],
    } as InitialValue;
  }, initialValue);

  return getSum(mapRomanToInteger(rejectNils(result)));
}

export default romanToInt;
