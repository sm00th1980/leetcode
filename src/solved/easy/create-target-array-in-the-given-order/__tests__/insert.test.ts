import { insert } from '..';

test('example 1', () => {
  const index = 0;
  const value = 0;
  const xs: number[] = [];

  const output = [0];

  expect(insert(index, value, xs)).toEqual(output);
});

test('example 2', () => {
  const index = 2;
  const value = 3;
  const xs = [0, 1, 2];

  const output = [0, 1, 3, 2];

  expect(insert(index, value, xs)).toEqual(output);
});

test('example 3', () => {
  const index = 1;
  const value = 1;
  const xs = [0];

  const output = [0, 1];

  expect(insert(index, value, xs)).toEqual(output);
});

test('example 4', () => {
  const index = 1;
  const value = 4;
  const xs = [0, 1, 3, 2];

  const output = [0, 4, 1, 3, 2];

  expect(insert(index, value, xs)).toEqual(output);
});
