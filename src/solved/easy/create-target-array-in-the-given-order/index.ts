export const insert = (index: number, value: number, xs: number[]): number[] => {
  const before = xs.slice(0, index);
  const after = xs.slice(index);

  return [...before, value, ...after];
};

function createTargetArray(nums: number[], index: number[]): number[] {
  const initialValue: number[] = [];
  return nums.reduce((acc, value, i) => {
    const idx = index[i];
    acc = insert(idx, value, acc);
    return acc;
  }, initialValue);
}

export default createTargetArray;
