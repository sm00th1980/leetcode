import defangIPaddr from '..';

test('example 1', () => {
  const input = '1.1.1.1';

  expect(defangIPaddr(input)).toBe('1[.]1[.]1[.]1');
});

test('example 2', () => {
  const input = '255.100.50.0';

  expect(defangIPaddr(input)).toBe('255[.]100[.]50[.]0');
});
