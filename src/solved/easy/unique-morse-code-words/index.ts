const CODE: { [letter: string]: string } = {
  a: '.-',
  b: '-...',
  c: '-.-.',
  d: '-..',
  e: '.',
  f: '..-.',
  g: '--.',
  h: '....',
  i: '..',
  j: '.---',
  k: '-.-',
  l: '.-..',
  m: '--',
  n: '-.',
  o: '---',
  p: '.--.',
  q: '--.-',
  r: '.-.',
  s: '...',
  t: '-',
  u: '..-',
  v: '...-',
  w: '.--',
  x: '-..-',
  y: '-.--',
  z: '--..',
};

function uniqueMorseRepresentations(words: string[]): number {
  const codedWords = words.map((word) => {
    return word
      .split('')
      .map((letter) => CODE[letter])
      .join('');
  });

  return new Set(codedWords).size;
}

export default uniqueMorseRepresentations;
