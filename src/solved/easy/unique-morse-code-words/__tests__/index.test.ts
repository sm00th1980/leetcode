import uniqueMorseRepresentations from '..';

test('example 1', () => {
  const words = ['gin', 'zen', 'gig', 'msg'];
  const output = 2;

  expect(uniqueMorseRepresentations(words)).toEqual(output);
});
