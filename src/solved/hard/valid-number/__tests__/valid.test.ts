import isNumber from '..';

test('example 1', () => {
  const input = '0';
  const output = true;

  expect(isNumber(input)).toBe(output);
});

test('example 2', () => {
  const input = '.1';
  const output = true;

  expect(isNumber(input)).toBe(output);
});

test('example 3', () => {
  const input = '2';
  const output = true;

  expect(isNumber(input)).toBe(output);
});

test('example 4', () => {
  const input = '0089';
  const output = true;

  expect(isNumber(input)).toBe(output);
});

test('example 5', () => {
  const input = '-0.1';
  const output = true;

  expect(isNumber(input)).toBe(output);
});

test('example 6', () => {
  const input = '+3.14';
  const output = true;

  expect(isNumber(input)).toBe(output);
});

test('example 7', () => {
  const input = '4.';
  const output = true;

  expect(isNumber(input)).toBe(output);
});

test('example 8', () => {
  const input = '-.9';
  const output = true;

  expect(isNumber(input)).toBe(output);
});

test('example 9', () => {
  const input = '2e10';
  const output = true;

  expect(isNumber(input)).toBe(output);
});

test('example 10', () => {
  const input = '-90E3';
  const output = true;

  expect(isNumber(input)).toBe(output);
});

test('example 11', () => {
  const input = '3e+7';
  const output = true;

  expect(isNumber(input)).toBe(output);
});

test('example 14', () => {
  const input = '+6e-1';
  const output = true;

  expect(isNumber(input)).toBe(output);
});

test('example 12', () => {
  const input = '53.5e93';
  const output = true;

  expect(isNumber(input)).toBe(output);
});

test('example 13', () => {
  const input = '-123.456e789';
  const output = true;

  expect(isNumber(input)).toBe(output);
});
