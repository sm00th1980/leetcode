import { normalize } from '..';

test('example 1', () => {
  const input = '+1';
  const output = '1';

  expect(normalize(input)).toEqual(output);
});

test('example 2', () => {
  const input = '-1';
  const output = '1';

  expect(normalize(input)).toEqual(output);
});

test('example 3', () => {
  const input = '0089';
  const output = '0089';

  expect(normalize(input)).toEqual(output);
});

test('example 4', () => {
  const input = '1E2';
  const output = '1e2';

  expect(normalize(input)).toEqual(output);
});

test('example 5', () => {
  const input = '--1';
  const output = '--1';

  expect(normalize(input)).toEqual(output);
});

test('example 6', () => {
  const input = '-0089';
  const output = '0089';

  expect(normalize(input)).toEqual(output);
});

test('example 7', () => {
  const input = '+0089';
  const output = '0089';

  expect(normalize(input)).toEqual(output);
});

test('example 8', () => {
  const input = '-+1';
  const output = '-+1';

  expect(normalize(input)).toEqual(output);
});

test('example 9', () => {
  const input = '-.9';
  const output = '.9';

  expect(normalize(input)).toEqual(output);
});

test('example 10', () => {
  const input = '+';
  const output = '';

  expect(normalize(input)).toEqual(output);
});
