import isNumber from '..';

test('example 1', () => {
  const input = 'e';
  const output = false;

  expect(isNumber(input)).toBe(output);
});

test('example 2', () => {
  const input = '.';
  const output = false;

  expect(isNumber(input)).toBe(output);
});

test('example 3', () => {
  const input = 'abc';
  const output = false;

  expect(isNumber(input)).toBe(output);
});

test('example 4', () => {
  const input = '1a';
  const output = false;

  expect(isNumber(input)).toBe(output);
});

test('example 5', () => {
  const input = '1e';
  const output = false;

  expect(isNumber(input)).toBe(output);
});

test('example 6', () => {
  const input = 'e3';
  const output = false;

  expect(isNumber(input)).toBe(output);
});

test('example 7', () => {
  const input = '99e2.5';
  const output = false;

  expect(isNumber(input)).toBe(output);
});

test('example 8', () => {
  const input = '--6';
  const output = false;

  expect(isNumber(input)).toBe(output);
});

test('example 9', () => {
  const input = '-+3';
  const output = false;

  expect(isNumber(input)).toBe(output);
});

test('example 10', () => {
  const input = '95a54e53';
  const output = false;

  expect(isNumber(input)).toBe(output);
});

test('example 11', () => {
  const input = '+eo';
  const output = false;

  expect(isNumber(input)).toBe(output);
});
