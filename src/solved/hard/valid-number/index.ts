import { head, tail } from '../../../utils';

const onlyLetters = (letters: string[], str: string): string => {
  return str
    .split('')
    .filter((letter) => letters.includes(letter))
    .join('');
};

const isOnlyOneOrZero = (letter: string, str: string): boolean => {
  const count = str.split('').filter((l) => l === letter).length;
  return count === 0 || count === 1;
};

const isOnlyOne = (letter: string, str: string): boolean => {
  const count = str.split('').filter((l) => l === letter).length;
  return count === 1;
};

const isOnlyDigits = (str: string): boolean => {
  const digits = onlyLetters(['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'], str);
  if (digits === str) {
    return true;
  }

  return false;
};

const isDigitsAndOnePointOnly = (str: string): boolean => {
  const letters = onlyLetters(['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.'], str);
  if (letters === str && isOnlyOneOrZero('.', str)) {
    return true;
  }

  return false;
};

const isHasOneMantissaOnly = (str: string): boolean => {
  return isOnlyOne('e', str);
};

export const normalize = (str: string): string => {
  if (str === '+' || str === '-') {
    return '';
  }

  const letters = str.split('');
  const firstLetter = head(letters);
  const secondLetter = head(tail(letters));

  if ((firstLetter === '+' || firstLetter === '-') && (isOnlyDigits(secondLetter) || secondLetter === '.')) {
    return tail(letters).join('').toLocaleLowerCase();
  }

  return letters.join('').toLocaleLowerCase();
};

const isValidRatioNumber = (str: string): boolean => {
  if (isOnlyDigits(str)) {
    return true;
  }

  if (isDigitsAndOnePointOnly(str)) {
    const [integerPart, ratioPart] = str.split('.');

    if (integerPart === '' && ratioPart === '') {
      return false;
    }

    if (isOnlyDigits(integerPart) && isOnlyDigits(ratioPart)) {
      return true;
    }
  }

  return false;
};

function isNumber(s: string): boolean {
  const normalizedString = normalize(s);

  if (isOnlyDigits(normalizedString)) {
    return true;
  }

  if (isValidRatioNumber(normalizedString)) {
    return true;
  }

  if (isHasOneMantissaOnly(normalizedString)) {
    const [numberPart, mantissaPart] = normalizedString.split('e');

    const normalizedNumberPart = normalize(numberPart);
    const normalizedMantissaPart = normalize(mantissaPart);

    if (normalizedNumberPart === '' || normalizedMantissaPart === '') {
      return false;
    }

    if (isOnlyDigits(normalizedNumberPart) && isOnlyDigits(normalizedMantissaPart)) {
      return true;
    }

    if (isValidRatioNumber(normalizedNumberPart) && isOnlyDigits(normalizedMantissaPart)) {
      return true;
    }
  }

  return false;
}

export default isNumber;
