import calculate from '..';

test('example 1', () => {
  const input = '1 + 1';

  expect(calculate(input)).toBe(2);
});

test('example 2', () => {
  const input = ' 2-1 + 2 ';

  expect(calculate(input)).toBe(3);
});

test('example 3', () => {
  const input = '(1+(4+5+2)-3)+(6+8)';

  expect(calculate(input)).toBe(23);
});

test('example 4', () => {
  const input = ' -2-1 + 2 ';

  expect(calculate(input)).toBe(-1);
});

test('example 5', () => {
  const input = '2147483647';

  expect(calculate(input)).toBe(2147483647);
});

test('example 6', () => {
  const input = ' 214';

  expect(calculate(input)).toBe(214);
});

test('example 7', () => {
  const input = ' (214) ';

  expect(calculate(input)).toBe(214);
});

test('example 8', () => {
  const input = '- 12';

  expect(calculate(input)).toBe(-12);
});

test('example 9', () => {
  const input = '- (3 + (4 + 5))';

  expect(calculate(input)).toBe(-12);
});

test('example 10', () => {
  const input = '-2+ 1';

  expect(calculate(input)).toBe(-1);
});

test('example 11', () => {
  const input = '1-(+1+1)';

  expect(calculate(input)).toBe(-1);
});

test('example 12', () => {
  const input = '2-(5-6)';

  expect(calculate(input)).toBe(3);
});

test('example 13', () => {
  const input = '1-(3+5-2+(3+19-(3-1-4+(9-4-(4-(1+(3)-2)-5)+8-(3-5)-1)-4)-5)-4+3-9)-4-(3+2-5)-10';

  expect(calculate(input)).toBe(-15);
});

test('example 14', () => {
  const input =
    '5+3-4-(1+2-7+(10-1+3+5+(3-0+(8-(3+(8-(10-(6-10-8-7+(0+0+7)-10+5-3-2+(9+0+(7+(2-(2-(9)-2+5+4+2+(2+9+1+5+5-8-9-2-9+1+0)-(5-(9)-(0-(7+9)+(10+(6-4+6))+0-2+(10+7+(8+(7-(8-(3)+(2)+(10-6+10-(2)-7-(2)+(3+(8))+(1-3-8)+6-(4+1)+(6))+6-(1)-(10+(4)+(8)+(5+(0))+(3-(6))-(9)-(4)+(2))))))-1)))+(9+6)+(0))))+3-(1))+(7))))))))';

  expect(calculate(input)).toBe(-35);
});
