import { splitByOperations } from '..';

test('example 1', () => {
  const input = '1 + 1';

  expect(splitByOperations(input)).toEqual(['+1', '+1']);
});

test('example 2', () => {
  const input = ' 2-1 + 2 ';

  expect(splitByOperations(input)).toEqual(['+2', '-1', '+2']);
});

test('example 3', () => {
  const input = ' -2-1 + 2 ';

  expect(splitByOperations(input)).toEqual(['-2', '-1', '+2']);
});

test('example 4', () => {
  const input = '6+14';

  expect(splitByOperations(input)).toEqual(['+6', '+14']);
});

test('example 5', () => {
  const input = '6-14';

  expect(splitByOperations(input)).toEqual(['+6', '-14']);
});

test('example 6', () => {
  const input = '6+14+2';

  expect(splitByOperations(input)).toEqual(['+6', '+14', '+2']);
});

test('example 7', () => {
  const input = '-6-14';

  expect(splitByOperations(input)).toEqual(['-6', '-14']);
});
