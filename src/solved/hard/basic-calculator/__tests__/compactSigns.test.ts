import { compactSigns } from '..';

test('example 1', () => {
  const input = '2--1';

  expect(compactSigns(input)).toEqual('+2+1');
});

test('example 2', () => {
  const input = '2---1';

  expect(compactSigns(input)).toEqual('+2-1');
});

test('example 3', () => {
  const input = '2++1';

  expect(compactSigns(input)).toEqual('+2+1');
});

test('example 4', () => {
  const input = '2+++1';

  expect(compactSigns(input)).toEqual('+2+1');
});

test('example 5', () => {
  const input = '2+-+1';

  expect(compactSigns(input)).toEqual('+2-1');
});

test('example 6', () => {
  const input = '+1+1';

  expect(compactSigns(input)).toEqual('+1+1');
});

test('example 7', () => {
  const input = '9-4--3+8--2-1';

  expect(compactSigns(input)).toEqual('+9-4+3+8+2-1');
});

test('example 8', () => {
  const input = '2-9-2+5+4+2+-5--35';

  expect(compactSigns(input)).toBe('+2-9-2+5+4+2-5+35');
});
