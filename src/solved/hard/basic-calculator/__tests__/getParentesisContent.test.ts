import { getParentesisContent } from '..';

test('example 1', () => {
  const input = '- (3 + (4 + 5))';

  expect(getParentesisContent(input)).toEqual(['- (3 + ', '(4 + 5)', ')']);
});

test('example 2', () => {
  const input = '- (3 + (4 + 5)) +7';

  expect(getParentesisContent(input)).toEqual(['- (3 + ', '(4 + 5)', ') +7']);
});

test('example 3', () => {
  const input = '(1+(4+5+2)-3)+(6+8)';

  expect(getParentesisContent(input)).toEqual(['(1+(4+5+2)-3)+', '(6+8)', '']);
});
