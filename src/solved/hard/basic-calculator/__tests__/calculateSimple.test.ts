import { calculateSimple } from '..';

test('example 1', () => {
  const input = '1 + 1';

  expect(calculateSimple(input)).toBe(2);
});

test('example 2', () => {
  const input = ' 2-1 + 2 ';

  expect(calculateSimple(input)).toBe(3);
});

test('example 3', () => {
  const input = ' -2-1 + 2 ';

  expect(calculateSimple(input)).toBe(-1);
});

test('example 4', () => {
  const input = '6+14';

  expect(calculateSimple(input)).toBe(20);
});

test('example 5', () => {
  const input = '2147483647';

  expect(calculateSimple(input)).toBe(2147483647);
});

test('example 6', () => {
  const input = ' 214';

  expect(calculateSimple(input)).toBe(214);
});

test('example 7', () => {
  const input = ' (214) ';

  expect(calculateSimple(input)).toBe(214);
});

test('example 8', () => {
  const input = '- 12';

  expect(calculateSimple(input)).toBe(-12);
});

test('example 9', () => {
  const input = '6-14';

  expect(calculateSimple(input)).toBe(-8);
});

test('example 10', () => {
  const input = '-2+ 1';

  expect(calculateSimple(input)).toBe(-1);
});

test('example 11', () => {
  const input = '(+1+1)';

  expect(calculateSimple(input)).toBe(2);
});

test('example 12', () => {
  const input = '2--1';

  expect(calculateSimple(input)).toBe(3);
});

test('example 13', () => {
  const input = '(9-4--3+8--2-1)';

  expect(calculateSimple(input)).toBe(17);
});

test('example 14', () => {
  const input = '(2-9-2+5+4+2+-5--35)';

  expect(calculateSimple(input)).toBe(32);
});
