import { getSum, getCount, head, zip, tail, initial } from '../../../utils';

const strip = (s: string): string => {
  return s
    .split('')
    .filter((letter) => !['(', ')', ' '].includes(letter))
    .join('');
};

const hasOnlyPluses = (s: string): boolean => {
  const letters = s.split('');
  return getCount('+', letters) === letters.length;
};

const hasEvenMinuses = (s: string): boolean => {
  const letters = s.split('');
  return getCount('-', letters) % 2 === 0;
};

const withoutEmpty = (xs: string[]): string[] => xs.filter((x) => x !== '');

export const compactSigns = (s: string): string => {
  const stripped = strip(s);

  const signedStripped = stripped[0] === '-' || stripped[0] === '+' ? stripped : `+${stripped}`;

  const onlyDigits = ['', ...withoutEmpty(signedStripped.split(/\++|-+/))];
  const onlySigns = signedStripped.split(/\d+/);

  const initialValue: string[] = [];
  const compactedSigns = onlySigns.reduce((acc, signs) => {
    if (signs === '') {
      return [...acc, ''];
    }

    if (hasOnlyPluses(signs)) {
      return [...acc, '+'];
    }

    if (hasEvenMinuses(signs)) {
      return [...acc, '+'];
    }
    return [...acc, '-'];
  }, initialValue);

  return zip(onlyDigits, compactedSigns)
    .map((pair) => pair.join(''))
    .join('');
};

export const splitByOperations = (s: string): string[] => {
  const stripped = compactSigns(strip(s));

  const signedStripped = stripped[0] === '-' || stripped[0] === '+' ? stripped : `+${stripped}`;

  const onlyDigits = tail(signedStripped.split(/\+|-/));
  const onlySigns = initial(signedStripped.split(/\d+/));

  return zip(onlySigns, onlyDigits).map((pair) => pair.join(''));
};

const hasOperations = (s: string): boolean => {
  const letters = s.split('');
  return letters.includes('+') || letters.includes('-');
};

const isNegativeNumber = (s: string): boolean => {
  const letters = s.split('');
  return getCount('-', letters) === 1 && head(letters) === '-' && getCount('+', letters) === 0;
};

export const calculateSimple = (s: string): number => {
  const stripped = strip(s);

  if (isNegativeNumber(stripped)) {
    return parseInt(stripped, 10);
  }

  if (!hasOperations(stripped)) {
    return parseInt(stripped, 10);
  }

  return getSum(splitByOperations(stripped).map((letter) => parseInt(letter, 10)));
};

export const getParentesisContent = (s: string): [string, string, string] => {
  const openParentesisIndex = s.lastIndexOf('(');
  const closeParentesisIndex = s.slice(openParentesisIndex).indexOf(')');

  return [
    s.slice(0, openParentesisIndex),
    s.slice(openParentesisIndex, openParentesisIndex + closeParentesisIndex + 1),
    s.slice(openParentesisIndex + closeParentesisIndex + 1),
  ];
};

const hasParentesises = (s: string): boolean => {
  const letters = s.split('');
  return letters.includes('(') || letters.includes(')');
};

function calculate(s: string): number {
  if (!hasParentesises(s)) {
    return calculateSimple(s);
  }

  const [start, operation, end] = getParentesisContent(s);
  // console.log('start:', start, 'op:', operation, 'end:', end);
  // console.log('>', `${start}${calculateSimple(operation)}${end}`);

  return calculate(`${start}${calculateSimple(operation)}${end}`);
}

export default calculate;
