import { sort, last, isEmpty, initial, head, curry } from '../../utils';

type Range = number[];
type InitialValue = Array<Range>;

const format = (range: Range) => {
  if (range.length === 1) {
    return range.join('-');
  }

  return [head(range), last(range)].join('-');
};

const ascend = (a: number, b: number) => a - b;
const sortByAsc = curry(sort)(ascend);

export const compact = (xs: number[]): string[] => {
  const sorted: number[] = sortByAsc(xs);
  const initialValue: InitialValue = [];

  return sorted
    .reduce((ranges, currentValue, currentIndex) => {
      if (isEmpty(ranges)) {
        return [[currentValue]];
      }

      const previousIndex = currentIndex - 1;
      const previousValue = sorted[previousIndex];

      if (currentValue - 1 === previousValue) {
        const lastRange = last(ranges);
        return [...initial(ranges), [...lastRange, currentValue]];
      }

      return [...ranges, [currentValue]];
    }, initialValue)
    .map(format);
};
