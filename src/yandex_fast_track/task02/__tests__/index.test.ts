import { compact } from '..';

test('example 1', () => {
  const input = [1, 2, 3, 4];
  const output = ['1-4'];

  expect(compact(input)).toEqual(output);
});

test('example 2', () => {
  const input = [1, 2, 3, 4, 6, 7, 8, 11];
  const output = ['1-4', '6-8', '11'];

  expect(compact(input)).toEqual(output);
});

test('example 3', () => {
  const input = [11];
  const output = ['11'];

  expect(compact(input)).toEqual(output);
});

test('example 4', () => {
  const input = [6, 1, 2, 7, 3, 4, 8, 11];
  const output = ['1-4', '6-8', '11'];

  expect(compact(input)).toEqual(output);
});

test('example 5', () => {
  const input = [1, 2, 3, 4, 6, 7, 8, 11, 10];
  const output = ['1-4', '6-8', '10-11'];

  expect(compact(input)).toEqual(output);
});

test('example 6', () => {
  const input = [1, 2, 3, 4, 6, 7, 8, 11, 10, 100];
  const output = ['1-4', '6-8', '10-11', '100'];

  expect(compact(input)).toEqual(output);
});
