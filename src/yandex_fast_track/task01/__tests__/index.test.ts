/* eslint @typescript-eslint/no-var-requires: off */

const check = require('..');

test('example 1', () => {
  const input1 = 1;
  const input2 = 1;

  const output = true;

  expect(check(input1, input2)).toEqual(output);
});

test('example 2', () => {
  const input1 = 1;
  const input2 = { _COMMUNICATOR_CONTROL_: { censorship: true } };

  const output = false;

  expect(check(input1, input2)).toEqual(output);
});

test('example 3', () => {
  const input1 = [1, 2];
  const input2 = [1, 2];

  const output = true;

  expect(check(input1, input2)).toEqual(output);
});

test('example 4', () => {
  const input1 = { a: 1, b: [1] };
  const input2 = { a: 1, b: [2] };

  const output = false;

  expect(check(input1, input2)).toEqual(output);
});

test('example 41', () => {
  const input1 = { a: 1, b: [2] };
  const input2 = { a: 1, b: [2] };

  const output = true;

  expect(check(input1, input2)).toEqual(output);
});

test('example 5', () => {
  const input1 = [1, 2];
  const input2 = [1, { _COMMUNICATOR_CONTROL_: { validated: true, content: 2 } }];

  const output = true;

  expect(check(input1, input2)).toEqual(output);
});

test('example 6', () => {
  const input1 = [1, 2];
  const input2 = [1, { _COMMUNICATOR_CONTROL_: { censorship: true } }];

  const output = false;

  expect(check(input1, input2)).toEqual(output);
});

test('example 7', () => {
  const input1 = {
    a: [1, 2, 3],
    b: [1, 2, 3],
    c: [1, 2, 3],
  };
  const input2 = {
    a: { _COMMUNICATOR_CONTROL_: { validated: true, hash: '123', content: [1, 2, 3] } },
    b: { _COMMUNICATOR_CONTROL_: { validated: true, hash: '123', content: [1, 2, 3] } },
    c: { _COMMUNICATOR_CONTROL_: { validated: true, hash: '123', content: [1, 2, 3] } },
  };

  const output = true;

  expect(check(input1, input2)).toEqual(output);
});

test('example 71', () => {
  const input1 = {
    a: [1, 2, 3],
    b: [1, 2, 3],
    c: [1, 2, 3],
  };
  const input2 = {
    a: { _COMMUNICATOR_CONTROL_: { validated: true, hash: '123', content: [1, 2, 3] } },
    b: { _COMMUNICATOR_CONTROL_: { validated: true, hash: '123', content: [1, 2, 3] } },
  };

  const output = false;

  expect(check(input1, input2)).toEqual(output);
});

test('example 8', () => {
  const input1 = {
    a: [1, 2, 3],
    b: [1, 2, 3],
    c: [1, 2, 3],
    d: [4, 5, 6],
  };
  const input2 = {
    a: { _COMMUNICATOR_CONTROL_: { validated: true, hash: '123', content: [1, 2, 3] } },
    b: { _COMMUNICATOR_CONTROL_: { validated: true, hash: '123', content: [1, 2, 3] } },
    c: { _COMMUNICATOR_CONTROL_: { validated: true, hash: '123', content: [1, 2, 3] } },
    d: [1, 2, 3],
  };

  const output = false;

  expect(check(input1, input2)).toEqual(output);
});

test('example 9', () => {
  const input1 = {
    a: { x: '.', y: '!', z: '?' },
    b: { x: '.', y: '!', z: '?' },
    c: { x: '.', y: '!', z: '?' },
  };
  const input2 = {
    a: {
      _COMMUNICATOR_CONTROL_: {
        validated: true,
        hash: 'xyz',
        content: {
          x: '.',
          y: { _COMMUNICATOR_CONTROL_: { censorship: true } },
          z: '?',
        },
      },
    },
    b: {
      _COMMUNICATOR_CONTROL_: {
        validated: true,
        hash: 'xyz',
        content: {
          x: '.',
          y: { _COMMUNICATOR_CONTROL_: { censorship: true } },
          z: '?',
        },
      },
    },
    c: {
      _COMMUNICATOR_CONTROL_: {
        validated: true,
        hash: 'xyz',
        content: {
          x: '.',
          y: { _COMMUNICATOR_CONTROL_: { censorship: true } },
          z: '?',
        },
      },
    },
  };

  const output = false;

  expect(check(input1, input2)).toEqual(output);
});

test('example 10', () => {
  const input1 = {
    a: { x: '.', y: '!', z: '?' },
  };

  const input2 = {
    a: {
      _COMMUNICATOR_CONTROL_: {
        validated: true,
        content: {
          x: '.',
          y: { _COMMUNICATOR_CONTROL_: { validated: true, content: '!' } },
          z: '?',
        },
      },
    },
  };

  const output = true;

  expect(check(input1, input2)).toEqual(output);
});

test('example 11', () => {
  const input1 = {
    a: { x: '.', y: '!', z: '?' },
    b: true,
  };
  const input2 = {
    a: {
      _COMMUNICATOR_CONTROL_: {
        validated: true,
        content: {
          x: '.',
          y: { _COMMUNICATOR_CONTROL_: { validated: true, content: '!' } },
          z: '?',
        },
      },
    },
  };

  const output = false;

  expect(check(input1, input2)).toEqual(output);
});
