import { path, isArray, all, isObject, equals, zip } from '../../utils';

const compareArrays = <T>(xs1: T[], xs2: T[]): boolean => {
  if (xs1.length !== xs2.length) {
    return false;
  }

  return all(([value1, value2]) => compareValues(value1, value2), zip(xs1, xs2));
};

const compareObjects = (obj1: Record<string, unknown>, obj2: Record<string, unknown>): boolean => {
  if (obj1 === obj2) {
    return true;
  }

  const keys1 = Object.keys(obj1);
  const values1 = Object.values(obj1);

  const keys2 = Object.keys(obj2);
  const values2 = Object.values(obj2);

  if (equals(keys1, keys2) && equals(values1, values2)) {
    return true;
  }

  if (!equals(keys1, keys2)) {
    return false;
  }

  return all(([value1, value2]) => compareValues(value1, value2), zip(values1, values2));
};

const compareValues = (value1: any, value2: any): boolean => {
  if (value1 === value2) {
    return true;
  }

  const extractedValue2 = path(['_COMMUNICATOR_CONTROL_', 'content'], value2);

  if (isArray(value1) && isArray(extractedValue2)) {
    return equals(value1, extractedValue2);
  }

  if (isObject(value1) && isObject(extractedValue2)) {
    return compareObjects(value1, extractedValue2 as Record<string, unknown>);
  }

  return value1 === extractedValue2;
};

function check(origin: any, sent: any): boolean {
  if (origin === sent) {
    return true;
  }

  if (isArray(origin) && isArray(sent)) {
    return compareArrays(origin, sent);
  }

  if (isObject(origin) && isObject(sent)) {
    return compareObjects(origin, sent);
  }

  return false;
}

module.exports = check;
