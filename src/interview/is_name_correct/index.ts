import { isEmpty, head, tail } from '../../utils';
import { compactMultipleLetters } from './utils';

const normalize = (value: string): string => value.toLowerCase();

const headAndTail = (xs: string[]): [string, string[]] => [head(xs), tail(xs)];

const hasSameLettersWithSameOrder = (nameLetters: string[], seenNameLetters: string[]): boolean => {
  if (isEmpty(seenNameLetters)) {
    return true;
  }

  const [nameFirstLetter, nameRestLetters] = headAndTail(nameLetters);
  const [seenNameFirstLetter, seenNameRestLetters] = headAndTail(seenNameLetters);

  if (nameFirstLetter === seenNameFirstLetter) {
    return hasSameLettersWithSameOrder(nameRestLetters, seenNameRestLetters);
  }

  if (!nameRestLetters.includes(seenNameFirstLetter)) {
    return false;
  }

  const index = nameLetters.findIndex((letter) => letter === seenNameFirstLetter);
  return hasSameLettersWithSameOrder(nameLetters.slice(index + 1), seenNameRestLetters);
};

export const isNameCorrect = (name: string, seenName: string): boolean => {
  const nameLetters = normalize(name).split('');
  const seenNameLetters = compactMultipleLetters(normalize(seenName)).split('');

  return hasSameLettersWithSameOrder(nameLetters, seenNameLetters);
};
