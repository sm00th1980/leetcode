import { isNameCorrect } from '..';

const name = 'Tom Jones';

test('example 1', () => {
  const seenName = 'Tom Jones';

  expect(isNameCorrect(name, seenName)).toBe(true);
});

test('example 2', () => {
  const seenName = 'Tom     Jones';

  expect(isNameCorrect(name, seenName)).toBe(true);
});

test('example 3', () => {
  const seenName = 'Tom     Jones     ';

  expect(isNameCorrect(name, seenName)).toBe(false);
});

test('example 4', () => {
  const seenName = '';

  expect(isNameCorrect(name, seenName)).toBe(true);
});

test('example 5', () => {
  const seenName = ' o ';

  expect(isNameCorrect(name, seenName)).toBe(false);
});

test('example 6', () => {
  const seenName = ' o';

  expect(isNameCorrect(name, seenName)).toBe(true);
});

test('example 7', () => {
  const seenName = 'o ';

  expect(isNameCorrect(name, seenName)).toBe(true);
});

test('example 8', () => {
  const seenName = 'J ';

  expect(isNameCorrect(name, seenName)).toBe(false);
});

test('example 9', () => {
  const seenName = ' J';

  expect(isNameCorrect(name, seenName)).toBe(true);
});

test('example 10', () => {
  const seenName = ' T';

  expect(isNameCorrect(name, seenName)).toBe(false);
});
