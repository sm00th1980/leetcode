import { isNameCorrect } from '..';

const name = 'Tomo';

test('example 1', () => {
  const seenName = 'Tm';

  expect(isNameCorrect(name, seenName)).toBe(true);
});

test('example 2', () => {
  const seenName = 'moo';

  expect(isNameCorrect(name, seenName)).toBe(true);
});
