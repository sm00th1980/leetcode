import { isNameCorrect } from '..';

const name = 'Tomm';

test('example 1', () => {
  const seenName = 'Tom';

  expect(isNameCorrect(name, seenName)).toBe(true);
});
