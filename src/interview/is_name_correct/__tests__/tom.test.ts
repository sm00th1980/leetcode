import { isNameCorrect } from '..';

const name = 'Tom';

test('example 1', () => {
  const seenName = 'Tom';

  expect(isNameCorrect(name, seenName)).toBe(true);
});

test('example 2', () => {
  const seenName = 'Tommmmmmm';

  expect(isNameCorrect(name, seenName)).toBe(true);
});

test('example 3', () => {
  const seenName = 'omt';

  expect(isNameCorrect(name, seenName)).toBe(false);
});

test('example 4', () => {
  const seenName = 'tm';

  expect(isNameCorrect(name, seenName)).toBe(true);
});

test('example 5', () => {
  const seenName = 'Tmmmm';

  expect(isNameCorrect(name, seenName)).toBe(true);
});

test('example 6', () => {
  const seenName = 'Tm';

  expect(isNameCorrect(name, seenName)).toBe(true);
});

test('example 7', () => {
  const seenName = 'moo';

  expect(isNameCorrect(name, seenName)).toBe(false);
});

test('example 8', () => {
  const seenName = '';

  expect(isNameCorrect(name, seenName)).toBe(true);
});
