import { head, last } from '../../../utils';

export const compactMultipleLetters = (value: string): string => {
  const letters = value.split('');
  return letters
    .reduce(
      (acc, letter) => {
        if (last(acc) === letter) {
          return acc;
        }

        return [...acc, letter];
      },
      [head(letters)],
    )
    .join('');
};
