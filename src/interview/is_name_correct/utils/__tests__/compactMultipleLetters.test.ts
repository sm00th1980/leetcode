import { compactMultipleLetters } from '..';

test('example 1', () => {
  const value = 'Tommm';
  const result = 'Tom';

  expect(compactMultipleLetters(value)).toEqual(result);
});

test('example 2', () => {
  const value = 'Tom     Jones     ';
  const result = 'Tom Jones ';

  expect(compactMultipleLetters(value)).toEqual(result);
});
