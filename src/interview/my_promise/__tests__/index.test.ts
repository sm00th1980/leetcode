import MyPromise from '..';

describe('My Promise', () => {
  test('with sync result', async () => {
    expect.assertions(1);
    const value = 123;
    const promise = new MyPromise((resolve) => {
      resolve(value);
    });

    const result = await promise;
    expect(result).toEqual(value);
  });

  test('with async result', async () => {
    expect.assertions(1);
    const value = 123;
    const promise = new MyPromise((resolve) => {
      setTimeout(() => {
        resolve(value);
      }, 100);
    });

    const result = await promise;
    expect(result).toEqual(value);
  });
});
