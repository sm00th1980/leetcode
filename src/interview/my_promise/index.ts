type Result = number;
type SuccessCallback = (value: Result) => void;
type Executor = (successCallback: SuccessCallback) => void;
class MyPromise {
  private result: Result | undefined = undefined;
  private resolved = false;
  private successCallback: SuccessCallback | undefined = undefined;

  constructor(executor: Executor) {
    executor((value: Result) => {
      if (this.successCallback) {
        //async
        this.successCallback(value);
      } else {
        //sync
        this.result = value;
        this.resolved = true;
      }
    });
  }

  then(successCallback: SuccessCallback) {
    if (this.resolved) {
      // already resolved, just call success callback
      successCallback(this.result!);
    } else {
      // not resolved yet, save success callback for later use
      this.successCallback = successCallback;
    }

    return this;
  }
}

export default MyPromise;
