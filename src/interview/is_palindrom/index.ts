const isPalindrom = (value: string): boolean => {
  const initialValue: string[] = [];
  const reversed = value.split('').reduce((acc, letter) => {
    acc.unshift(letter);
    return acc;
  }, initialValue);
  return value === reversed.join('');
};

export default isPalindrom;
