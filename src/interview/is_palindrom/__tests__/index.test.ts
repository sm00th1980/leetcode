import isPalindrom from '..';

test('example 1', () => {
  const input = 'abcddcba';
  const output = true;

  expect(isPalindrom(input)).toBe(output);
});

test('example 2', () => {
  const input = 'abcd';
  const output = false;

  expect(isPalindrom(input)).toBe(output);
});

test('example 3', () => {
  const input = '';
  const output = true;

  expect(isPalindrom(input)).toBe(output);
});
