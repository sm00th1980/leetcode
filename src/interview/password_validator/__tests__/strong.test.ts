import { isPasswordValid } from '..';

const SCHEME = 'strong';
test('example 1 - apwd', () => {
  const username = 'apwd';
  const output = false;

  expect(isPasswordValid(SCHEME, username)).toBe(output);
});

test('example 2 - abcdedge', () => {
  const username = 'abcdedge';
  const output = true;

  expect(isPasswordValid(SCHEME, username)).toBe(output);
});

test('example 3 - abcdedgefg', () => {
  const username = 'abcdedgefg';
  const output = true;

  expect(isPasswordValid(SCHEME, username)).toBe(output);
});
