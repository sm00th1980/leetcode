import { isPasswordValid } from '..';

const SCHEME = 'weak';
test('example 1 - apwd', () => {
  const username = 'apwd';
  const output = false;

  expect(isPasswordValid(SCHEME, username)).toBe(output);
});

test('example 2  - alongerpwd', () => {
  const username = 'alongerpwd';
  const output = false;

  expect(isPasswordValid(SCHEME, username)).toBe(output);
});

test('example 3 - alongerpwd2', () => {
  const username = 'alongerpwd2';
  const output = false;

  expect(isPasswordValid(SCHEME, username)).toBe(output);
});

test('example 4 - empty', () => {
  const username = '';
  const output = false;

  expect(isPasswordValid(SCHEME, username)).toBe(output);
});

test('example 5 - undefined', () => {
  const username = undefined;
  const output = false;

  expect(isPasswordValid(SCHEME, username)).toBe(output);
});

test('example 6  - too long password', () => {
  const username = 'alongerpwdnvjkfdnvjkfdnjkvfndjkvnjkfdnjvkfdnjkvnfjkdnvjfdnvkfdnj';
  const output = false;

  expect(isPasswordValid(SCHEME, username)).toBe(output);
});

test('example 7  - Alongerpwd2', () => {
  const username = 'Alongerpwd2';
  const output = true;

  expect(isPasswordValid(SCHEME, username)).toBe(output);
});

test('example 8  - Alongerpwd2!', () => {
  const username = 'Alongerpwd2!';
  const output = false;

  expect(isPasswordValid(SCHEME, username)).toBe(output);
});
