const REJECTED_SYMBOLS = ['!', '@', '#', '^', '&', '-'];

const isNumber = (letter: string): boolean => {
  return ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'].includes(letter);
};

const isMoreThan8Letters = (password: string): boolean => {
  return password.length >= 8;
};

const MAX_LENGTH_OF_PASSWORD = 31;

type SCHEME = 'weak' | 'strong';

const isWeakPasswordValid = (password: string): boolean => {
  const isLengthEnough = isMoreThan8Letters(password) && password.length <= MAX_LENGTH_OF_PASSWORD;

  const isAtLeastOneNumber = password.split('').reduce((acc, letter) => {
    if (acc) {
      return true;
    }

    return isNumber(letter);
  }, false);

  const isAtLeastOneLetterIsCapitalized = password.split('').reduce((acc, letter) => {
    if (acc) {
      return true;
    }

    return letter === letter.toUpperCase() && !isNumber(letter);
  }, false);

  const hasRejectedSymbols = password.split('').reduce((acc, letter) => {
    if (acc) {
      return true;
    }

    return REJECTED_SYMBOLS.includes(letter);
  }, false);

  return isLengthEnough && isAtLeastOneNumber && isAtLeastOneLetterIsCapitalized && !hasRejectedSymbols;
};

const isStrongPasswordValid = (password: string): boolean => {
  return isMoreThan8Letters(password);
};

export const isPasswordValid = (scheme: SCHEME, password: string | undefined): boolean => {
  if (password) {
    return scheme === 'weak' ? isWeakPasswordValid(password) : isStrongPasswordValid(password);
  }

  return false;
};
