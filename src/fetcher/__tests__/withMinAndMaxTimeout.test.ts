import { compose } from 'ramda';

import { withMaxTimeout, TIMEOUT_ERROR } from '../withMaxTimeout';
import { withMinTimeout } from '../withMinTimeout';

const resolveAfter = <T>(timeout: number, value: T) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(value);
    }, timeout);
  });
};

const VALUE = 'value';

describe('run without arguments', () => {
  test('fire resolve between min and max timeouts', async () => {
    expect.assertions(4);

    //setup
    const fetcher = jest.fn(() => resolveAfter(15, VALUE));

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    //run
    const withMinAndMaxTimeouts = compose(withMinTimeout(10), withMaxTimeout(20));
    await withMinAndMaxTimeouts(fetcher)().then(onSuccess).catch(onFailure);

    //verify
    expect(onSuccess).toHaveBeenCalledWith(VALUE);
    expect(onFailure).not.toHaveBeenCalled();
    expect(fetcher).toHaveBeenCalled();
    expect(fetcher).toHaveBeenCalledTimes(1);
  });

  test('fire rejection if max timeouts', async () => {
    expect.assertions(4);

    //setup
    const fetcher = jest.fn(() => resolveAfter(30, VALUE));

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    //run
    const withMinAndMaxTimeouts = compose(withMinTimeout(10), withMaxTimeout(20));
    await withMinAndMaxTimeouts(fetcher)().then(onSuccess).catch(onFailure);

    //verify
    expect(onSuccess).not.toHaveBeenCalled();
    expect(onFailure).toHaveBeenCalledWith(TIMEOUT_ERROR);
    expect(fetcher).toHaveBeenCalled();
    expect(fetcher).toHaveBeenCalledTimes(1);
  });
});

describe('run with 3 arguments', () => {
  const arg1 = 1;
  const arg2 = 2;
  const arg3 = 3;

  test('fire resolve between min and max timeouts', async () => {
    expect.assertions(4);

    //setup
    const fetcher = jest.fn(() => resolveAfter(15, VALUE));

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    //run
    const withMinAndMaxTimeouts = compose(withMinTimeout(10), withMaxTimeout(20));
    await withMinAndMaxTimeouts(fetcher)(arg1, arg2, arg3).then(onSuccess).catch(onFailure);

    //verify
    expect(onSuccess).toHaveBeenCalledWith(VALUE);
    expect(onFailure).not.toHaveBeenCalled();
    expect(fetcher).toHaveBeenCalledWith(arg1, arg2, arg3);
    expect(fetcher).toHaveBeenCalledTimes(1);
  });

  test('fire rejection if max timeouts', async () => {
    expect.assertions(4);

    //setup
    const fetcher = jest.fn(() => resolveAfter(30, VALUE));

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    //run
    const withMinAndMaxTimeouts = compose(withMinTimeout(10), withMaxTimeout(20));
    await withMinAndMaxTimeouts(fetcher)(arg1, arg2, arg3).then(onSuccess).catch(onFailure);

    //verify
    expect(onSuccess).not.toHaveBeenCalled();
    expect(onFailure).toHaveBeenCalledWith(TIMEOUT_ERROR);
    expect(fetcher).toHaveBeenCalledWith(arg1, arg2, arg3);
    expect(fetcher).toHaveBeenCalledTimes(1);
  });
});
