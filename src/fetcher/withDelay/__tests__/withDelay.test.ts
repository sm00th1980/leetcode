import { withDelay } from '..';

const VALUE = 'value';

describe('run without arguments', () => {
  test('start run after some delay', async () => {
    expect.assertions(4);

    //setup
    const fetcher = jest.fn().mockResolvedValueOnce(VALUE);

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    //run
    await withDelay(10)(fetcher)().then(onSuccess).catch(onFailure);

    //verify
    expect(onSuccess).toHaveBeenCalledWith(VALUE);
    expect(onFailure).not.toHaveBeenCalled();
    expect(fetcher).toHaveBeenCalled();
    expect(fetcher).toHaveBeenCalledTimes(1);
  });

  test('start run after some delay', async () => {
    expect.assertions(4);

    //setup
    const error = Error('oops');
    const fetcher = jest.fn().mockRejectedValueOnce(error);

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    //run
    await withDelay(10)(fetcher)().then(onSuccess).catch(onFailure);

    //verify
    expect(onSuccess).not.toHaveBeenCalled();
    expect(onFailure).toHaveBeenCalledWith(error);
    expect(fetcher).toHaveBeenCalled();
    expect(fetcher).toHaveBeenCalledTimes(1);
  });
});

describe('run with 2 arguments', () => {
  const arg1 = 1;
  const arg2 = 2;

  test('start run after some delay', async () => {
    expect.assertions(4);

    //setup
    const fetcher = jest.fn().mockResolvedValueOnce(VALUE);

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    //run
    await withDelay(10)(fetcher)(arg1, arg2).then(onSuccess).catch(onFailure);

    //verify
    expect(onSuccess).toHaveBeenCalledWith(VALUE);
    expect(onFailure).not.toHaveBeenCalled();
    expect(fetcher).toHaveBeenCalledWith(arg1, arg2);
    expect(fetcher).toHaveBeenCalledTimes(1);
  });

  test('start run after some delay', async () => {
    expect.assertions(4);

    //setup
    const error = Error('oops');
    const fetcher = jest.fn().mockRejectedValueOnce(error);

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    //run
    await withDelay(10)(fetcher)(arg1, arg2).then(onSuccess).catch(onFailure);

    //verify
    expect(onSuccess).not.toHaveBeenCalled();
    expect(onFailure).toHaveBeenCalledWith(error);
    expect(fetcher).toHaveBeenCalledWith(arg1, arg2);
    expect(fetcher).toHaveBeenCalledTimes(1);
  });
});
