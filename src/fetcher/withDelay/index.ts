type Fetcher<T> = (...args: unknown[]) => Promise<T>;
type Milliseconds = number;

export const withDelay = <T>(delay: Milliseconds) => (fetcher: Fetcher<T>) => {
  return (...args: unknown[]) => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        fetcher(...args)
          .then(resolve)
          .catch(reject);
      }, delay);
    });
  };
};
