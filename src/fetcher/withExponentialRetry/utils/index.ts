export const delay = (attempt: number): number => {
  if (attempt <= 0) {
    return 0;
  }

  return Math.pow(2, attempt) / 2;
};
