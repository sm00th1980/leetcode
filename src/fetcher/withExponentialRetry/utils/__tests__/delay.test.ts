import { delay } from '..';

test('calc delay', async () => {
  //setup
  const attempt = 0;

  //run
  const result = delay(attempt);

  //verify
  expect(result).toBe(0);
});

test('calc delay', async () => {
  //setup
  const attempt = 1;

  //run
  const result = delay(attempt);

  //verify
  expect(result).toBe(1);
});

test('calc delay', async () => {
  //setup
  const attempt = 2;

  //run
  const result = delay(attempt);

  //verify
  expect(result).toBe(2);
});

test('calc delay', async () => {
  //setup
  const attempt = 3;

  //run
  const result = delay(attempt);

  //verify
  expect(result).toBe(4);
});

test('calc delay', async () => {
  //setup
  const attempt = 4;

  //run
  const result = delay(attempt);

  //verify
  expect(result).toBe(8);
});

test('calc delay', async () => {
  //setup
  const attempt = 5;

  //run
  const result = delay(attempt);

  //verify
  expect(result).toBe(16);
});
