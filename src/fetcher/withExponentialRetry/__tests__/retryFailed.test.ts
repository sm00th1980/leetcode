import { withExponentialRetry } from '..';

describe('run without arguments', () => {
  test('fire rejection when retry too much', async () => {
    expect.assertions(4);

    //setup
    const error1 = Error('oops 1');
    const error2 = Error('oops 2');
    const fetcher = jest.fn().mockRejectedValueOnce(error1).mockRejectedValueOnce(error2);
    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    //run
    await withExponentialRetry(1, 0)(fetcher)().then(onSuccess).catch(onFailure);

    //verify
    expect(onSuccess).not.toHaveBeenCalled();
    expect(onFailure).toHaveBeenCalledWith(error1);
    expect(fetcher).toHaveBeenCalled();
    expect(fetcher).toHaveBeenCalledTimes(1);
  });
});

describe('run with 2 arguments', () => {
  const arg1 = 1;
  const arg2 = 2;

  test('fire rejection when retry too much', async () => {
    expect.assertions(4);

    //setup
    const error1 = Error('oops 1');
    const error2 = Error('oops 2');
    const fetcher = jest.fn().mockRejectedValueOnce(error1).mockRejectedValueOnce(error2);
    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    //run
    await withExponentialRetry(1, 0)(fetcher)(arg1, arg2).then(onSuccess).catch(onFailure);

    //verify
    expect(onSuccess).not.toHaveBeenCalled();
    expect(onFailure).toHaveBeenCalledWith(error1);
    expect(fetcher).toHaveBeenCalledWith(arg1, arg2);
    expect(fetcher).toHaveBeenCalledTimes(1);
  });
});
