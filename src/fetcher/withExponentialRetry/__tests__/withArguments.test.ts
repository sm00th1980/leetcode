import { withExponentialRetry } from '..';

const VALUE = 'value';

describe('run with 2 arguments', () => {
  const arg1 = 1;
  const arg2 = 2;

  test('fire resolve after 1 retry', async () => {
    expect.assertions(4);

    //setup
    const error = Error('oops');
    const fetcher = jest.fn().mockRejectedValueOnce(error).mockResolvedValueOnce(VALUE);

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    //run
    await withExponentialRetry(1, 10)(fetcher)(arg1, arg2).then(onSuccess).catch(onFailure);

    //verify
    expect(onSuccess).toHaveBeenCalledWith(VALUE);
    expect(onFailure).not.toHaveBeenCalled();
    expect(fetcher).toHaveBeenCalledWith(arg1, arg2);
    expect(fetcher).toHaveBeenCalledTimes(2);
  });

  test('fire resolve after 2 retry', async () => {
    expect.assertions(4);

    //setup
    const error = Error('oops');
    const fetcher = jest.fn().mockRejectedValueOnce(error).mockRejectedValueOnce(error).mockResolvedValueOnce(VALUE);

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    //run
    await withExponentialRetry(1, 10)(fetcher)(arg1, arg2).then(onSuccess).catch(onFailure);

    //verify
    expect(onSuccess).toHaveBeenCalledWith(VALUE);
    expect(onFailure).not.toHaveBeenCalled();
    expect(fetcher).toHaveBeenCalledWith(arg1, arg2);
    expect(fetcher).toHaveBeenCalledTimes(3);
  });

  test('fire resolve after 3 retry', async () => {
    expect.assertions(4);

    //setup
    const error = Error('oops');
    const fetcher = jest
      .fn()
      .mockRejectedValueOnce(error)
      .mockRejectedValueOnce(error)
      .mockRejectedValueOnce(error)
      .mockResolvedValueOnce(VALUE);

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    //run
    await withExponentialRetry(1, 10)(fetcher)(arg1, arg2).then(onSuccess).catch(onFailure);

    //verify
    expect(onSuccess).toHaveBeenCalledWith(VALUE);
    expect(onFailure).not.toHaveBeenCalled();
    expect(fetcher).toHaveBeenCalledWith(arg1, arg2);
    expect(fetcher).toHaveBeenCalledTimes(4);
  });
});
