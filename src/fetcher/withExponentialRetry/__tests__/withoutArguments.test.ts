import { withExponentialRetry } from '..';

const VALUE = 'value';

describe('run without arguments', () => {
  test('fire rejection when retry too much', async () => {
    expect.assertions(4);

    //setup
    const error1 = Error('oops 1');
    const error2 = Error('oops 2');
    const fetcher = jest.fn().mockRejectedValueOnce(error1).mockRejectedValueOnce(error2);
    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    //run
    await withExponentialRetry(1, 1)(fetcher)().then(onSuccess).catch(onFailure);

    //verify
    expect(onSuccess).not.toHaveBeenCalled();
    expect(onFailure).toHaveBeenCalledWith(error2);
    expect(fetcher).toHaveBeenCalled();
    expect(fetcher).toHaveBeenCalledTimes(2);
  });
});

describe('run without arguments', () => {
  test('fire resolve after 1 retry', async () => {
    expect.assertions(4);

    //setup
    const error = Error('oops');
    const fetcher = jest.fn().mockRejectedValueOnce(error).mockResolvedValueOnce(VALUE);
    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    //run
    await withExponentialRetry(1, 10)(fetcher)().then(onSuccess).catch(onFailure);

    //verify
    expect(onSuccess).toHaveBeenCalledWith(VALUE);
    expect(onFailure).not.toHaveBeenCalled();
    expect(fetcher).toHaveBeenCalled();
    expect(fetcher).toHaveBeenCalledTimes(2);
  });

  test('fire resolve after 2 retry', async () => {
    expect.assertions(4);

    //setup
    const error = Error('oops');
    const fetcher = jest.fn().mockRejectedValueOnce(error).mockRejectedValueOnce(error).mockResolvedValueOnce(VALUE);
    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    //run
    await withExponentialRetry(1, 10)(fetcher)().then(onSuccess).catch(onFailure);

    //verify
    expect(onSuccess).toHaveBeenCalledWith(VALUE);
    expect(onFailure).not.toHaveBeenCalled();
    expect(fetcher).toHaveBeenCalled();
    expect(fetcher).toHaveBeenCalledTimes(3);
  });

  test('fire resolve after 3 retry', async () => {
    expect.assertions(4);

    //setup
    const error = Error('oops');
    const fetcher = jest
      .fn()
      .mockRejectedValueOnce(error)
      .mockRejectedValueOnce(error)
      .mockRejectedValueOnce(error)
      .mockResolvedValueOnce(VALUE);
    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    //run
    await withExponentialRetry(1, 10)(fetcher)().then(onSuccess).catch(onFailure);

    //verify
    expect(onSuccess).toHaveBeenCalledWith(VALUE);
    expect(onFailure).not.toHaveBeenCalled();
    expect(fetcher).toHaveBeenCalled();
    expect(fetcher).toHaveBeenCalledTimes(4);
  });
});
