import { withExponentialRetry } from '..';

const VALUE = 'value';

describe('run without arguments', () => {
  test('fire resolve', async () => {
    expect.assertions(4);

    //setup
    const error1 = Error('oops 1');
    const error2 = Error('oops 2');

    const fetcher = jest.fn().mockRejectedValueOnce(error1).mockRejectedValueOnce(error2).mockResolvedValueOnce(VALUE);
    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    //run
    await withExponentialRetry(1)(fetcher)().then(onSuccess).catch(onFailure);

    //verify
    expect(onSuccess).toHaveBeenCalledWith(VALUE);
    expect(onFailure).not.toHaveBeenCalled();
    expect(fetcher).toHaveBeenCalled();
    expect(fetcher).toHaveBeenCalledTimes(3);
  });
});

describe('run with 2 arguments', () => {
  const arg1 = 1;
  const arg2 = 2;

  test('fire resolve', async () => {
    expect.assertions(4);

    //setup
    const error1 = Error('oops 1');
    const error2 = Error('oops 2');
    const fetcher = jest.fn().mockRejectedValueOnce(error1).mockRejectedValueOnce(error2).mockResolvedValueOnce(VALUE);
    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    //run
    await withExponentialRetry(1)(fetcher)(arg1, arg2).then(onSuccess).catch(onFailure);

    //verify
    expect(onSuccess).toHaveBeenCalledWith(VALUE);
    expect(onFailure).not.toHaveBeenCalled();
    expect(fetcher).toHaveBeenCalledWith(arg1, arg2);
    expect(fetcher).toHaveBeenCalledTimes(3);
  });
});
