import { withDelay } from '../withDelay';
import { delay } from './utils';

type Fetcher<T> = (...args: unknown[]) => Promise<T>;
const DELAY_MULTIPLICATOR = 1000; //1 sec

export const withExponentialRetry = <T>(
  delayMultiplicator: number = DELAY_MULTIPLICATOR,
  retryCount = Infinity,
  attempt = 0,
) => (fetcher: Fetcher<T>) => {
  return (...args: unknown[]): Promise<unknown> => {
    const delayInMs = delay(attempt) * delayMultiplicator;
    return withDelay(delayInMs)(fetcher)(...args).catch((error) => {
      if (retryCount <= 0) {
        throw error;
      }

      return withExponentialRetry(delayMultiplicator, retryCount - 1, attempt + 1)(fetcher)(...args);
    });
  };
};
