import { withMinTimeout } from '..';

const resolveAfter = <T>(timeout: number, value: T) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(value);
    }, timeout);
  });
};

const VALUE = 'value';

describe('run without arguments', () => {
  test('fire resolve after min timeout reached', async () => {
    expect.assertions(3);

    //setup
    const fetcher = jest.fn(() => resolveAfter(20, VALUE));

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    //run
    await withMinTimeout(10)(fetcher)().then(onSuccess).catch(onFailure);

    //verify
    expect(onSuccess).toHaveBeenCalledWith(VALUE);
    expect(onFailure).not.toHaveBeenCalled();
    expect(fetcher).toHaveBeenCalled();
  });

  test('fire resolve after min timeout reached', async () => {
    expect.assertions(3);

    //setup
    const fetcher = jest.fn(() => resolveAfter(10, VALUE));

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    //run
    await withMinTimeout(20)(fetcher)().then(onSuccess).catch(onFailure);

    //verify
    expect(onSuccess).toHaveBeenCalledWith(VALUE);
    expect(onFailure).not.toHaveBeenCalled();
    expect(fetcher).toHaveBeenCalled();
  });

  test('fire resolve after min timeout reached', async () => {
    expect.assertions(3);

    //setup
    const fetcher = jest.fn(() => resolveAfter(0, VALUE));

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    //run
    await withMinTimeout(20)(fetcher)().then(onSuccess).catch(onFailure);

    //verify
    expect(onSuccess).toHaveBeenCalledWith(VALUE);
    expect(onFailure).not.toHaveBeenCalled();
    expect(fetcher).toHaveBeenCalled();
  });
});

describe('run with 3 arguments', () => {
  const arg1 = 1;
  const arg2 = 2;
  const arg3 = 3;

  test('fire resolve after min timeout reached', async () => {
    expect.assertions(3);

    //setup
    const fetcher = jest.fn(() => resolveAfter(20, VALUE));

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    //run
    await withMinTimeout(10)(fetcher)(arg1, arg2, arg3).then(onSuccess).catch(onFailure);

    //verify
    expect(onSuccess).toHaveBeenCalledWith(VALUE);
    expect(onFailure).not.toHaveBeenCalled();
    expect(fetcher).toHaveBeenCalledWith(arg1, arg2, arg3);
  });

  test('fire resolve after min timeout reached', async () => {
    expect.assertions(3);

    //setup
    const fetcher = jest.fn(() => resolveAfter(10, VALUE));

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    //run
    await withMinTimeout(20)(fetcher)(arg1, arg2, arg3).then(onSuccess).catch(onFailure);

    //verify
    expect(onSuccess).toHaveBeenCalledWith(VALUE);
    expect(onFailure).not.toHaveBeenCalled();
    expect(fetcher).toHaveBeenCalledWith(arg1, arg2, arg3);
  });

  test('fire resolve after min timeout reached', async () => {
    expect.assertions(3);

    //setup
    const fetcher = jest.fn(() => resolveAfter(0, VALUE));

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    //run
    await withMinTimeout(20)(fetcher)(arg1, arg2, arg3).then(onSuccess).catch(onFailure);

    //verify
    expect(onSuccess).toHaveBeenCalledWith(VALUE);
    expect(onFailure).not.toHaveBeenCalled();
    expect(fetcher).toHaveBeenCalledWith(arg1, arg2, arg3);
  });
});
