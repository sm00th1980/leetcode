type Fetcher<T> = (...args: unknown[]) => Promise<T>;

export const withMinTimeout = <T>(minTimeout: number) => (fetcher: Fetcher<T>) => {
  const timer = () => {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(undefined);
      }, minTimeout);
    });
  };

  return (...args: unknown[]) => Promise.all([fetcher(...args), timer()]).then(([result]) => result);
};
