import { withMaxTimeout, TIMEOUT_ERROR } from '..';

const resolveAfter = <T>(timeout: number, value: T) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(value);
    }, timeout);
  });
};

describe('run without arguments', () => {
  test('fire rejection after timeout reached', async () => {
    expect.assertions(3);

    //setup
    const fetcher = jest.fn(() => resolveAfter(20, 'value'));

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    //run
    await withMaxTimeout(10)(fetcher)().then(onSuccess).catch(onFailure);

    //verify
    expect(onSuccess).not.toHaveBeenCalled();
    expect(onFailure).toHaveBeenCalledWith(TIMEOUT_ERROR);
    expect(fetcher).toHaveBeenCalled();
  });

  test('fire resolve before timeout reached', async () => {
    expect.assertions(3);

    //setup
    const fetcher = jest.fn(() => resolveAfter(10, 'value'));

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    //run
    await withMaxTimeout(20)(fetcher)().then(onSuccess).catch(onFailure);

    //verify
    expect(onSuccess).toHaveBeenCalledWith('value');
    expect(onFailure).not.toHaveBeenCalled();
    expect(fetcher).toHaveBeenCalled();
  });

  test('fire rejection when timeout reached', async () => {
    expect.assertions(3);

    const fetcher = jest.fn(() => resolveAfter(10, 'value'));

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    await withMaxTimeout(10)(fetcher)().then(onSuccess).catch(onFailure);

    expect(onSuccess).not.toHaveBeenCalled();
    expect(onFailure).toHaveBeenCalledWith(TIMEOUT_ERROR);
    expect(fetcher).toHaveBeenCalled();
  });
});

describe('run with 3 arguments', () => {
  const arg1 = 1;
  const arg2 = 2;
  const arg3 = 3;

  test('fire rejection after timeout reached', async () => {
    expect.assertions(3);

    //setup
    const fetcher = jest.fn(() => resolveAfter(20, 'value'));

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    //run
    await withMaxTimeout(10)(fetcher)(arg1, arg2, arg3).then(onSuccess).catch(onFailure);

    //verify
    expect(onSuccess).not.toHaveBeenCalled();
    expect(onFailure).toHaveBeenCalledWith(TIMEOUT_ERROR);
    expect(fetcher).toHaveBeenCalled();
  });

  test('fire resolve before timeout reached', async () => {
    expect.assertions(3);

    //setup
    const fetcher = jest.fn(() => resolveAfter(10, 'value'));

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    //run
    await withMaxTimeout(20)(fetcher)(arg1, arg2, arg3).then(onSuccess).catch(onFailure);

    //verify
    expect(onSuccess).toHaveBeenCalledWith('value');
    expect(onFailure).not.toHaveBeenCalled();
    expect(fetcher).toHaveBeenCalledWith(arg1, arg2, arg3);
  });

  test('fire rejection when timeout reached', async () => {
    expect.assertions(3);

    const fetcher = jest.fn(() => resolveAfter(10, 'value'));

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    await withMaxTimeout(10)(fetcher)(arg1, arg2, arg3).then(onSuccess).catch(onFailure);

    expect(onSuccess).not.toHaveBeenCalled();
    expect(onFailure).toHaveBeenCalledWith(TIMEOUT_ERROR);
    expect(fetcher).toHaveBeenCalled();
  });
});
