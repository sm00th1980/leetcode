type Fetcher<T> = (...args: unknown[]) => Promise<T>;

export const TIMEOUT_ERROR = Error('timeout reached');

export const withMaxTimeout = <T>(maxTimeout: number) => (fetcher: Fetcher<T>) => {
  const timer = () => {
    return new Promise((_, reject) => {
      setTimeout(() => {
        reject(TIMEOUT_ERROR);
      }, maxTimeout);
    });
  };

  return (...args: unknown[]) => Promise.race([timer(), fetcher(...args)]);
};
