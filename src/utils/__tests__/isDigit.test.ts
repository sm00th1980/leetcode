import { isDigit } from '..';

test('example 1', () => {
  const input = '5';
  const output = true;

  expect(isDigit(input)).toBe(output);
});

test('example 2', () => {
  const input = ',';
  const output = false;

  expect(isDigit(input)).toBe(output);
});
