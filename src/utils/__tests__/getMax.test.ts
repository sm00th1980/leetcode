import { getMax } from '..';

test('example 1', () => {
  const xs = [1, 2, 3];

  expect(getMax(xs)).toBe(3);
});

test('example 2', () => {
  const xs = [1, 2, 3, 6, 4, 5];

  expect(getMax(xs)).toBe(6);
});

test('example 3', () => {
  const xs = [-1, -2, -3, -6, -4, -5];

  expect(getMax(xs)).toBe(-1);
});

test('example 4', () => {
  const xs = [-2, -3, -6, -4, -1, -5];

  expect(getMax(xs)).toBe(-1);
});

test('example 5', () => {
  const xs: Array<number> = [];

  expect(getMax(xs)).toBeUndefined();
});

test('example 6', () => {
  const xs = new Set([1, 2, 3, 6, 4, 5, 1, 1, 1]);

  expect(getMax(xs)).toBe(6);
});
