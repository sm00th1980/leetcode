import { findIndex } from '..';

test('example 1', () => {
  const predicate = (value: string) => value === '2';
  const xs = ['1', '2', '2', '3', '4'];

  expect(findIndex(predicate, xs)).toBe(1);
});

test('example 2', () => {
  const predicate = (value: string) => value === '2';
  const xs: string[] = [];

  expect(findIndex(predicate, xs)).toBe(-1);
});

test('example 3', () => {
  const predicate = () => false;
  const xs = ['1', '2', '2', '3'];

  expect(findIndex(predicate, xs)).toBe(-1);
});

test('example 4', () => {
  const predicate = (value: string) => value === '2';
  const xs = ['1', '2', '2', '3'];

  expect(findIndex(predicate, xs)).toBe(1);
});

test('example 5', () => {
  const predicate = (value: string) => value === '2';
  const xs = ['1', '2.1', '2', '3', '2', '4'];

  expect(findIndex(predicate, xs)).toBe(2);
});
