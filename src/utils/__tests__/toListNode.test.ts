import { toListNode, listNode } from '../structures';

describe('toListNode', () => {
  test('example 1', () => {
    const input = [1];
    const output = listNode(1);

    expect(toListNode(input)).toEqual(output);
  });

  test('example 2', () => {
    const input = [1, 2];
    const output = listNode(1, listNode(2));

    expect(toListNode(input)).toEqual(output);
  });

  test('example 3', () => {
    const input = [1, 2, 3];
    const output = listNode(1, listNode(2, listNode(3)));

    expect(toListNode(input)).toEqual(output);
  });

  test('example 4', () => {
    const input = [1, 2, 3, 4];
    const output = listNode(1, listNode(2, listNode(3, listNode(4))));

    expect(toListNode(input)).toEqual(output);
  });
});
