/* eslint @typescript-eslint/no-empty-function: off */

import { isObject } from '..';

test('example 1', () => {
  const input = null;

  expect(isObject(input)).toBe(false);
});

test('example 2', () => {
  const input = undefined;

  expect(isObject(input)).toBe(false);
});

test('example 3', () => {
  const input = NaN;

  expect(isObject(input)).toBe(false);
});

test('example 4', () => {
  const input = 1;

  expect(isObject(input)).toBe(false);
});

test('example 5', () => {
  const input = 0;

  expect(isObject(input)).toBe(false);
});

test('example 6', () => {
  const input = {};

  expect(isObject(input)).toBe(true);
});

test('example 7', () => {
  const input: number[] = [];

  expect(isObject(input)).toBe(false);
});

test('example 8', () => {
  const input = () => {};

  expect(isObject(input)).toBe(false);
});

test('example 9', () => {
  const input = 1.2;

  expect(isObject(input)).toBe(false);
});
