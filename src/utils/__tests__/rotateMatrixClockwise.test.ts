import { rotateMatrixClockwise } from '..';

test('example 1', () => {
  const input = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
  ];

  const output = [
    [7, 4, 1],
    [8, 5, 2],
    [9, 6, 3],
  ];

  expect(rotateMatrixClockwise(input)).toEqual(output);
});

test('example 2', () => {
  const input = [
    [1, 1, 1, 1],
    [1, 1, 1, 1],
    [1, 1, 1, 1],
    [1, 1, 1, 1],
  ];

  const output = [
    [1, 1, 1, 1],
    [1, 1, 1, 1],
    [1, 1, 1, 1],
    [1, 1, 1, 1],
  ];

  expect(rotateMatrixClockwise(input)).toEqual(output);
});

test('example 3', () => {
  const input = [[5]];
  const output = [[5]];

  expect(rotateMatrixClockwise(input)).toEqual(output);
});

test('example 4', () => {
  const input = [
    [7, 3, 1, 9],
    [3, 4, 6, 9],
    [6, 9, 6, 6],
    [9, 5, 8, 5],
  ];
  const output = [
    [9, 6, 3, 7],
    [5, 9, 4, 3],
    [8, 6, 6, 1],
    [5, 6, 9, 9],
  ];

  expect(rotateMatrixClockwise(input)).toEqual(output);
});
