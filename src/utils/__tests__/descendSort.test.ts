import { descendSort } from '..';

describe('descendSort for numbers', () => {
  test('example 1', () => {
    const xs = [1, 2, 3];

    expect(descendSort<number>(xs)).toEqual([3, 2, 1]);
  });

  test('example 2', () => {
    const xs = [3, 2, 1];

    expect(descendSort<number>(xs)).toEqual([3, 2, 1]);
  });

  test('example 3', () => {
    const xs: number[] = [];

    expect(descendSort<number>(xs)).toEqual([]);
  });
});

describe('descendSort for string', () => {
  test('example 1', () => {
    const xs = ['a', 'b', 'c'];

    expect(descendSort<string>(xs)).toEqual(['c', 'b', 'a']);
  });

  test('example 2', () => {
    const xs = ['c', 'b', 'a'];

    expect(descendSort<string>(xs)).toEqual(['c', 'b', 'a']);
  });

  test('example 3', () => {
    const xs: string[] = [];

    expect(descendSort<string>(xs)).toEqual([]);
  });
});
