/* eslint @typescript-eslint/no-empty-function: off */

import { isArray } from '..';

test('example 1', () => {
  const input = null;

  expect(isArray(input)).toBe(false);
});

test('example 2', () => {
  const input = undefined;

  expect(isArray(input)).toBe(false);
});

test('example 3', () => {
  const input = NaN;

  expect(isArray(input)).toBe(false);
});

test('example 4', () => {
  const input = 1;

  expect(isArray(input)).toBe(false);
});

test('example 5', () => {
  const input = 0;

  expect(isArray(input)).toBe(false);
});

test('example 6', () => {
  const input = {};

  expect(isArray(input)).toBe(false);
});

test('example 7', () => {
  const input: number[] = [];

  expect(isArray(input)).toBe(true);
});

test('example 8', () => {
  const input = () => {};

  expect(isArray(input)).toBe(false);
});

test('example 9', () => {
  const input = 1.2;

  expect(isArray(input)).toBe(false);
});
