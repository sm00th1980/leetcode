import { flatten } from '..';

test('example 1', () => {
  const input = [1, 2, 3];
  const output = [1, 2, 3];

  expect(flatten(input)).toEqual(output);
});

test('example 2', () => {
  const input = [1, 2, [3, 4]];
  const output = [1, 2, 3, 4];

  expect(flatten(input)).toEqual(output);
});

test('example 3', () => {
  const input = [1, 2, [3, 4], 5, [6, [7, 8, [9, [10, 11], 12]]]];
  const output = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

  expect(flatten(input)).toEqual(output);
});
