import { isLetter } from '..';

test('example 1', () => {
  const input = '5';
  const output = false;

  expect(isLetter(input)).toBe(output);
});

test('example 2', () => {
  const input = ',';
  const output = false;

  expect(isLetter(input)).toBe(output);
});

test('example 3', () => {
  const input = 'a';
  const output = true;

  expect(isLetter(input)).toBe(output);
});

test('example 4', () => {
  const input = 'Z';
  const output = true;

  expect(isLetter(input)).toBe(output);
});
