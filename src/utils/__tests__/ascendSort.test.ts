import { ascendSort } from '..';

describe('ascendSort for numbers', () => {
  test('example 1', () => {
    const xs = [1, 2, 3];

    expect(ascendSort<number>(xs)).toEqual([1, 2, 3]);
  });

  test('example 2', () => {
    const xs = [3, 2, 1];

    expect(ascendSort<number>(xs)).toEqual([1, 2, 3]);
  });

  test('example 3', () => {
    const xs: number[] = [];

    expect(ascendSort<number>(xs)).toEqual([]);
  });
});

describe('ascendSort for string', () => {
  test('example 1', () => {
    const xs = ['a', 'b', 'c'];

    expect(ascendSort<string>(xs)).toEqual(['a', 'b', 'c']);
  });

  test('example 2', () => {
    const xs = ['c', 'b', 'a'];

    expect(ascendSort<string>(xs)).toEqual(['a', 'b', 'c']);
  });

  test('example 3', () => {
    const xs: string[] = [];

    expect(ascendSort<string>(xs)).toEqual([]);
  });
});
