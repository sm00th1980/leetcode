import { take } from '..';

test('example 1', () => {
  const count = 1;
  const xs = ['foo', 'bar', 'baz'];

  expect(take(count, xs)).toEqual(['foo']);
});

test('example 2', () => {
  const count = 2;
  const xs = ['foo', 'bar', 'baz'];

  expect(take(count, xs)).toEqual(['foo', 'bar']);
});

test('example 3', () => {
  const count = 3;
  const xs = ['foo', 'bar', 'baz'];

  expect(take(count, xs)).toEqual(['foo', 'bar', 'baz']);
});

test('example 4', () => {
  const count = 4;
  const xs = ['foo', 'bar', 'baz'];

  expect(take(count, xs)).toEqual(['foo', 'bar', 'baz']);
});
