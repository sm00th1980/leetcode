import { zip } from '..';

test('example 1', () => {
  const xs1 = ['1', '2', '3'];
  const xs2 = [1, 2, 3];

  expect(zip(xs1, xs2)).toEqual([
    ['1', 1],
    ['2', 2],
    ['3', 3],
  ]);
});

test('example 2', () => {
  const xs1 = ['1'];
  const xs2 = [1];

  expect(zip(xs1, xs2)).toEqual([['1', 1]]);
});

test('example 3', () => {
  const xs1 = ['1', '2', '3'];
  const xs2 = [1, 2, 3, 4];

  expect(zip(xs1, xs2)).toEqual([
    ['1', 1],
    ['2', 2],
    ['3', 3],
  ]);
});
