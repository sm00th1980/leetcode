import { isLastIndex } from '..';

test('example 1', () => {
  const index = 0;
  const xs = [1, 2, 3];

  expect(isLastIndex(index, xs)).toBe(false);
});

test('example 2', () => {
  const index = 1;
  const xs = [1, 2, 3];

  expect(isLastIndex(index, xs)).toBe(false);
});

test('example 3', () => {
  const index = 2;
  const xs = [1, 2, 3];

  expect(isLastIndex(index, xs)).toBe(true);
});

test('example 4', () => {
  const index = 3;
  const xs = [1, 2, 3];

  expect(isLastIndex(index, xs)).toBe(true);
});

test('example 5', () => {
  const index = -1;
  const xs = [1, 2, 3];

  expect(isLastIndex(index, xs)).toBe(false);
});
