import { prepend } from '..';

test('example 1', () => {
  const element = '1';
  const xs = ['foo', 'bar', 'baz'];

  expect(prepend(element, xs)).toEqual(['1', 'foo', 'bar', 'baz']);
});

test('example 2', () => {
  const element = 1;
  const xs: Array<number> = [];

  expect(prepend(element, xs)).toEqual([1]);
});
