import { sort } from '..';

type Value = {
  value: number;
};

describe('sort ascend', () => {
  const comparator = (a: number, b: number): number => a - b;

  test('example 1', () => {
    const xs = [1, 2, 3];

    expect(sort(comparator, xs)).toEqual([1, 2, 3]);
  });

  test('example 2', () => {
    const xs = [3, 2, 1];

    expect(sort(comparator, xs)).toEqual([1, 2, 3]);
  });

  test('example 3', () => {
    const comparator = (a: Value, b: Value) => a.value - b.value;
    const xs = [{ value: 3 }, { value: 2 }, { value: 1 }];

    expect(sort(comparator, xs)).toEqual([{ value: 1 }, { value: 2 }, { value: 3 }]);
  });

  test('example 4', () => {
    const comparator = (a: Value, b: Value) => a.value - b.value;
    const xs = [{ value: 1 }, { value: 2 }, { value: 3 }];

    expect(sort(comparator, xs)).toEqual([{ value: 1 }, { value: 2 }, { value: 3 }]);
  });
});

describe('sort descend', () => {
  const comparator = (a: number, b: number): number => b - a;

  test('example 1', () => {
    const xs = [1, 2, 3];

    expect(sort(comparator, xs)).toEqual([3, 2, 1]);
  });

  test('example 2', () => {
    const xs = [3, 2, 1];

    expect(sort(comparator, xs)).toEqual([3, 2, 1]);
  });

  test('example 3', () => {
    const comparator = (a: Value, b: Value) => b.value - a.value;
    const xs = [{ value: 1 }, { value: 2 }, { value: 3 }];

    expect(sort(comparator, xs)).toEqual([{ value: 3 }, { value: 2 }, { value: 1 }]);
  });

  test('example 4', () => {
    const comparator = (a: Value, b: Value) => b.value - a.value;
    const xs = [{ value: 3 }, { value: 2 }, { value: 1 }];

    expect(sort(comparator, xs)).toEqual([{ value: 3 }, { value: 2 }, { value: 1 }]);
  });

  test('example 5', () => {
    const comparator = (a: string, b: string): number => a.localeCompare(b);
    const xs = ['c', 'b', 'a'];

    expect(sort(comparator, xs)).toEqual(['a', 'b', 'c']);
  });
});
