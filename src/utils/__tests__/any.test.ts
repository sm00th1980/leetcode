import { any } from '..';

test('example 1', () => {
  const predicate = () => true;
  const xs = [1, 2, 3];

  expect(any(predicate, xs)).toBe(true);
});

test('example 2', () => {
  const predicate = () => false;
  const xs = [1, 2, 3];

  expect(any(predicate, xs)).toBe(false);
});

test('example 3', () => {
  const predicate = (value: number) => value % 2 === 0;
  const xs = [1, 2, 3];

  expect(any(predicate, xs)).toBe(true);
});
