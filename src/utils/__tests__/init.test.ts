import { init } from '..';

test('example 1', () => {
  const xs = ['foo', 'bar', 'baz'];

  expect(init(xs)).toEqual(['foo', 'bar']);
});

test('example 2', () => {
  const xs: string[] = [];

  expect(init(xs)).toEqual([]);
});

test('example 3', () => {
  const xs = [1, 2, 3, 4, 5];

  expect(init(xs)).toEqual([1, 2, 3, 4]);
});

test('example 3', () => {
  const xs = '12345';

  expect(init(xs)).toEqual('1234');
});
