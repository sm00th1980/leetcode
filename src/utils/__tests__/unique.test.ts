import { unique } from '..';

test('example 1', () => {
  const xs = [1, 2, 3];

  expect(unique(xs)).toEqual([1, 2, 3]);
});

test('example 2', () => {
  const xs = [1, 2, 3, 6, 4, 5];

  expect(unique(xs)).toEqual([1, 2, 3, 6, 4, 5]);
});

test('example 3', () => {
  const xs = [1, 2, 3, 6, 4, 5, 1, 1, 1];

  expect(unique(xs)).toEqual([1, 2, 3, 6, 4, 5]);
});

test('example 4', () => {
  const xs = new Set([1, 2, 3, 6, 4, 5, 1, 1, 1]);

  expect(unique(xs)).toEqual([1, 2, 3, 6, 4, 5]);
});
