import { assert } from '..';

test('example 1', () => {
  const t = () => {
    assert(true, 'oops');
  };

  expect(t).toThrow(Error);
  expect(t).toThrow('oops');
});

test('example 2', () => {
  const t = () => {
    assert(false, 'oops');
  };

  expect(t).not.toThrow(Error);
});
