/* eslint-disable @typescript-eslint/no-empty-function */

import { toNumber } from '..';

test('example 1', () => {
  const input = 1;
  const output = 1;

  expect(toNumber(input)).toEqual(output);
});

test('example 2', () => {
  const input = null;

  expect(toNumber(input)).toBeUndefined();
});

test('example 3', () => {
  const input = undefined;

  expect(toNumber(input)).toBeUndefined();
});

test('example 4', () => {
  const input = '1';
  const output = 1;

  expect(toNumber(input)).toEqual(output);
});

test('example 5', () => {
  const input = {};

  expect(toNumber(input)).toBeUndefined();
});

test('example 6', () => {
  const input = [] as unknown;

  expect(toNumber(input)).toBeUndefined();
});

test('example 7', () => {
  const input = [1, 2, 3];

  expect(toNumber(input)).toBeUndefined();
});

test('example 8', () => {
  const input = () => {};

  expect(toNumber(input)).toBeUndefined();
});

test('example 9', () => {
  const input = true;

  expect(toNumber(input)).toBeUndefined();
});

test('example 10', () => {
  const input = false;

  expect(toNumber(input)).toBeUndefined();
});

test('example 11', () => {
  const input = 1.23;
  const output = 1.23;

  expect(toNumber(input)).toEqual(output);
});

test('example 12', () => {
  const input = 0;
  const output = 0;

  expect(toNumber(input)).toEqual(output);
});

test('example 13', () => {
  const input = -1;
  const output = -1;

  expect(toNumber(input)).toEqual(output);
});
