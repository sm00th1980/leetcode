import { isNil } from '..';

test('example 1', () => {
  const input = null;

  expect(isNil(input)).toBe(true);
});

test('example 2', () => {
  const input = undefined;

  expect(isNil(input)).toBe(true);
});

test('example 3', () => {
  const input = NaN;

  expect(isNil(input)).toBe(false);
});

test('example 4', () => {
  const input = 1;

  expect(isNil(input)).toBe(false);
});

test('example 5', () => {
  const input = 0;

  expect(isNil(input)).toBe(false);
});

test('example 6', () => {
  const input = {};

  expect(isNil(input)).toBe(false);
});

test('example 7', () => {
  const input: number[] = [];

  expect(isNil(input)).toBe(false);
});

test('example 8', () => {
  const input = true;

  expect(isNil(input)).toBe(false);
});

test('example 9', () => {
  const input = false;

  expect(isNil(input)).toBe(false);
});

test('example 10', () => {
  const input = Infinity;

  expect(isNil(input)).toBe(false);
});

test('example 11', () => {
  const input = -1;

  expect(isNil(input)).toBe(false);
});

test('example 12', () => {
  const input = '';

  expect(isNil(input)).toBe(false);
});

test('example 13', () => {
  const input = 'vfdl';

  expect(isNil(input)).toBe(false);
});
