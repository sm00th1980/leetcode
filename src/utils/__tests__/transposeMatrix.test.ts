import { transposeMatrix } from '..';

test('example 1', () => {
  const input = [
    ['a', 'b', 'c'],
    ['1', '2', '3'],
  ];

  const output = [
    ['a', '1'],
    ['b', '2'],
    ['c', '3'],
  ];

  expect(transposeMatrix(input)).toEqual(output);
});

test('example 2', () => {
  const input = [['a']];

  const output = [['a']];

  expect(transposeMatrix(input)).toEqual(output);
});

test('example 3', () => {
  const input = [['a', 'b']];

  const output = [['a'], ['b']];

  expect(transposeMatrix(input)).toEqual(output);
});
