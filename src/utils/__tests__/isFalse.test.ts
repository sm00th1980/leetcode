import { isFalse } from '..';

test('example 1', () => {
  const input = null;

  expect(isFalse(input)).toBe(false);
});

test('example 2', () => {
  const input = undefined;

  expect(isFalse(input)).toBe(false);
});

test('example 3', () => {
  const input = NaN;

  expect(isFalse(input)).toBe(false);
});

test('example 4', () => {
  const input = 1;

  expect(isFalse(input)).toBe(false);
});

test('example 5', () => {
  const input = 0;

  expect(isFalse(input)).toBe(false);
});

test('example 6', () => {
  const input = {};

  expect(isFalse(input)).toBe(false);
});

test('example 7', () => {
  const input: number[] = [];

  expect(isFalse(input)).toBe(false);
});

test('example 8', () => {
  const input = true;

  expect(isFalse(input)).toBe(false);
});

test('example 9', () => {
  const input = false;

  expect(isFalse(input)).toBe(true);
});

test('example 10', () => {
  const input = Infinity;

  expect(isFalse(input)).toBe(false);
});

test('example 11', () => {
  const input = -1;

  expect(isFalse(input)).toBe(false);
});

test('example 12', () => {
  const input = '';

  expect(isFalse(input)).toBe(false);
});

test('example 13', () => {
  const input = 'vfdl';

  expect(isFalse(input)).toBe(false);
});
