import { capitalize } from '..';

test('example 1', () => {
  const input = 'hello world';
  const output = 'Hello World';

  expect(capitalize(input)).toEqual(output);
});

test('example 2', () => {
  const input = 'hello';
  const output = 'Hello';

  expect(capitalize(input)).toEqual(output);
});

test('example 3', () => {
  const input = '';
  const output = '';

  expect(capitalize(input)).toEqual(output);
});

test('example 4', () => {
  const input = undefined;

  expect(capitalize(input)).toBeUndefined();
});

test('example 5', () => {
  const input = null;

  expect(capitalize(input)).toBeNull();
});
