import { toPairs } from '..';

describe('toPairs', () => {
  test('example 1', () => {
    const input = { a: 1, b: 2, c: 3 };
    const output = [
      ['a', 1],
      ['b', 2],
      ['c', 3],
    ];

    expect(toPairs(input)).toEqual(output);
  });
});
