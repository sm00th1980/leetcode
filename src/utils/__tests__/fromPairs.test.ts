import { fromPairs } from '..';

type Pair = [string, unknown];

describe('fromPairs', () => {
  test('example 1', () => {
    const input: Pair[] = [
      ['a', 1],
      ['b', 2],
      ['c', 3],
    ];

    const output = { a: 1, b: 2, c: 3 };

    expect(fromPairs(input)).toEqual(output);
  });
});
