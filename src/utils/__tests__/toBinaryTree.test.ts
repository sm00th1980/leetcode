import { toBinaryTree, binaryTreeNode, toLevels } from '../structures';

describe('toLevels', () => {
  test('example 1', () => {
    const input = [1];
    const output = [[1]];

    expect(toLevels(input)).toEqual(output);
  });

  test('example 2', () => {
    const input: number[] = [];
    const output: number[][] = [];

    expect(toLevels(input)).toEqual(output);
  });

  test('example 3', () => {
    const input = [1, 2];
    const output = [[1], [2]];

    expect(toLevels(input)).toEqual(output);
  });

  test('example 4', () => {
    const input = [1, 2, 3];
    const output = [[1], [2, 3]];

    expect(toLevels(input)).toEqual(output);
  });

  test('example 5', () => {
    const input = [1, 2, 3, 4];
    const output = [[1], [2, 3], [4]];

    expect(toLevels(input)).toEqual(output);
  });

  test('example 6', () => {
    const input = [1, 2, 3, 4, 5, 6, 7, 8];
    const output = [[1], [2, 3], [4, 5, 6, 7], [8]];

    expect(toLevels(input)).toEqual(output);
  });

  test('example 7', () => {
    const input = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    const output = [[1], [2, 3], [4, 5, 6, 7], [8, 9, 10]];

    expect(toLevels(input)).toEqual(output);
  });

  test('example 8', () => {
    const input = [1, 2, 3, 4, 5, 6, 7, 8, null, 10];
    const output = [[1], [2, 3], [4, 5, 6, 7], [8, null, 10]];

    expect(toLevels(input)).toEqual(output);
  });

  test('example 9', () => {
    const input = [1, 2, 3, null, 5, 6, 7, 8, null, 10];
    const output = [[1], [2, 3], [null, 5, 6, 7], [null, null, 8, null, 10]];

    expect(toLevels(input)).toEqual(output);
  });

  test('example 10', () => {
    const input = [4, -7, -3, null, null, -9, -3, 9, -7];
    const output = [[4], [-7, -3], [null, null, -9, -3], [null, null, null, null, 9, -7]];

    expect(toLevels(input)).toEqual(output);
  });

  test('example 11', () => {
    const input = [1, 2, 3, 4, null, 6, 7, 8, 9, null, null, 12];
    const output = [[1], [2, 3], [4, null, 6, 7], [8, 9, null, null, null, null, 12]];

    expect(toLevels(input)).toEqual(output);
  });
});

describe('toBinaryTree', () => {
  test('example 1', () => {
    const input = [1];
    const output = binaryTreeNode(1);

    expect(toBinaryTree(input)).toEqual(output);
  });

  test('example 2', () => {
    const input: number[] = [];
    const output = null;

    expect(toBinaryTree(input)).toEqual(output);
  });

  test('example 3', () => {
    const input = [1, 2];
    const output = binaryTreeNode(1, binaryTreeNode(2));

    expect(toBinaryTree(input)).toEqual(output);
  });

  test('example 4', () => {
    const input = [1, 2, 3];
    const output = binaryTreeNode(1, binaryTreeNode(2), binaryTreeNode(3));

    expect(toBinaryTree(input)).toEqual(output);
  });

  test('example 5', () => {
    const input = [1, null, 3];
    const output = binaryTreeNode(1, null, binaryTreeNode(3));

    expect(toBinaryTree(input)).toEqual(output);
  });

  test('example 5', () => {
    const input = [1, 2, 3, 4];
    const output = binaryTreeNode(1, binaryTreeNode(2, binaryTreeNode(4)), binaryTreeNode(3));

    expect(toBinaryTree(input)).toEqual(output);
  });

  test('example 6', () => {
    const input = [1, 2, 3, 4, 5, 6];
    const output = binaryTreeNode(
      1,
      binaryTreeNode(2, binaryTreeNode(4), binaryTreeNode(5)),
      binaryTreeNode(3, binaryTreeNode(6)),
    );

    expect(toBinaryTree(input)).toEqual(output);
  });

  test('example 7', () => {
    const input = [1, 2, 3, 4, 5, 6, 7];
    const output = binaryTreeNode(
      1,
      binaryTreeNode(2, binaryTreeNode(4), binaryTreeNode(5)),
      binaryTreeNode(3, binaryTreeNode(6), binaryTreeNode(7)),
    );
    expect(toBinaryTree(input)).toEqual(output);
  });

  test('example 8', () => {
    const input = [1, 2, 3, 4, 5, null, 7];
    const output = binaryTreeNode(
      1,
      binaryTreeNode(2, binaryTreeNode(4), binaryTreeNode(5)),
      binaryTreeNode(3, null, binaryTreeNode(7)),
    );

    expect(toBinaryTree(input)).toEqual(output);
  });

  test('example 9', () => {
    const input = [1, 2, 3, 4, null, 6, 7, 8, 9, null, null, 12];
    const output = binaryTreeNode(
      1,
      binaryTreeNode(2, binaryTreeNode(4, binaryTreeNode(8), binaryTreeNode(9))),
      binaryTreeNode(3, binaryTreeNode(6), binaryTreeNode(7, binaryTreeNode(12))),
    );

    expect(toBinaryTree(input)).toEqual(output);
  });
});
