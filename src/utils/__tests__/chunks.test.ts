import { chunks } from '..';

test('example 1', () => {
  const n = 2;
  const nums = [1, 2, 3, 4, 5, 6];
  const output = [
    [1, 2],
    [3, 4],
    [5, 6],
  ];

  expect(chunks(n, nums)).toEqual(output);
});

test('example 2', () => {
  const n = 3;
  const nums = [1, 2, 3, 4, 5, 6];
  const output = [
    [1, 2, 3],
    [4, 5, 6],
  ];

  expect(chunks(n, nums)).toEqual(output);
});

test('example 3', () => {
  const n = 4;
  const nums = [1, 2, 3, 4, 5, 6];
  const output = [
    [1, 2, 3, 4],
    [5, 6],
  ];

  expect(chunks(n, nums)).toEqual(output);
});
