/* eslint @typescript-eslint/no-empty-function: off */

import { isFunction } from '..';

test('example 1', () => {
  const input = null;

  expect(isFunction(input)).toBe(false);
});

test('example 2', () => {
  const input = undefined;

  expect(isFunction(input)).toBe(false);
});

test('example 3', () => {
  const input = NaN;

  expect(isFunction(input)).toBe(false);
});

test('example 4', () => {
  const input = 1;

  expect(isFunction(input)).toBe(false);
});

test('example 5', () => {
  const input = 0;

  expect(isFunction(input)).toBe(false);
});

test('example 6', () => {
  const input = {};

  expect(isFunction(input)).toBe(false);
});

test('example 7', () => {
  const input: number[] = [];

  expect(isFunction(input)).toBe(false);
});

test('example 8', () => {
  const input = () => {};

  expect(isFunction(input)).toBe(true);
});

test('example 9', () => {
  const input = 1.2;

  expect(isFunction(input)).toBe(false);
});
