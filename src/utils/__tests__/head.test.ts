import { head } from '..';

test('example 1', () => {
  const xs = ['foo', 'bar', 'baz'];

  expect(head(xs)).toEqual('foo');
});

test('example 2', () => {
  const xs: string[] = [];

  expect(head(xs)).toBeUndefined();
});

test('example 3', () => {
  const xs = ('foo' as unknown) as string[];

  expect(head(xs)).toEqual('f');
});
