import { isTrue } from '..';

test('example 1', () => {
  const input = null;

  expect(isTrue(input)).toBe(false);
});

test('example 2', () => {
  const input = undefined;

  expect(isTrue(input)).toBe(false);
});

test('example 3', () => {
  const input = NaN;

  expect(isTrue(input)).toBe(false);
});

test('example 4', () => {
  const input = 1;

  expect(isTrue(input)).toBe(false);
});

test('example 5', () => {
  const input = 0;

  expect(isTrue(input)).toBe(false);
});

test('example 6', () => {
  const input = {};

  expect(isTrue(input)).toBe(false);
});

test('example 7', () => {
  const input: number[] = [];

  expect(isTrue(input)).toBe(false);
});

test('example 8', () => {
  const input = true;

  expect(isTrue(input)).toBe(true);
});

test('example 9', () => {
  const input = false;

  expect(isTrue(input)).toBe(false);
});

test('example 10', () => {
  const input = Infinity;

  expect(isTrue(input)).toBe(false);
});

test('example 11', () => {
  const input = -1;

  expect(isTrue(input)).toBe(false);
});

test('example 12', () => {
  const input = '';

  expect(isTrue(input)).toBe(false);
});

test('example 13', () => {
  const input = 'vfdl';

  expect(isTrue(input)).toBe(false);
});
