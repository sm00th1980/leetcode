import { reject } from '..';

test('example 1', () => {
  const predicate = (value: number) => value === 1;
  const xs = [1, 2, 3];

  expect(reject<number>(predicate, xs)).toEqual([2, 3]);
});

test('example 2', () => {
  const predicate = (value: number) => value === 2;
  const xs = [1, 2, 3];

  expect(reject<number>(predicate, xs)).toEqual([1, 3]);
});

test('example 3', () => {
  const predicate = (_: number, index: number | undefined) => index === 0;
  const xs = [1, 2, 3];

  expect(reject<number>(predicate, xs)).toEqual([2, 3]);
});
