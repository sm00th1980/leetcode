import { findLastIndex } from '..';

test('example 1', () => {
  const predicate = (value: string) => value === '2';
  const xs = ['1', '2', '2', '3', '4'];

  expect(findLastIndex(predicate, xs)).toBe(2);
});

test('example 2', () => {
  const predicate = (value: string) => value === '2';
  const xs: string[] = [];

  expect(findLastIndex(predicate, xs)).toBe(-1);
});

test('example 3', () => {
  const predicate = () => false;
  const xs = ['1', '2', '2', '3'];

  expect(findLastIndex(predicate, xs)).toBe(-1);
});

test('example 4', () => {
  const predicate = (value: string) => value === '2';
  const xs = ['1', '2', '2', '3'];

  expect(findLastIndex(predicate, xs)).toBe(2);
});

test('example 5', () => {
  const predicate = (value: string) => value === '2';
  const xs = ['1', '2.1', '2', '3', '2', '4'];

  expect(findLastIndex(predicate, xs)).toBe(4);
});

test('example 6', () => {
  const predicate = (_: string, index: number | undefined) => index === 1;

  const xs = ['1', '2.1', '2', '3', '2', '4'];

  expect(findLastIndex(predicate, xs)).toBe(1);
});
