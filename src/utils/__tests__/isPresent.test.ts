import { isPresent } from '..';

test('example 1', () => {
  const input = null;

  expect(isPresent(input)).toBe(false);
});

test('example 2', () => {
  const input = undefined;

  expect(isPresent(input)).toBe(false);
});

test('example 3', () => {
  const input = NaN;

  expect(isPresent(input)).toBe(true);
});

test('example 4', () => {
  const input = 1;

  expect(isPresent(input)).toBe(true);
});

test('example 5', () => {
  const input = 0;

  expect(isPresent(input)).toBe(true);
});

test('example 6', () => {
  const input = {};

  expect(isPresent(input)).toBe(true);
});

test('example 7', () => {
  const input: number[] = [];

  expect(isPresent(input)).toBe(true);
});

test('example 8', () => {
  const input = true;

  expect(isPresent(input)).toBe(true);
});

test('example 9', () => {
  const input = false;

  expect(isPresent(input)).toBe(true);
});

test('example 10', () => {
  const input = Infinity;

  expect(isPresent(input)).toBe(true);
});

test('example 11', () => {
  const input = -1;

  expect(isPresent(input)).toBe(true);
});

test('example 12', () => {
  const input = '';

  expect(isPresent(input)).toBe(true);
});

test('example 13', () => {
  const input = 'vfdl';

  expect(isPresent(input)).toBe(true);
});
