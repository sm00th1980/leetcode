import { curry } from '..';

describe('fn of 1 argument', () => {
  const increment = (a: number) => a + 1;

  test('example 1', () => {
    const curriedIncrement = curry(increment);
    expect(curriedIncrement(1)).toBe(2);
  });

  test('example 2', () => {
    const curriedIncrement = curry(increment);
    expect(curriedIncrement(1, 2)).toBe(2);
  });
});

describe('fn of 2 arguments', () => {
  const sum = (a: number, b: number) => a + b;

  test('example 1', () => {
    const curriedSum = curry(sum);
    expect(curriedSum(1)(2)).toBe(3);
  });

  test('example 2', () => {
    const curriedSum = curry(sum);
    expect(curriedSum(1, 2)).toBe(3);
  });

  test('example 3', () => {
    const curriedSum = curry(sum);
    expect(curriedSum(1, 2, 3)).toBe(3);
  });
});

describe('fn of 3 arguments', () => {
  const sum = (a: number, b: number, c: number) => a + b + c;

  test('example 1', () => {
    const curriedSum = curry(sum);
    expect(curriedSum(1)(2)(3)).toBe(6);
  });

  test('example 2', () => {
    const curriedSum = curry(sum);
    expect(curriedSum(1, 2)(3)).toBe(6);
  });

  test('example 3', () => {
    const curriedSum = curry(sum);
    expect(curriedSum(1)(2, 3)).toBe(6);
  });

  test('example 4', () => {
    const curriedSum = curry(sum);
    expect(curriedSum(1, 2, 3)).toBe(6);
  });

  test('example 5', () => {
    const curriedSum = curry(sum);
    expect(curriedSum(1, 2, 3, 4)).toBe(6);
  });
});
