import { drop } from '..';

test('example 1', () => {
  const count = 1;
  const xs = ['foo', 'bar', 'baz'];

  expect(drop(count, xs)).toEqual(['bar', 'baz']);
});

test('example 2', () => {
  const count = 2;
  const xs = ['foo', 'bar', 'baz'];

  expect(drop(count, xs)).toEqual(['baz']);
});

test('example 3', () => {
  const count = 3;
  const xs = ['foo', 'bar', 'baz'];

  expect(drop(count, xs)).toEqual([]);
});

test('example 4', () => {
  const count = 4;
  const xs = ['foo', 'bar', 'baz'];

  expect(drop(count, xs)).toEqual([]);
});

test('example 5', () => {
  const count = 4;
  const xs: string[] = [];

  expect(drop(count, xs)).toEqual([]);
});
