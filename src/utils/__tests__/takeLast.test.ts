import { takeLast } from '..';

test('example 1', () => {
  const count = 1;
  const xs = ['foo', 'bar', 'baz'];

  expect(takeLast(count, xs)).toEqual(['baz']);
});

test('example 2', () => {
  const count = 2;
  const xs = ['foo', 'bar', 'baz'];

  expect(takeLast(count, xs)).toEqual(['bar', 'baz']);
});

test('example 3', () => {
  const count = 3;
  const xs = ['foo', 'bar', 'baz'];

  expect(takeLast(count, xs)).toEqual(['foo', 'bar', 'baz']);
});

test('example 4', () => {
  const count = 4;
  const xs = ['foo', 'bar', 'baz'];

  expect(takeLast(count, xs)).toEqual(['foo', 'bar', 'baz']);
});
