import { complement, isNil } from '..';

describe('', () => {
  const isNotNil = complement(isNil);

  test('example 1', () => {
    const input = null;

    expect(isNotNil(input)).toBe(false);
  });

  test('example 2', () => {
    const input = undefined;

    expect(isNotNil(input)).toBe(false);
  });

  test('example 3', () => {
    const input = NaN;

    expect(isNotNil(input)).toBe(true);
  });

  test('example 4', () => {
    const input = 1;

    expect(isNotNil(input)).toBe(true);
  });

  test('example 5', () => {
    const input = 0;

    expect(isNotNil(input)).toBe(true);
  });

  test('example 6', () => {
    const input = {};

    expect(isNotNil(input)).toBe(true);
  });

  test('example 7', () => {
    const input: number[] = [];

    expect(isNotNil(input)).toBe(true);
  });
});
