import { getMin } from '..';

test('example 1', () => {
  const xs = [1, 2, 3];

  expect(getMin(xs)).toBe(1);
});

test('example 2', () => {
  const xs = [2, 3, 6, 1, 4, 5];

  expect(getMin(xs)).toBe(1);
});

test('example 3', () => {
  const xs = [-1, -2, -3, -6, -4, -5];

  expect(getMin(xs)).toBe(-6);
});

test('example 4', () => {
  const xs = [-2, -3, -6, -4, -1, -5];

  expect(getMin(xs)).toBe(-6);
});

test('example 5', () => {
  const xs: Array<number> = [];

  expect(getMin(xs)).toBeUndefined();
});
