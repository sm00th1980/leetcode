import { path } from '..';

test('example 1', () => {
  const routes = ['a', 'b', 'c'];
  const xs = { a: { b: { c: 1 } } };

  const output = 1;

  expect(path(routes, xs)).toEqual(output);
});

test('example 2', () => {
  const routes = ['a', 'b'];
  const xs = { a: { b: { c: 1 } } };

  const output = { c: 1 };

  expect(path(routes, xs)).toEqual(output);
});

test('example 3', () => {
  const routes = ['a', 'b', 'x'];
  const xs = { a: { b: { c: 1 } } };

  expect(path(routes, xs)).toBeUndefined();
});

test('example 4', () => {
  const routes = ['_COMMUNICATOR_CONTROL_', 'content'];
  const xs = { _COMMUNICATOR_CONTROL_: { validated: true, content: 2 } };

  expect(path(routes, xs)).toEqual(2);
});

test('example 5', () => {
  const routes = ['a', 'b', 'c'];
  const xs = undefined;

  expect(path(routes, xs)).toBeUndefined();
});
