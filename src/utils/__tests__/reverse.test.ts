import { reverse } from '..';

test('example 1', () => {
  const xs: number[] = [];
  const output: number[] = [];

  expect(reverse(xs)).toEqual(output);
  expect(xs).toEqual([]);
});

test('example 2', () => {
  const xs = [1];
  const output = [1];

  expect(reverse(xs)).toEqual(output);
  expect(xs).toEqual([1]);
});

test('example 3', () => {
  const xs = [0, 1];
  const output = [1, 0];

  expect(reverse(xs)).toEqual(output);
  expect(xs).toEqual([0, 1]);
});

test('example 4', () => {
  const xs = [0, 1, 2];
  const output = [2, 1, 0];

  expect(reverse(xs)).toEqual(output);
  expect(xs).toEqual([0, 1, 2]);
});
