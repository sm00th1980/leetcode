import { equals } from '..';

describe('objects', () => {
  test('example 1', () => {
    const xs1 = { a: { b: 1 } };
    const xs2 = { a: { b: 1 } };

    expect(equals(xs1, xs2)).toBe(true);
  });

  test('example 2', () => {
    const xs1 = { a: { b: 1 } };
    const xs2 = { a: { b: 2 } };

    expect(equals(xs1, xs2)).toBe(false);
  });

  test('example 3', () => {
    const xs1 = { a: { b: 1 } };
    const xs2 = { a: 1 };

    expect(equals(xs1, xs2)).toBe(false);
  });

  test('example 4', () => {
    const xs1 = {};
    const xs2 = {};

    expect(equals(xs1, xs2)).toBe(true);
  });

  test('example 5', () => {
    const xs1 = { b: { a: 1 } };
    const xs2 = { a: { b: 2 } };

    expect(equals(xs1, xs2)).toBe(false);
  });
});

describe('arrays', () => {
  test('example 1', () => {
    const xs1 = [1, 2, 3];
    const xs2 = [1, 2, 3];

    expect(equals(xs1, xs2)).toBe(true);
  });

  test('example 2', () => {
    const xs1 = [1];
    const xs2 = [2];

    expect(equals(xs1, xs2)).toBe(false);
  });

  test('example 3', () => {
    const xs1 = [1, 2];
    const xs2 = [1];

    expect(equals(xs1, xs2)).toBe(false);
  });

  test('example 4', () => {
    const xs1: number[] = [];
    const xs2: number[] = [];

    expect(equals(xs1, xs2)).toBe(true);
  });

  test('example 5', () => {
    const xs1 = [3, 2, 1];
    const xs2 = [1, 2, 3];

    expect(equals(xs1, xs2)).toBe(false);
  });
});
