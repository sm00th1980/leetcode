import { append } from '..';

test('example 1', () => {
  const element = '1';
  const xs = ['foo', 'bar', 'baz'];

  expect(append(element, xs)).toEqual(['foo', 'bar', 'baz', '1']);
});

test('example 2', () => {
  const element = 1;
  const xs: Array<number> = [];

  expect(append(element, xs)).toEqual([1]);
});
