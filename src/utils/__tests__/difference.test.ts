import { difference } from '..';

test('example 1', () => {
  const xs1 = [1, 2, 3];
  const xs2 = [3];

  expect(difference(xs1, xs2)).toEqual([1, 2]);
});

test('example 2', () => {
  const xs1 = ['t', 'o', 'm'];
  const xs2 = ['t', 'm'];

  expect(difference(xs1, xs2)).toEqual(['o']);
});

test('example 3', () => {
  const xs1 = ['t', 'm'];
  const xs2 = ['t', 'o', 'm'];

  expect(difference(xs1, xs2)).toEqual([]);
});
