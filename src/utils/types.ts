export type Nil = null | undefined;
export type Option<T> = T | Nil;
export type Callable = (...args: any[]) => any;
export type Pair<T, K> = [T, K];
export type Predicate<T> = (value: T, index?: number) => boolean;
export type Nullable<T> = T | null;
