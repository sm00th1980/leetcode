import { Option, Nullable } from './types';
import { isNil, last, take, isEmpty, tail, drop } from './index';

export class BinaryTreeNode {
  val: number;
  left: Option<BinaryTreeNode>;
  right: Option<BinaryTreeNode>;
  constructor(val?: number, left?: Option<BinaryTreeNode>, right?: Option<BinaryTreeNode>) {
    this.val = val === undefined ? 0 : val;
    this.left = left === undefined ? null : left;
    this.right = right === undefined ? null : right;
  }
}

export const binaryTreeNode = (
  value: number,
  left?: Option<BinaryTreeNode>,
  right?: Option<BinaryTreeNode>,
): BinaryTreeNode => {
  return new BinaryTreeNode(value, left, right);
};

export const toLevels = (xs: Array<Nullable<number>>): Array<Array<Nullable<number>>> => {
  if (isEmpty(xs)) {
    return [];
  }

  return xs.reduce(
    (acc) => {
      if (isEmpty(acc.remaining)) {
        return acc;
      }

      const prevLevel = last(acc.levels);

      const levelContent = prevLevel.reduce((content, parentValue) => {
        if (isNil(parentValue)) {
          return [...content, null, null];
        }

        return [...content, ...acc.remaining.splice(0, 2)];
      }, [] as Array<Nullable<number>>);

      acc.levels.push(levelContent);
      acc.level = acc.level + 1;

      return acc;
    },
    {
      levels: [take(1, xs)] as Array<Array<Nullable<number>>>,
      level: 1,
      remaining: tail(xs),
    },
  ).levels;
};

export const toBinaryTree = (xs: Array<Nullable<number>>): Nullable<BinaryTreeNode> => {
  if (isEmpty(xs)) {
    return null;
  }

  const nodes = toLevels(xs)
    .map((level) => level.map((value) => (isNil(value) ? null : binaryTreeNode(value!))))
    .map((level, levelIndex, levels) =>
      level.map((node, nodeIndex) => {
        if (isNil(node)) {
          return node;
        }

        const leftNode = levels[levelIndex + 1]?.[nodeIndex * 2];
        const rightNode = levels[levelIndex + 1]?.[nodeIndex * 2 + 1];

        node!.left = isNil(leftNode) ? null : leftNode;
        node!.right = isNil(rightNode) ? null : rightNode;

        return node;
      }),
    );

  return nodes[0][0];
};

export class ListNode {
  val: number;
  next: Option<ListNode>;
  constructor(val?: number, next?: Option<ListNode>) {
    this.val = val === undefined ? 0 : val;
    this.next = next === undefined ? null : next;
  }

  isLast() {
    return isNil(this.next);
  }
}

export const listNode = (val?: number, next?: Option<ListNode>): ListNode => {
  return new ListNode(val, next);
};

export const toListNode = <T>(xs: T[]): Option<ListNode> => {
  if (isEmpty(xs)) {
    return null;
  }

  const root = listNode((xs[0] as unknown) as number);
  if (xs.length === 1) {
    return root;
  }

  const second = listNode((xs[1] as unknown) as number);
  root.next = second;
  if (xs.length === 2) {
    return root;
  }

  return drop(2, xs).reduce(
    (acc, value) => {
      const next_ = listNode((value as unknown) as number);
      acc.next.next = next_;
      acc.next = next_;

      return acc;
    },
    {
      next: second,
      root,
    },
  ).root;
};

export class TreeNode {
  val: number;
  children: TreeNode[];
  constructor(val?: number, children?: TreeNode[]) {
    this.val = val === undefined ? 0 : val;
    this.children = children === undefined ? [] : children;
  }
}

export const treeNode = (value: number, children?: Option<TreeNode[]>): TreeNode => {
  if (isNil(children)) {
    return new TreeNode(value, undefined);
  }

  return new TreeNode(value, children!);
};

export class Stack<T> {
  elements: Array<T> = [];

  push(element: T) {
    this.elements.push(element);
  }

  pop() {
    if (this.isEmpty()) {
      throw Error('oops, stack is empty, failed to pop from it');
    }

    return this.elements.pop();
  }

  peek() {
    if (this.isEmpty()) {
      throw Error('oops, stack is empty, failed to get top of it');
    }

    return last(this.elements);
  }

  isEmpty() {
    return this.elements.length <= 0;
  }
}
