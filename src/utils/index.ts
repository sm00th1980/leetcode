import { Option, Callable, Pair, Predicate } from './types';

export const isFunction = (value: any): boolean => {
  return !!(value && value.constructor && value.call && value.apply);
};

export const isNil = <T>(value: T | T[]): boolean => {
  if (typeof value === 'undefined') {
    return true;
  }
  if (value === null && typeof value === 'object') {
    return true;
  }

  return false;
};

export const complement = (fn: Callable) => <T>(value: T | T[]): boolean => !fn(value);

export const isPresent = complement(isNil);

export const isTrue = (value: unknown): boolean => value === true;
export const isFalse = (value: unknown): boolean => value === false;

export const head = <T>(xs: T[]): T => xs[0];

export const tail = <T>(xs: T[]): T[] => xs.slice(1);

export const last = <T>(xs: T[]): T => xs[xs.length - 1];

export const init = <T>(xs: T[] | string): T[] | string => xs.slice(0, -1);

export const getMax = (xs: number[] | Set<number>): Option<number> => {
  const uniqValues = unique(xs);
  return uniqValues.reduce((acc, x) => (x > acc ? x : acc), head(uniqValues));
};

export const getMin = (xs: number[] | Set<number>): Option<number> => {
  const uniqValues = unique(xs);
  return uniqValues.reduce((acc, x) => (x < acc ? x : acc), head(uniqValues));
};

export const repeat = (n: number, s: string): string => {
  return Array(n)
    .fill(undefined)
    .map(() => s)
    .join('');
};

export const getSum = (xs: number[]): number => xs.reduce((acc, x) => acc + x, 0);

export const chunks = <T>(size: number, xs: T[]): T[][] => {
  const initialValue: T[][] = [];
  return xs.reduce((acc, _, index) => {
    if (index % size === 0) {
      const sliced = xs.slice(index, index + size);
      acc.push(sliced);
    }

    return acc;
  }, initialValue);
};

export const reverse = <T>(xs: T[]): T[] => [...xs].reverse();

export const dec2bin = (dec: number): string => {
  return (dec >>> 0).toString(2);
};

export const bin2dec = (bin: string): number => {
  return parseInt(bin, 2);
};

export const initial = <T>(xs: T[]): T[] => xs.slice(0, -1);

export const unique = <T>(xs: T[] | Set<T>): T[] => Array.from(new Set(xs));

export const isEmpty = <T>(xs: T[]): boolean => xs.length === 0;

export const getCount = <T>(x: T, xs: T[]): number => {
  return xs.reduce((acc, value) => {
    return value === x ? acc + 1 : acc;
  }, 0);
};

export const all = <T>(predicate: Predicate<T>, xs: T[]): boolean => {
  return xs.reduce((acc: boolean, value, index) => {
    return acc ? predicate(value, index) : false;
  }, true);
};

export const any = <T>(predicate: Predicate<T>, xs: T[]): boolean => {
  return xs.reduce((acc: boolean, value, index) => {
    return acc ? true : predicate(value, index);
  }, false);
};

export const identity = <T>(value: T): T => value;

export const transposeMatrix = <T>(matrix: T[][]): T[][] => {
  const getColumn = <T>(columnIndex: number, grid: T[][]): T[] => {
    return grid.map((row) => row[columnIndex]);
  };

  return Array(matrix[0].length)
    .fill(undefined)
    .map((_, index) => getColumn(index, matrix));
};

export const rotateMatrixClockwise = (matrix: number[][]): number[][] => {
  const getColumn = <T>(columnIndex: number, grid: T[][]): T[] => {
    return grid.map((row) => row[columnIndex]);
  };

  return Array(matrix[0].length)
    .fill(undefined)
    .map((_, index) => getColumn(index, matrix).reverse());
};

export const zip = <T, K>(xs1: T[], xs2: K[]): [T, K][] => {
  const initialValue: [T, K][] = [];
  return xs1.reduce((acc, item1, index) => {
    const item2 = xs2[index];
    acc.push([item1, item2]);
    return acc;
  }, initialValue);
};

export const difference = <T>(xs1: T[], xs2: T[]): T[] => {
  const initialValue: T[] = [];
  return xs1.reduce((acc, item) => {
    if (!xs2.includes(item)) {
      acc.push(item);
    }

    return acc;
  }, initialValue);
};

export const capitalize = (value: Option<string>): Option<string> => {
  if (value) {
    const words = value.split(' ');

    return words
      .map((word) => {
        const letters = word.split('');
        return [head(letters).toUpperCase(), ...tail(letters)].join('');
      })
      .join(' ');
  }

  return value;
};

export const sort = <T>(comparator: (a: T, b: T) => number, xs: T[]): T[] => [...xs].sort(comparator);

const isNumber = (possibleNumber: unknown): possibleNumber is number => isDigit(possibleNumber as string);
const isString = (possibleString: unknown): possibleString is string => isLetter(possibleString as string);

type K = string | number;
export const ascendSort = <K>(xs: K[]): K[] => {
  const stringComparator = (a: string, b: string): number => a.localeCompare(b);
  const numberComparator = (a: number, b: number): number => a - b;

  if (isString(xs[0])) {
    return (sort<string>(stringComparator, (xs as unknown) as string[]) as unknown) as K[];
  }

  if (isNumber(xs[0])) {
    return (sort<number>(numberComparator, (xs as unknown) as number[]) as unknown) as K[];
  }

  throw Error('Invalid input type, dont know how to compare');
};

export const descendSort = <K>(xs: K[]): K[] => {
  const stringComparator = (a: string, b: string): number => b.localeCompare(a);
  const numberComparator = (a: number, b: number): number => b - a;

  if (isString(xs[0])) {
    return (sort<string>(stringComparator, (xs as unknown) as string[]) as unknown) as K[];
  }

  if (isNumber(xs[0])) {
    return (sort<number>(numberComparator, (xs as unknown) as number[]) as unknown) as K[];
  }

  throw Error('Invalid input type, dont know how to compare');
};

export const compose = (...fns: Array<Callable>) => (value: any) => {
  return fns.reduce((acc, fn) => fn(acc), value);
};

export const flatten = <T extends any[]>(xs: T): T => {
  return xs.reduce((acc, x) => {
    if (!Array.isArray(x)) {
      return [...acc, x];
    }

    return [...acc, ...flatten(x)];
  }, []);
};

export const path = (keys: string[], x: Option<Record<string, unknown>>): unknown => {
  if (isNil(x)) {
    return undefined;
  }

  type InitialValue = {
    value: Option<Record<string, unknown>>;
    found: boolean;
  };

  const initialValue = {
    value: x,
    found: false,
  };

  const { value } = keys.reduce<InitialValue>((acc, key) => {
    const { value, found } = acc;
    if (found || isNil(value)) {
      return acc;
    }

    const valueByKey = value![key];

    if (isNil(valueByKey)) {
      return { found: true, value: undefined };
    }

    return { found: false, value: valueByKey } as InitialValue;
  }, initialValue);

  return value;
};

export const isArray = Array.isArray;

export const isObject = (value: any): boolean => {
  if (isNil(value)) {
    return false;
  }

  return !isArray(value) && typeof value === 'object';
};

export const equals = (value1: unknown, value2: unknown): boolean => {
  const stringifiedValue1 = JSON.stringify(value1);
  const stringifiedValue2 = JSON.stringify(value2);

  return stringifiedValue1 === stringifiedValue2;
};

export const toPairs = (x: Record<string, unknown>): unknown[] => {
  const keys = Object.keys(x);
  const values = Object.values(x);

  return zip(keys, values);
};

export const fromPairs = (x: Array<Pair<string, unknown>>): Record<string, unknown> => {
  return x.reduce((acc, [key, value]) => ({ ...acc, [key]: value }), {});
};

export const values = Object.values;

export const curry = (fn: Callable): Callable => {
  let restArgsLength = fn.length;
  return (...args: unknown[]) => {
    if (args.length >= restArgsLength) {
      return fn(...args);
    }

    restArgsLength = restArgsLength - args.length;

    return curry(fn.bind(null, ...args));
  };
};

export const takeLast = <T>(size: number, xs: T[]): T[] => xs.filter((_, index) => index + size >= xs.length);

export const take = <T>(size: number, xs: T[]): T[] => xs.filter((_, index) => index < size);

export const drop = <T>(size: number, xs: T[]): T[] => xs.filter((_, index) => index >= size);

export const isLastIndex = (index: number, xs: unknown[]): boolean => index + 1 >= xs.length;

export const reject = <T>(predicate: Predicate<T>, xs: T[]): T[] =>
  xs.filter((value, index) => !predicate(value, index));

export const toNumber = (value: unknown): Option<number> => {
  if (isNil(value) || Array.isArray(value) || value === true || value === false) {
    return undefined;
  }

  const possibleNumber = Number(value);
  if (Number.isNaN(possibleNumber)) {
    return undefined;
  }

  return possibleNumber;
};

export const toNumberWithDefaultZero = (value: unknown): number => toNumber(value) || 0;

export const isDigit = (s: string): boolean => {
  return /\d+/.test(s);
};

export const isLetter = (s: string): boolean => {
  return /[a-zA-Z]+/.test(s);
};

export const assert = (condition: boolean, errorDescription: string): void | never => {
  if (condition) {
    throw Error(errorDescription);
  }
};

export const findLastIndex = <T>(predicate: Predicate<T>, xs: T[]): number => {
  const index = reverse(xs).findIndex((value, index) => {
    const idx = xs.length - index - 1;
    return predicate(value, idx);
  });

  if (index === -1) {
    return index;
  }

  return xs.length - index - 1;
};

export const findLast = <T>(predicate: Predicate<T>, xs: T[]): Option<T> => {
  return reverse(xs).find(predicate);
};

export const findIndex = <T>(predicate: Predicate<T>, xs: T[]): number => {
  return xs.findIndex(predicate);
};

export const append = <T>(element: T, xs: T[]): T[] => [...xs, element];

export const prepend = <T>(element: T, xs: T[]): T[] => [element, ...xs];

export const isZero = (value: unknown): boolean => value === 0 || value === -0;
