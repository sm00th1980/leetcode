import slidingWindow from '..';

describe('numberOfColumns=1', () => {
  const totalCount = 9;
  const numberOfColumns = 1;
  const viewportCount = 3;

  test('first page, focusedIndex=0', () => {
    const focusedIndex = 0;

    const output = {
      beforeIndexes: [],
      viewportIndexes: [0, 1, 2],
      afterIndexes: [3],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        numberOfColumns,
        viewportCount,
      }),
    ).toEqual(output);
  });

  test('second page, focusedIndex=1', () => {
    const focusedIndex = 1;

    const output = {
      beforeIndexes: [0],
      viewportIndexes: [1, 2, 3],
      afterIndexes: [4],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        numberOfColumns,
        viewportCount,
      }),
    ).toEqual(output);
  });

  test('third page, focusedIndex=2', () => {
    const focusedIndex = 2;

    const output = {
      beforeIndexes: [1],
      viewportIndexes: [2, 3, 4],
      afterIndexes: [5],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        numberOfColumns,
        viewportCount,
      }),
    ).toEqual(output);
  });

  test('fourth page, focusedIndex=3', () => {
    const focusedIndex = 3;

    const output = {
      beforeIndexes: [2],
      viewportIndexes: [3, 4, 5],
      afterIndexes: [6],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        numberOfColumns,
        viewportCount,
      }),
    ).toEqual(output);
  });

  test('fifth page, focusedIndex=6', () => {
    const focusedIndex = 6;

    const output = {
      beforeIndexes: [5],
      viewportIndexes: [6, 7, 8],
      afterIndexes: [],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        numberOfColumns,
        viewportCount,
      }),
    ).toEqual(output);
  });
});

describe('numberOfColumns=2', () => {
  const totalCount = 11;
  const numberOfColumns = 2;
  const viewportCount = 2;

  test('first page, focusedIndex=0', () => {
    const focusedIndex = 0;

    const output = {
      beforeIndexes: [],
      viewportIndexes: [0, 1],
      afterIndexes: [2, 3],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        numberOfColumns,
        viewportCount,
      }),
    ).toEqual(output);
  });

  test('first page, focusedIndex=1', () => {
    const focusedIndex = 1;

    const output = {
      beforeIndexes: [],
      viewportIndexes: [0, 1],
      afterIndexes: [2, 3],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        numberOfColumns,
        viewportCount,
      }),
    ).toEqual(output);
  });

  test('second page, focusedIndex=2', () => {
    const focusedIndex = 2;

    const output = {
      beforeIndexes: [0, 1],
      viewportIndexes: [2, 3],
      afterIndexes: [4, 5],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        numberOfColumns,
        viewportCount,
      }),
    ).toEqual(output);
  });

  test('second page, focusedIndex=3', () => {
    const focusedIndex = 3;

    const output = {
      beforeIndexes: [0, 1],
      viewportIndexes: [2, 3],
      afterIndexes: [4, 5],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        numberOfColumns,
        viewportCount,
      }),
    ).toEqual(output);
  });

  test('fourth page, focusedIndex=6', () => {
    const focusedIndex = 6;

    const output = {
      beforeIndexes: [4, 5],
      viewportIndexes: [6, 7],
      afterIndexes: [8, 9],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        numberOfColumns,
        viewportCount,
      }),
    ).toEqual(output);
  });
});

describe('numberOfColumns=3', () => {
  const totalCount = 9;
  const numberOfColumns = 3;
  const viewportCount = 6;

  test('first page, focusedIndex=0', () => {
    const focusedIndex = 0;

    const output = {
      beforeIndexes: [],
      viewportIndexes: [0, 1, 2, 3, 4, 5],
      afterIndexes: [6, 7, 8],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        numberOfColumns,
        viewportCount,
      }),
    ).toEqual(output);
  });

  test('first page, focusedIndex=1', () => {
    const focusedIndex = 1;

    const output = {
      beforeIndexes: [],
      viewportIndexes: [0, 1, 2, 3, 4, 5],
      afterIndexes: [6, 7, 8],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        numberOfColumns,
        viewportCount,
      }),
    ).toEqual(output);
  });

  test('first page, focusedIndex=2', () => {
    const focusedIndex = 2;

    const output = {
      beforeIndexes: [],
      viewportIndexes: [0, 1, 2, 3, 4, 5],
      afterIndexes: [6, 7, 8],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        numberOfColumns,
        viewportCount,
      }),
    ).toEqual(output);
  });

  test('second page, focusedIndex=3', () => {
    const focusedIndex = 3;

    const output = {
      beforeIndexes: [0, 1, 2],
      viewportIndexes: [3, 4, 5, 6, 7, 8],
      afterIndexes: [],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        numberOfColumns,
        viewportCount,
      }),
    ).toEqual(output);
  });

  test('second page, focusedIndex=4', () => {
    const focusedIndex = 4;

    const output = {
      beforeIndexes: [0, 1, 2],
      viewportIndexes: [3, 4, 5, 6, 7, 8],
      afterIndexes: [],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        numberOfColumns,
        viewportCount,
      }),
    ).toEqual(output);
  });

  test('second page, focusedIndex=5', () => {
    const focusedIndex = 5;

    const output = {
      beforeIndexes: [0, 1, 2],
      viewportIndexes: [3, 4, 5, 6, 7, 8],
      afterIndexes: [],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        numberOfColumns,
        viewportCount,
      }),
    ).toEqual(output);
  });

  test('third page, focusedIndex=6', () => {
    const focusedIndex = 6;

    const output = {
      beforeIndexes: [3, 4, 5],
      viewportIndexes: [6, 7, 8],
      afterIndexes: [],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        numberOfColumns,
        viewportCount,
      }),
    ).toEqual(output);
  });
});

describe('numberOfColumns=4', () => {
  const totalCount = 121;
  const numberOfColumns = 4;
  const viewportCount = numberOfColumns * 3;

  test('first page, focusedIndex=0', () => {
    const focusedIndex = 0;

    const output = {
      beforeIndexes: [],
      viewportIndexes: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
      afterIndexes: [12, 13, 14, 15],
    };

    expect(
      slidingWindow({
        totalCount,
        focusedIndex,
        numberOfColumns,
        viewportCount,
      }),
    ).toEqual(output);
  });
});
