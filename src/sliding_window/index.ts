import { chunks, takeLast, take } from '../utils';

type Return = {
  beforeIndexes: Array<number>;
  viewportIndexes: Array<number>;
  afterIndexes: Array<number>;
};

type Props = {
  totalCount: number;
  focusedIndex: number;
  numberOfColumns: number;
  viewportCount: number;
};

const slidingWindow = ({ totalCount, focusedIndex, numberOfColumns, viewportCount }: Props): Return => {
  const indexes = Array(totalCount)
    .fill(undefined)
    .map((_, index) => index);

  const pages = chunks(numberOfColumns, indexes);
  const indexOfPage = pages.findIndex((page) => page.includes(focusedIndex));

  const startIndex = indexOfPage * numberOfColumns;
  const endIndex = startIndex + viewportCount;

  const beforeIndexes = indexes.slice(0, startIndex);
  const viewportIndexes = indexes.slice(startIndex, endIndex);
  const afterIndexes = indexes.slice(endIndex);

  return {
    beforeIndexes: takeLast(numberOfColumns, beforeIndexes),
    viewportIndexes,
    afterIndexes: take(numberOfColumns, afterIndexes),
  };
};

export default slidingWindow;
